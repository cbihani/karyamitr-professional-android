package com.karyamitr.workseekers.viewmodel;


import androidx.lifecycle.ViewModel;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.MyApplication;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.SkillSets;
import com.karyamitr.workseekers.utils.CallBackHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterViewModel extends ViewModel {

    private static final String TAG = RegisterViewModel.class.getSimpleName();
    private ResultCallBack listner, basicProfileListener, qualificationListener, experienceListener,
            skillListener, languagesListener;
    private Fragment fragment;

    SkillDataInterface skillDataInterface;


    public RegisterViewModel() {

    }

    public void setSkillDataInterface(SkillDataInterface skillDataInterface) {
        this.skillDataInterface = skillDataInterface;
    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void getBasicProfileListner(ResultCallBack resultCallBack) {
        basicProfileListener = resultCallBack;
    }

    public void getQualificationListner(ResultCallBack resultCallBack) {
        qualificationListener = resultCallBack;
    }

    public void getExperienceListener(ResultCallBack resultCallBack) {
        experienceListener = resultCallBack;
    }

    public void getSkillsListener(ResultCallBack resultCallBack) {
        skillListener = resultCallBack;
    }

    public void getLanguagesListener(ResultCallBack resultCallBack) {
        languagesListener = resultCallBack;
    }


    public void callApi(String firstName, String lastName) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JsonObject lData = new JsonObject();
        JsonObject lJson = new JsonObject();
        lJson.addProperty("first_name", firstName);
        lJson.addProperty("last_name", lastName);
        lData.add("user", lJson);

        Call call = apiService.registerUser(lData);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            listner.onSuccess(data);
                        } else {
                            listner.onFailure(data.getMessage());
                        }
                    } else {
                        listner.onFailure(response.message());
                    }

                } else {
                    listner.onFailure("failed");
                }
            }
        });

    }

    public void callApi(Professional professional) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.updateProfile(professional);
        call.enqueue(new CallBackHelper(basicProfileListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            basicProfileListener.onSuccess(data);
                        } else {
                            basicProfileListener.onFailure("failed");
                        }
                    } else {
                        basicProfileListener.onFailure(response.message());
                    }

                } else {
                    basicProfileListener.onFailure("failed");
                }
            }
        });

    }


    public void callQualificationApi(JsonObject qualification) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call call = apiService.createQualification(qualification);
        call.enqueue(new CallBackHelper(qualificationListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            qualificationListener.onSuccess(data);
                        } else {
                            qualificationListener.onFailure("failed");
                        }
                    } else {
                        qualificationListener.onFailure(response.message());
                    }

                } else {
                    qualificationListener.onFailure("failed");
                }
            }
        });
    }

    public void callApiForExperience(JsonObject workExperience) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.createExperience(workExperience);
        call.enqueue(new CallBackHelper(experienceListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            experienceListener.onSuccess(data);
                        } else {
                            experienceListener.onFailure("failed");
                        }
                    } else {
                        experienceListener.onFailure(response.message());
                    }


                } else {
                    experienceListener.onFailure("failed");
                }
            }
        });
    }


    public void callApiForSkill(String allSkills) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("skills", allSkills);

        Call call = apiService.addSkills(jsonObject);
        call.enqueue(new CallBackHelper(skillListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            skillListener.onSuccess(data);
                        } else {
                            skillListener.onFailure("failed. Try again later");
                        }
                    } else {
                        skillListener.onFailure("Something went wrong.. Try again later");
                    }

                } else {
                    skillListener.onFailure("failed. Try again later");
                }
            }
        });
    }


    public void callApiForUpdateSkill(JsonObject jsonObject, String publicId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.updateSkills(publicId, jsonObject);
        call.enqueue(new CallBackHelper(skillListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            skillListener.onSuccess(data);
                        } else {
                            skillListener.onFailure("failed");
                        }
                    } else {
                        skillListener.onFailure("failed");
                    }

                } else {
                    skillListener.onFailure("failed");
                }
            }
        });
    }

    public void callLanguagesApi(Professional professional) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.updateProfileLanguages(professional);
        call.enqueue(new CallBackHelper(languagesListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            languagesListener.onSuccess(data);
                        } else {
                            languagesListener.onFailure(data.getMessage());
                        }
                    } else {
                        languagesListener.onFailure("failed");
                    }

                } else {
                    languagesListener.onFailure("failed");
                }
            }
        });

    }

    public void callApiForSetSkills(String skillsSet) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.setSkills(skillsSet);
        call.enqueue(new CallBackHelper(skillListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    List<String> data = (List<String>) response.body();
                    if (data != null) {

                        skillDataInterface.getSkillList(data);
                    }

                } else {
                    skillListener.onFailure("failed");
                }
            }
        });
    }

    public void callApiForUpdateExperience(String publicId, JsonObject obj) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.updateExperience(publicId, obj);
        call.enqueue(new CallBackHelper(experienceListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            experienceListener.onSuccess(data);
                        } else {
                            experienceListener.onFailure("failed");
                        }
                    }

                } else {
                    experienceListener.onFailure("failed");
                }
            }
        });

    }

    public void callApiForUpdateQualification(String public_id, JsonObject qualify) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call call = apiService.updateQualification(public_id, qualify);
        call.enqueue(new CallBackHelper(qualificationListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            qualificationListener.onSuccess(data);
                        } else {
                            qualificationListener.onFailure("failed");
                        }
                    }

                } else {
                    qualificationListener.onFailure("failed");
                }
            }
        });

    }

    public interface SkillDataInterface {
        void getSkillList(List<String> skillList);

        void getSkillSet(List<SkillSets> skillSets);

    }

    public void getSkillSets() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getSkillSet();
        call.enqueue(new CallBackHelper(skillListener) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
//                        if (data.isSuccess()) {
                        skillDataInterface.getSkillSet(data.getSkill_sets());
//                        } else {
//                            skillListener.onFailure("failed");
//                        }
                    }

                } else {
                    skillListener.onFailure("failed");
                }
            }
        });

    }


    public boolean checkValidationForWorkExperience(String toString, String toString1, String toString2) {
        boolean flag = true;
        if (toString.isEmpty()) {
            flag = false;
            experienceListener.onValidationFailed("Please enter Designation");

        } else if (toString1.isEmpty()) {
            flag = false;
            experienceListener.onValidationFailed("Please enter Company name");

        } else if (toString2.isEmpty()) {
            flag = false;
            experienceListener.onValidationFailed("Please enter city");

        }

        return flag;
    }

    public boolean checkValidationForQualification(String toString, String toString1,String toString3,String toString4) {
        boolean flag = true;
        if (toString.isEmpty()) {
            flag = false;
            qualificationListener.onValidationFailed("Please enter Education");

        } else if (toString1.isEmpty()) {
            flag = false;
            qualificationListener.onValidationFailed("Please enter University");

        }
        else if (toString3.isEmpty()) {
            flag = false;
            qualificationListener.onValidationFailed("Please enter Start Year");

        }
        else if (toString4.isEmpty()) {
            flag = false;
            qualificationListener.onValidationFailed("Please enter End Year");

        }
        return flag;
    }


    public boolean checkValidation(String name, String lastname, boolean checked) {
        boolean flag = true;
        if (name.isEmpty()) {
            flag = false;
            listner.onValidationFailed("Please enter first name");

        } else if (lastname.isEmpty()) {
            flag = false;
            listner.onValidationFailed("Please enter last name");

        } else if (!checked) {
            flag = false;
            listner.onValidationFailed("Please accept the Terms & Conditions");

        }

        return flag;
    }

    public boolean checkValidationForProfessional(Professional professional) {
        boolean flag = true;
        if (professional.getCurrent_address().isEmpty()) {
            flag = false;
            basicProfileListener.onValidationFailed("Please enter Current address");

        } else if (!professional.getAlternate_contact_number().isEmpty() && !MyApplication.isValidMobile(professional.getAlternate_contact_number())) {
            flag = false;
            basicProfileListener.onValidationFailed("Please enter a valid mobile number");

        } else if (professional.getGender() == null) {
            flag = false;
            basicProfileListener.onValidationFailed("Please select Gender");
        } else if (professional.getDate_of_birth() == null || professional.getDate_of_birth().equalsIgnoreCase("")) {
            flag = false;
            basicProfileListener.onValidationFailed("Please enter Birth Date");
        } else
            try {
                if (Float.parseFloat(professional.getTotal_experience()) > 60f || Float.parseFloat(professional.getTotal_experience()) < 0f) {
                    flag = false;
                    basicProfileListener.onValidationFailed("Please enter a valid experience");
                }
            } catch (Exception e) {
            }


        return flag;
    }

}
