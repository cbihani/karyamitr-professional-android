package com.karyamitr.workseekers.viewmodel;

import androidx.lifecycle.ViewModel;

import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.CallBackHelper;

import retrofit2.Call;
import retrofit2.Response;

public class SplashViewModel extends ViewModel {

    private static final String TAG = SplashViewModel.class.getSimpleName();
    private ResultCallBack listner;

    public SplashViewModel() {

    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void callApi() {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.validateAuthToken();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
//                        if (data.isSuccess()) {
                        listner.onSuccess(data);
//                        } else {
//
//                        }
                    }
                    else{
                        listner.onFailure("new user");
                    }

                }
                else {
                    listner.onFailure("Fail");

                }
            }
        });

    }


    public boolean checkValidation(String mobile) {
        boolean flag = true;
        if (mobile.isEmpty()) {
            flag = false;
//            listner.onValidationFailed(R.string.emptyMobileNumber);

        }


        return flag;
    }

}