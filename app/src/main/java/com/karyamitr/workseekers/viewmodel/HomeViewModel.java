package com.karyamitr.workseekers.viewmodel;


import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.DeleteCallBackHelper;
import com.karyamitr.workseekers.api.DeleteCallBacks;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.api.UpdateCallbacks;
import com.karyamitr.workseekers.model.JobApplications;
import com.karyamitr.workseekers.model.Jobs;
import com.karyamitr.workseekers.model.Posts;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.ResponseModel;
import com.karyamitr.workseekers.model.SkillSets;
import com.karyamitr.workseekers.utils.CallBackHelper;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    private static final String TAG = HomeViewModel.class.getSimpleName();
    private ResultCallBack listner;
    private DeleteCallBacks deleteCallBacks;
    private UpdateCallbacks updateCallbacks;

    SkillInterface skillDataInterface;

    JobApplyInterface jobApplyInterface;

    public void callLanguagesApi(Professional professional) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.updateProfileLanguages(professional);
        call.enqueue(new CallBackHelper(updateCallbacks) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            updateCallbacks.onUpdateSuccess(data);
                        } else {
                            updateCallbacks.onUpdateFailure("failed");
                        }
                    } else {
                        updateCallbacks.onUpdateFailure("failed");
                    }

                } else {
                    updateCallbacks.onUpdateFailure("failed");
                }
            }
        });
    }


    public interface JobApplyInterface {
        void getresponse(Object object);

    }

    public void setJobApplyInterface(JobApplyInterface jobApplyInterface) {
        this.jobApplyInterface = jobApplyInterface;
    }

    public HomeViewModel() {

    }

    public void setSkillInterface(SkillInterface skillDataInterface) {
        this.skillDataInterface = skillDataInterface;
    }

    public void callApiForJob(JsonObject jobApplication) {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.applyJob(jobApplication);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {

                        jobApplyInterface.getresponse(response.body());

                    } else {
                        listner.onFailure(response.message());
                    }
                } else {
                    listner.onFailure(response.message());
                }
            }
        });

    }

    public void callAppliedJobDetails(String publicId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getAppliedJobDetails(publicId);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {

                        jobApplyInterface.getresponse(response.body());

                    } else {
                        listner.onFailure(response.message());
                    }
                } else {
                    listner.onFailure(response.message());
                }
            }
        });
    }

    public interface SkillInterface {

        void getSkillSet(List<SkillSets> skillSets);

    }

    public void setUpdateCallbacks(UpdateCallbacks updateCallbacks) {
        this.updateCallbacks = updateCallbacks;
    }

    public void setDeleteCallBacks(DeleteCallBacks deleteCallBacks) {
        this.deleteCallBacks = deleteCallBacks;
    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void callApi(int currentPage) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getJobs(currentPage);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseModel body = (ResponseModel) response.body();
                        if (body != null) {
                            if (body.isSuccess()) {
                                List<Jobs> jobs = body.getData();
                                listner.onSuccess(jobs);
                            } else {
                                listner.onFailure(body.getMessage());
                            }
                        } else {
                            listner.onFailure(body.getMessage());
                        }

                    } else {
                        try {
                            Log.e("onResponse : Faild","-"+response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        listner.onFailure("Faild");
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }

    public void callGetProfile() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getProfile();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        Professional body = (Professional) response.body();
                        if (body != null) {
                            if (body.isSuccess()) {

                                listner.onSuccess(response.body());
                            } else {
                                listner.onSuccess(response.body());
                            }
                        } else {
                            listner.onFailure("Faild");
                        }


                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }

    public void callApiForFilteredData(JsonObject filter) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.applyFilter(filter);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseModel data = (ResponseModel) response.body();
                        if (data != null) {
                            if (data.isSuccess()) {
                                List<Jobs> jobs = data.getData();
                                listner.onSuccess(jobs);
                            } else {
                                listner.onFailure(data.getMessage());
                            }
                        } else {
                            listner.onFailure("Faild");
                        }
                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }

    public void callAppliedJobListApi() {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getJobApplications();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();
                        if (data != null) {
                            List<JobApplications> applications = data.getJob_applications();
                            listner.onSuccess(applications);
                        } else {
                            listner.onFailure(data.getMessage());
                        }
                    } else {
                        listner.onFailure("Faild");
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });
    }

    public void callKaryapediaApi() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getKaryapediaData();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();

                        if (data != null) {
                            if (data.isSuccess()) {
                                List<Posts> postsList = data.getPosts();

                                listner.onSuccess(postsList);

                            } else {
                                listner.onFailure(data.getMessage());
                            }
                        } else {
                            listner.onFailure("Faild");
                        }

                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }


    public void deleteSkillApi(String public_id, JsonObject jsonObject) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.deleteSkill(public_id, jsonObject);
        call.enqueue(new DeleteCallBackHelper(deleteCallBacks) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();
                        if (data != null) {

                            deleteCallBacks.onDeleteSuccess(data.getMessage());
                        } else {
                            listner.onFailure("Failed");
                        }
                    } else {
                        deleteCallBacks.onDeleteFailure(response.message());
                    }
                } else {
                    listner.onFailure("Failed");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }


    public void deleteWorkExApi(String public_id) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.deleteWorkEx(public_id);
        call.enqueue(new DeleteCallBackHelper(deleteCallBacks) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();
                        if (data != null) {
                            deleteCallBacks.onDeleteSuccess(data.getMessage());
                        } else {
                            listner.onFailure("Failed");
                        }
                    } else {
                        deleteCallBacks.onDeleteFailure(response.message());
                    }
                } else {
                    listner.onFailure("Failed");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }


    public void deleteQUalificationApi(String public_id) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.deleteQualification(public_id);
        call.enqueue(new DeleteCallBackHelper(deleteCallBacks) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();
                        if (data != null) {
                            deleteCallBacks.onDeleteSuccess(data.getMessage());
                        } else {
                            listner.onFailure("Failed");
                        }
                    } else {
                        deleteCallBacks.onDeleteFailure(response.message());
                    }
                } else {
                    listner.onFailure("Failed");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    public void getSkillSets() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getSkillSet();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
//                        if (data.isSuccess()) {
                        skillDataInterface.getSkillSet(data.getSkill_sets());
//                        } else {
//                            skillListener.onFailure("failed");
//                        }
                    } else {
                        listner.onFailure(response.message());
                    }

                } else {
                    listner.onFailure("failed");
                }
            }
        });

    }
}
