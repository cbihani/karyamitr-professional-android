package com.karyamitr.workseekers.viewmodel;


import androidx.lifecycle.ViewModel;

import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.User;
import com.karyamitr.workseekers.utils.CallBackHelper;

import retrofit2.Call;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    private static final String TAG = LoginViewModel.class.getSimpleName();
    private ResultCallBack listner;

    public LoginViewModel() {

    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void callApi(String mobile) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        User lUser = new User();
        lUser.setMobilenumber(mobile);
        lUser.setRole("professional");
        Call call = apiService.getUser(lUser);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();
                        if (data != null) {
                            if (data.isSuccess()) {
                                listner.onSuccess(data);
                            } else {
                                listner.onFailure(response.message());
                            }

                        }else {
                            listner.onFailure(response.message());
                        }


                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }


    public boolean checkValidation(String mobile) {
        boolean flag = true;
        if (mobile.isEmpty()) {
            flag = false;
            listner.onValidationFailed("Please enter mobile number");

        }
        if (!mobile.isEmpty()) {
            String initial = mobile.substring(0);
            if (mobile.startsWith("9") || mobile.startsWith("8") || mobile.startsWith("7") || mobile.startsWith("6")) {

            } else {
                flag = false;
                listner.onValidationFailed("Please enter valid mobile number");
            }
        }


        return flag;
    }
}
