package com.karyamitr.workseekers.viewmodel;


import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseModel;
import com.karyamitr.workseekers.utils.CallBackHelper;

import retrofit2.Call;
import retrofit2.Response;

public class FilterViewModel extends ViewModel {

    private static final String TAG = FilterViewModel.class.getSimpleName();
    private ResultCallBack listner;

    public FilterViewModel() {

    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void callApi(JsonObject filter) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.applyFilter(filter);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseModel data = (ResponseModel) response.body();
                        if (data != null) {
                            if (data.isSuccess()) {
                                listner.onSuccess(data);
                            } else {

                            }
                        }

                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }


    public boolean checkValidation(String mobile) {
        boolean flag = true;
        if (mobile.isEmpty()) {
            flag = false;
//            listner.onValidationFailed(R.string.emptyMobileNumber);

        }



        return flag;
    }
}
