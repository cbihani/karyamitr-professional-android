package com.karyamitr.workseekers.viewmodel;


import androidx.lifecycle.ViewModel;

import android.util.Log;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.OtpResponse;
import com.karyamitr.workseekers.utils.CallBackHelper;

import retrofit2.Call;
import retrofit2.Response;

public class OtpViewModel extends ViewModel {

    private static final String TAG = OtpViewModel.class.getSimpleName();
    private ResultCallBack listner;

    public OtpViewModel() {

    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void callApi(String uuid, String otp) {
        ApiClient.getUuid(uuid);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JsonObject lData = new JsonObject();
        JsonObject lJson = new JsonObject();
        lJson.addProperty("otp", otp);
        lData.add("user", lJson);

        Call call = apiService.confirmOtp(lData);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    Log.d(TAG, "onResponse: " + response.body().toString());
                    OtpResponse data = (OtpResponse) response.body();
                    if (data != null) {
                        if (data.isSuccess()) {
                            listner.onSuccess(data);
                        } else {
                            listner.onFailure(data.getMessage());
                        }
                    }else {
                        listner.onFailure(response.message());
                    }

                }else {
                    listner.onFailure("fail");
                }

            }
        });

    }

    public void callApiForResendOtp() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call call = apiService.resend_otp();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
//                    Log.d(TAG, "onResponse: " + response.body().toString());
//                    ResponseData data = (ResponseData) response.body();
                    OtpResponse res = (OtpResponse) response.body();
                    if (res != null) {
                        if (res.isSuccess()) {
                            listner.onSuccess(res);
                        } else {
                            listner.onFailure(res.getMessage());
                        }
                    }
                }
            }
        });

    }

    public boolean checkValidation(String otp) {
        boolean flag = true;
        if (otp.isEmpty()) {
            flag = false;
            listner.onValidationFailed("Please enter OTP");

        }
        return flag;
    }

}
