package com.karyamitr.workseekers.viewmodel;

import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.CallBackHelper;

import retrofit2.Call;
import retrofit2.Response;

public class JobDetailsViewModel extends ViewModel {

    private static final String TAG = HomeViewModel.class.getSimpleName();
    private ResultCallBack listner;

    ProfileInterface profileInterface;

    public JobDetailsViewModel() {

    }

    public interface ProfileInterface {

        void getProfessionalDetails(Object professional);

    }

    public void ProfileInterface(ProfileInterface profileInterface) {
        this.profileInterface = profileInterface;
    }

    public void getListners(ResultCallBack resultCallBack) {
        listner = resultCallBack;
    }

    public void callApi(JsonObject jobApplication) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.applyJob(jobApplication);
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {

                        listner.onSuccess(response.body());

                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }

    public void getJobUtilities() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getUtilities();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        ResponseData data = (ResponseData) response.body();

                        listner.onSuccess(data.getJob());

                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }

    public void callGetProfile() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getProfile();
        call.enqueue(new CallBackHelper(listner) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    if (response.code() == 200) {
                        Professional body = (Professional) response.body();
                        if (body != null) {
                            if (body.isSuccess()) {
                                profileInterface.getProfessionalDetails(response.body());

                            } else {
                                profileInterface.getProfessionalDetails(response.body());
                            }
                        } else {
                            listner.onFailure("Faild");
                        }


                    } else {
                        listner.onFailure(response.message());
                    }
                } else {

                    listner.onFailure(response.message());
                }
            }
        });

    }
}
