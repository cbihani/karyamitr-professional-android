package com.karyamitr.workseekers;

import android.app.Application;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import androidx.room.Room;

import com.crashlytics.android.Crashlytics;
import com.google.android.libraries.places.api.model.Place;
import com.karyamitr.workseekers.database.AppRoomDatabase;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyApplication extends Application {
    private static Context context;

    public static AppRoomDatabase roomDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = this;
        roomDatabase = Room.databaseBuilder(context, AppRoomDatabase.class, "Karyamitr").allowMainThreadQueries().build();
    }

    public static String getAddress(Place place) {
        double latitude = place.getLatLng().latitude;
        double longitude = place.getLatLng().longitude;
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                result.append(place.getName() + " , ");
                //  result.append(addresses.get(0).getAddressLine(0));
                // result.append(addresses.get(0).getLocality());
                //  result.append(addresses.get(0).getAdminArea());
                // result.append(addresses.get(0).getCountryName());
                if (addresses.get(0).getPostalCode() != null)
                    result.append(addresses.get(0).getPostalCode());
                //result.append(addresses.get(0).getFeatureName());
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

    public static boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[6-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

}
