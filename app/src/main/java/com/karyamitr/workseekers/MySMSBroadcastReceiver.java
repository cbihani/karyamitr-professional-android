package com.karyamitr.workseekers;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

public class MySMSBroadcastReceiver extends BroadcastReceiver {
    public static final int SMS_CONSENT_REQUEST = 2;
    private static Action action;

    public MySMSBroadcastReceiver() {
    }

//    public MySMSBroadcastReceiver(Action action) {
//        this.action = action;
//    }

    public static void setAction(Action action) {
        MySMSBroadcastReceiver.action = action;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("onReceive", "---" + intent.getAction());
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            Log.e("status.getStatusCode ", "--" + status.getStatusCode());
            Log.e("onRecive ", "--" + SmsRetriever.EXTRA_CONSENT_INTENT);
            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    // Get SMS message contents
//                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
//                    Log.e("OtpVerification message", "--" + message);
//                    Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
//                    Log.e("OtpVerification message", "--" + consentIntent.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE));
//
//                    try {
////                        startActivityForResult(consentIntent, SMS_CONSENT_REQUEST);
//                    } catch (ActivityNotFoundException e) {
//                        // Handle the exception ...
//                    }
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server.
                    Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                    try {
                        // Start activity to show consent dialog to user, activity must be started in
                        // 5 minutes, otherwise you'll receive another TIMEOUT intent

                        action.onSucessSMS(consentIntent, SMS_CONSENT_REQUEST);
                    } catch (ActivityNotFoundException e) {
                        // Handle the exception ...
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                    Log.e("OtpVerification TIMEOUT", "--Waiting for SMS timed out");

                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    break;
            }
        }
    }

    public interface Action {
        public void onSucessSMS(Intent intent, int str);
    }
}