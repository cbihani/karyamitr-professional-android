package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResponseData implements Serializable {


    @Expose
    @SerializedName("success")
    private boolean success;

    @Expose
    @SerializedName("data")
    private List<UserResponse> data;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("user_token")
    private String userToken;

    @Expose
    @SerializedName("posts")
    private List<Posts> posts;

    @Expose
    @SerializedName("job")
    private Job job;

    @Expose
    @SerializedName("skill_sets")
    private List<SkillSets> skill_sets;

    @Expose
    @SerializedName("job_applications")
    private List<JobApplications> job_applications;


    public List<SkillSets> getSkill_sets() {
        return skill_sets;
    }

    public void setSkill_sets(List<SkillSets> skill_sets) {
        this.skill_sets = skill_sets;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public List<JobApplications> getJob_applications() {
        return job_applications;
    }

    public void setJob_applications(List<JobApplications> job_applications) {
        this.job_applications = job_applications;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<UserResponse> getData() {
        return data;
    }

    public void setData(List<UserResponse> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public List<Posts> getPosts() {
        return posts;
    }

    public void setPosts(List<Posts> posts) {
        this.posts = posts;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "success=" + success +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", userToken='" + userToken + '\'' +
                ", posts=" + posts +
                ", job=" + job +
                ", skill_sets=" + skill_sets +
                ", job_applications=" + job_applications +
                '}';
    }
}
