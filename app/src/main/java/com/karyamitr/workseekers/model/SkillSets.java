package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkillSets extends ResponseData implements Serializable {
    @Expose
    @SerializedName("public_id")
    private String public_id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("colored_icon")
    private String colored_icon;

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColored_icon() {
        return colored_icon;
    }

    public void setColored_icon(String colored_icon) {
        this.colored_icon = colored_icon;
    }

    @Override
    public String toString() {
        return "SkillSets{" +
                "public_id='" + public_id + '\'' +
                ", name='" + name + '\'' +
                ", colored_icon='" + colored_icon + '\'' +
                '}';
    }
}
