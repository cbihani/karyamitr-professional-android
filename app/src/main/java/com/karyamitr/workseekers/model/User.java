package com.karyamitr.workseekers.model;

public class User {

    private String mobilenumber;
    private String role;

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "user{" +
                "mobilenumber='" + mobilenumber + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
