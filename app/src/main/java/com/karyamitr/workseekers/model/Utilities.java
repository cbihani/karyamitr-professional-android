package com.karyamitr.workseekers.model;

import java.io.Serializable;

public class Utilities implements Serializable {

    String name;
    boolean value = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

}
