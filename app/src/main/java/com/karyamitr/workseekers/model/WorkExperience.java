package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WorkExperience implements Serializable {

    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("company")
    private String company;
    @Expose
    @SerializedName("city")
    private String city;
    @Expose
    @SerializedName("working_presently")
    private boolean working_presently;
    @Expose
    @SerializedName("joining_year")
    private String joining_year;
    @Expose
    @SerializedName("relieving_year")
    private String relieving_year;
    @Expose
    @SerializedName("public_id")
    String public_id;

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isWorking_presently() {
        return working_presently;
    }

    public void setWorking_presently(boolean working_presently) {
        this.working_presently = working_presently;
    }

    public String getJoining_year() {
        return joining_year;
    }

    public void setJoining_year(String joining_year) {
        this.joining_year = joining_year;
    }

    public String getRelieving_year() {
        return relieving_year;
    }

    public void setRelieving_year(String relieving_year) {
        this.relieving_year = relieving_year;
    }

    @Override
    public String toString() {
        return "WorkExperience{" +
                "title='" + title + '\'' +
                ", company='" + company + '\'' +
                ", city='" + city + '\'' +
                ", working_presently=" + working_presently +
                ", joining_year='" + joining_year + '\'' +
                ", relieving_year='" + relieving_year + '\'' +
                ", public_id='" + public_id + '\'' +
                '}';
    }
}
