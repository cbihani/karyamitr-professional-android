package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResponseModel implements Serializable {

    @Expose
    @SerializedName("success")
    private boolean success;

    @Expose
    @SerializedName("jobs")
    private List<Jobs> data;

    @Expose
    @SerializedName("message")
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Jobs> getData() {
        return data;
    }

    public void setData(List<Jobs> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
