package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Professional extends ResponseData implements Serializable {
    @Expose
    @SerializedName("first_name")
    private String first_name;
    @Expose
    @SerializedName("last_name")
    private String last_name;
    @Expose
    @SerializedName("current_address")
    private String current_address;
    @Expose
    @SerializedName("permanent_address")
    private String permanent_address;
    @Expose
    @SerializedName("alternate_contact_number")
    private String alternate_contact_number;
    @Expose
    @SerializedName("gender")
    private String gender;
    @Expose
    @SerializedName("date_of_birth")
    private String date_of_birth;
    @Expose
    @SerializedName("dob")
    private String dob;
    @Expose
    @SerializedName("is_fresher")
    private boolean is_fresher;
    @Expose
    @SerializedName("total_experience")
    private String total_experience;
    @Expose
    @SerializedName("current_salary")
    private String current_salary;
    @Expose
    @SerializedName("expected_salary")
    private String expected_salary;
    @Expose
    @SerializedName("profile_pic")
    private String profile_pic;
    @Expose
    @SerializedName("profile_pic_url")
    private String profile_pic_url;
    @Expose
    @SerializedName("languages")
    private List<Languages> languages;
    @Expose
    @SerializedName("skills")
    private List<SkillsModel> skills;
    @Expose
    @SerializedName("work_experiences")
    private List<WorkExperience> work_experiences;
    @Expose
    @SerializedName("qualifications")
    private List<Qualification> qualifications;
    @Expose
    @SerializedName("profile_details")
    private ProfileDetails profile_details;


    public ProfileDetails getProfile_details() {
        return profile_details;
    }

    public void setProfile_details(ProfileDetails profile_details) {
        this.profile_details = profile_details;
    }

    public List<WorkExperience> getWork_experiences() {
        return work_experiences;
    }

    public void setWork_experiences(List<WorkExperience> work_experiences) {
        this.work_experiences = work_experiences;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<SkillsModel> getSkills() {
        return skills;
    }

    public void setSkills(List<SkillsModel> skills) {
        this.skills = skills;
    }

    public String getCurrent_address() {
        return current_address;
    }

    public void setCurrent_address(String current_address) {
        this.current_address = current_address;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getAlternate_contact_number() {
        return alternate_contact_number;
    }

    public void setAlternate_contact_number(String alternate_contact_number) {
        this.alternate_contact_number = alternate_contact_number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public boolean isIs_fresher() {
        return is_fresher;
    }

    public void setIs_fresher(boolean is_fresher) {
        this.is_fresher = is_fresher;
    }

    public String getTotal_experience() {
        return total_experience;
    }

    public void setTotal_experience(String total_experience) {
        this.total_experience = total_experience;
    }

    public String getCurrent_salary() {
        return current_salary;
    }

    public void setCurrent_salary(String current_salary) {
        this.current_salary = current_salary;
    }

    public String getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(String expected_salary) {
        this.expected_salary = expected_salary;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public List<Languages> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Languages> languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        return "Professional{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", current_address='" + current_address + '\'' +
                ", permanent_address='" + permanent_address + '\'' +
                ", alternate_contact_number='" + alternate_contact_number + '\'' +
                ", gender='" + gender + '\'' +
                ", date_of_birth='" + date_of_birth + '\'' +
                ", dob='" + dob + '\'' +
                ", is_fresher=" + is_fresher +
                ", total_experience='" + total_experience + '\'' +
                ", current_salary='" + current_salary + '\'' +
                ", expected_salary='" + expected_salary + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                ", profile_pic_url='" + profile_pic_url + '\'' +
                ", languages=" + languages +
                ", skills=" + skills +
                ", work_experiences=" + work_experiences +
                ", qualifications=" + qualifications +
                ", profile_details=" + profile_details +
                '}';
    }
}
