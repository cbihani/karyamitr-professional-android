package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobApplications implements Serializable {


    @Expose
    @SerializedName("public_id")
    String public_id;
    @Expose
    @SerializedName("status")
    String status;
    @Expose
    @SerializedName("schedules")
    String schedules;
    @Expose
    @SerializedName("created_at")
    String created_at;
    @Expose
    @SerializedName("job")
    Jobs jobs;
    @Expose
    @SerializedName("job_specific_answers")
    ApplicantDocumentList applicantDocumentList;
    @Expose
    @SerializedName("shortlisted")
    boolean shortlisted;

    public boolean isShortlisted() {
        return shortlisted;
    }

    public void setShortlisted(boolean shortlisted) {
        this.shortlisted = shortlisted;
    }

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSchedules() {
        return schedules;
    }

    public void setSchedules(String schedules) {
        this.schedules = schedules;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Jobs getJobs() {
        return jobs;
    }

    public void setJobs(Jobs jobs) {
        this.jobs = jobs;
    }

    public ApplicantDocumentList getApplicantDocumentList() {
        return applicantDocumentList;
    }

    public void setApplicantDocumentList(ApplicantDocumentList applicantDocumentList) {
        this.applicantDocumentList = applicantDocumentList;
    }

    @Override
    public String toString() {
        return "JobApplications{" +
                "public_id='" + public_id + '\'' +
                ", status='" + status + '\'' +
                ", schedules='" + schedules + '\'' +
                ", created_at='" + created_at + '\'' +
                ", jobs=" + jobs +
                ", applicantDocumentList=" + applicantDocumentList +
                ", shortlisted=" + shortlisted +
                '}';
    }
}
