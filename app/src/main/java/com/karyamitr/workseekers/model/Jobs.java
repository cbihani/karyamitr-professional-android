package com.karyamitr.workseekers.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;

@Entity(tableName = "jobs")
public class Jobs implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Long id;

    @Expose
    @SerializedName("title")
    String title;
    @Expose
    @SerializedName("employment_type")
    String employment_type;
    @Expose
    @SerializedName("salary")
    String salary;
    @Expose
    @SerializedName("location")
    String location;
    @Expose
    @SerializedName("minimum_salary")
    String minimum_salary;
    @Expose
    @SerializedName("maximum_salary")
    String maximum_salary;
    @Expose
    @SerializedName("minimum_experience")
    String minimum_experience;
    @Expose
    @SerializedName("maximum_experience")
    String maximum_experience;
    @Expose
    @SerializedName("primary_skill")
    String primary_skill;
    @Expose
    @SerializedName("secondary_skill")
    String secondary_skill;
    @Expose
    @SerializedName("public_id")
    String public_id;
    @Expose
    @SerializedName("functional_area")
    String functional_area;
    @Expose
    @SerializedName("shift")
    String shift;
    @Expose
    @SerializedName("description")
    String description;

    @Ignore
    @Expose
    @SerializedName("company")
    Company company;

    @Ignore
    @Expose
    @SerializedName("details")
    Details details;

    @Ignore
    @Expose
    @SerializedName("benefits")
    HashMap<String, Boolean> benefits;

    @Ignore
    @Expose
    @SerializedName("applicant_document_list")
    ApplicantDocumentList applicantDocumentList;

    @Ignore
    @Expose
    @SerializedName("interview_details")
    InterviewDetails interview_details;

    @Ignore
    @Expose
    @SerializedName("job_application")
    JobApplications job_application;


    @NonNull
    public Long getId() {
        return id;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    public JobApplications getJob_application() {
        return job_application;
    }

    public void setJob_application(JobApplications job_application) {
        this.job_application = job_application;
    }

    public InterviewDetails getInterview_details() {
        return interview_details;
    }

    public void setInterview_details(InterviewDetails interview_details) {
        this.interview_details = interview_details;
    }

    public HashMap<String, Boolean> getBenefits() {
        return benefits;
    }

    public void setBenefits(HashMap<String, Boolean> benefits) {
        this.benefits = benefits;
    }

    public ApplicantDocumentList getApplicantDocumentList() {
        return applicantDocumentList;
    }

    public void setApplicantDocumentList(ApplicantDocumentList applicantDocumentList) {
        this.applicantDocumentList = applicantDocumentList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmployment_type() {
        return employment_type;
    }

    public void setEmployment_type(String employment_type) {
        this.employment_type = employment_type;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMinimum_salary() {
        return minimum_salary;
    }

    public void setMinimum_salary(String minimum_salary) {
        this.minimum_salary = minimum_salary;
    }

    public String getMaximum_salary() {
        return maximum_salary;
    }

    public void setMaximum_salary(String maximum_salary) {
        this.maximum_salary = maximum_salary;
    }

    public String getMinimum_experience() {
        return minimum_experience;
    }

    public void setMinimum_experience(String minimum_experience) {
        this.minimum_experience = minimum_experience;
    }

    public String getMaximum_experience() {
        return maximum_experience;
    }

    public void setMaximum_experience(String maximum_experience) {
        this.maximum_experience = maximum_experience;
    }

    public String getPrimary_skill() {
        return primary_skill;
    }

    public void setPrimary_skill(String primary_skill) {
        this.primary_skill = primary_skill;
    }

    public String getSecondary_skill() {
        return secondary_skill;
    }

    public void setSecondary_skill(String secondary_skill) {
        this.secondary_skill = secondary_skill;
    }

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getFunctional_area() {
        return functional_area;
    }

    public void setFunctional_area(String functional_area) {
        this.functional_area = functional_area;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "Jobs{" +
                "title='" + title + '\'' +
                ", employment_type='" + employment_type + '\'' +
                ", salary='" + salary + '\'' +
                ", location='" + location + '\'' +
                ", minimum_salary='" + minimum_salary + '\'' +
                ", maximum_salary='" + maximum_salary + '\'' +
                ", minimum_experience='" + minimum_experience + '\'' +
                ", maximum_experience='" + maximum_experience + '\'' +
                ", primary_skill='" + primary_skill + '\'' +
                ", secondary_skill='" + secondary_skill + '\'' +
                ", public_id='" + public_id + '\'' +
                ", functional_area='" + functional_area + '\'' +
                ", shift='" + shift + '\'' +
                ", description='" + description + '\'' +
                ", company=" + company +
                ", details=" + details +
                ", benefits=" + benefits +
                ", applicantDocumentList=" + applicantDocumentList +
                ", interview_details=" + interview_details +
                ", job_application=" + job_application +
                '}';
    }
}
