package com.karyamitr.workseekers.model;

import java.io.Serializable;

public class JobSpecificAnswers implements Serializable {

    boolean has_rc = false;
    boolean has_pan = false;
    boolean has_bike = false;
    boolean has_cycle = false;
    boolean has_aadhar = false;
    boolean document_check = false;
    boolean has_driving_license = false;
    boolean has_local_address_proof = false;

    public boolean isHas_rc() {
        return has_rc;
    }

    public void setHas_rc(boolean has_rc) {
        this.has_rc = has_rc;
    }

    public boolean isHas_pan() {
        return has_pan;
    }

    public void setHas_pan(boolean has_pan) {
        this.has_pan = has_pan;
    }

    public boolean isHas_bike() {
        return has_bike;
    }

    public void setHas_bike(boolean has_bike) {
        this.has_bike = has_bike;
    }

    public boolean isHas_cycle() {
        return has_cycle;
    }

    public void setHas_cycle(boolean has_cycle) {
        this.has_cycle = has_cycle;
    }

    public boolean isHas_aadhar() {
        return has_aadhar;
    }

    public void setHas_aadhar(boolean has_aadhar) {
        this.has_aadhar = has_aadhar;
    }

    public boolean isDocument_check() {
        return document_check;
    }

    public void setDocument_check(boolean document_check) {
        this.document_check = document_check;
    }

    public boolean isHas_driving_license() {
        return has_driving_license;
    }

    public void setHas_driving_license(boolean has_driving_license) {
        this.has_driving_license = has_driving_license;
    }

    public boolean isHas_local_address_proof() {
        return has_local_address_proof;
    }

    public void setHas_local_address_proof(boolean has_local_address_proof) {
        this.has_local_address_proof = has_local_address_proof;
    }

    @Override
    public String toString() {
        return "applicant_document_list{" +
                "has_rc=" + has_rc +
                ", has_pan=" + has_pan +
                ", has_bike=" + has_bike +
                ", has_cycle=" + has_cycle +
                ", has_aadhar=" + has_aadhar +
                ", document_check=" + document_check +
                ", has_driving_license=" + has_driving_license +
                ", has_local_address_proof=" + has_local_address_proof +
                '}';
    }
}
