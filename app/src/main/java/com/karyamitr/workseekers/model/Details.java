package com.karyamitr.workseekers.model;

import java.io.Serializable;

public class Details implements Serializable {

    String featured;
    String vacancies = "1";
    String is_walking;
    String skill_required;
    String interview_end_date;
    String interview_start_date;
    String maximum_monthly_salary;
    String minimum_monthly_salary;
    String qualification_required;
    String work_experience_required;

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getVacancies() {
        return vacancies;
    }

    public void setVacancies(String vacancies) {
        this.vacancies = vacancies;
    }

    public String getIs_walking() {
        return is_walking;
    }

    public void setIs_walking(String is_walking) {
        this.is_walking = is_walking;
    }

    public String getSkill_required() {
        return skill_required;
    }

    public void setSkill_required(String skill_required) {
        this.skill_required = skill_required;
    }

    public String getInterview_end_date() {
        return interview_end_date;
    }

    public void setInterview_end_date(String interview_end_date) {
        this.interview_end_date = interview_end_date;
    }

    public String getInterview_start_date() {
        return interview_start_date;
    }

    public void setInterview_start_date(String interview_start_date) {
        this.interview_start_date = interview_start_date;
    }

    public String getMaximum_monthly_salary() {
        return maximum_monthly_salary;
    }

    public void setMaximum_monthly_salary(String maximum_monthly_salary) {
        this.maximum_monthly_salary = maximum_monthly_salary;
    }

    public String getMinimum_monthly_salary() {
        return minimum_monthly_salary;
    }

    public void setMinimum_monthly_salary(String minimum_monthly_salary) {
        this.minimum_monthly_salary = minimum_monthly_salary;
    }

    public String getQualification_required() {
        return qualification_required;
    }

    public void setQualification_required(String qualification_required) {
        this.qualification_required = qualification_required;
    }

    public String getWork_experience_required() {
        return work_experience_required;
    }

    public void setWork_experience_required(String work_experience_required) {
        this.work_experience_required = work_experience_required;
    }


    @Override
    public String toString() {
        return "Details{" +
                "featured='" + featured + '\'' +
                ", vacancies='" + vacancies + '\'' +
                ", is_walking='" + is_walking + '\'' +
                ", skill_required='" + skill_required + '\'' +
                ", interview_end_date='" + interview_end_date + '\'' +
                ", interview_start_date='" + interview_start_date + '\'' +
                ", maximum_monthly_salary='" + maximum_monthly_salary + '\'' +
                ", minimum_monthly_salary='" + minimum_monthly_salary + '\'' +
                ", qualification_required='" + qualification_required + '\'' +
                ", work_experience_required='" + work_experience_required + '\'' +
                '}';
    }
}
