package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Posts implements Serializable {

    @Expose
    @SerializedName("public_id")
    private String public_id;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("header_image")
    private String header_image;
    @Expose
    @SerializedName("keywords")
    private String keywords;
    @Expose
    @SerializedName("content")
    private String content;
    @Expose
    @SerializedName("meta_description")
    private String meta_description;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("category")
    private String category;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("author")
    private String author;


    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeader_image() {
        return header_image;
    }

    public void setHeader_image(String header_image) {
        this.header_image = header_image;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public void setMeta_description(String meta_description) {
        this.meta_description = meta_description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    @Override
    public String toString() {
        return "Posts{" +
                "public_id='" + public_id + '\'' +
                ", title='" + title + '\'' +
                ", header_image='" + header_image + '\'' +
                ", keywords='" + keywords + '\'' +
                ", content='" + content + '\'' +
                ", meta_description='" + meta_description + '\'' +
                ", status='" + status + '\'' +
                ", category='" + category + '\'' +
                ", created_at='" + created_at + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
