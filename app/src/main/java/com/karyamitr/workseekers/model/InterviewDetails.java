package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InterviewDetails implements Serializable {

    @Expose
    @SerializedName("url")
    String url;
    @Expose
    @SerializedName("address")
    String address;
    @Expose
    @SerializedName("latitude")
    String latitude;
    @Expose
    @SerializedName("longitude")
    String longitude;
    @Expose
    @SerializedName("allow_contacting")
    String allow_contacting;
    @Expose
    @SerializedName("contact_person_name")
    String contact_person_name;
    @Expose
    @SerializedName("contact_person_number")
    String contact_person_number;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAllow_contacting() {
        return allow_contacting;
    }

    public void setAllow_contacting(String allow_contacting) {
        this.allow_contacting = allow_contacting;
    }

    public String getContact_person_name() {
        return contact_person_name;
    }

    public void setContact_person_name(String contact_person_name) {
        this.contact_person_name = contact_person_name;
    }

    public String getContact_person_number() {
        return contact_person_number;
    }

    public void setContact_person_number(String contact_person_number) {
        this.contact_person_number = contact_person_number;
    }

    @Override
    public String toString() {
        return "InterviewDetails{" +
                "url='" + url + '\'' +
                ", address='" + address + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", allow_contacting='" + allow_contacting + '\'' +
                ", contact_person_name='" + contact_person_name + '\'' +
                ", contact_person_number='" + contact_person_number + '\'' +
                '}';
    }
}
