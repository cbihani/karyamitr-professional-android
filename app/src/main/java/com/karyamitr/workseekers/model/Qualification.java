package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Qualification implements Serializable {

    @Expose
    @SerializedName("university")
    private String university;
    @Expose
    @SerializedName("degree")
    private String degree;
    @Expose
    @SerializedName("passing_year")
    private String passing_year;
    @Expose
    @SerializedName("course_start_year")
    private String course_start_year;
    @Expose
    @SerializedName("course_end_year")
    private String course_end_year;
    @Expose
    @SerializedName("pursuing")
    private boolean pursuing;

    @Expose
    @SerializedName("public_id")
    private String public_id;


    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getPassing_year() {
        return passing_year;
    }

    public void setPassing_year(String passing_year) {
        this.passing_year = passing_year;
    }

    public String getCourse_start_year() {
        return course_start_year;
    }

    public void setCourse_start_year(String course_start_year) {
        this.course_start_year = course_start_year;
    }

    public String getCourse_end_year() {
        return course_end_year;
    }

    public void setCourse_end_year(String course_end_year) {
        this.course_end_year = course_end_year;
    }

    public boolean isPursuing() {
        return pursuing;
    }

    public void setPursuing(boolean pursuing) {
        this.pursuing = pursuing;
    }

    @Override
    public String toString() {
        return "Qualification{" +
                "university='" + university + '\'' +
                ", degree='" + degree + '\'' +
                ", passing_year='" + passing_year + '\'' +
                ", course_start_year='" + course_start_year + '\'' +
                ", course_end_year='" + course_end_year + '\'' +
                ", pursuing=" + pursuing +
                ", public_id='" + public_id + '\'' +
                '}';
    }
}
