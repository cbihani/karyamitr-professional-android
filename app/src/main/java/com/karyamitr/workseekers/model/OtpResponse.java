package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtpResponse implements Serializable {

    //    {
//        "success": true,
//            "message": "Signed in successfully!",
//            "public_id": "UbBzkqhHjc8sEcxu_KeV",
//            "user_confirmed": false,
//            "authentication_token": "AcRTBU8yz7NZMktm1okL"
//    }

    @Expose
    @SerializedName("success")
    private boolean success;

    @Expose
    @SerializedName("user_confirmed")
    private boolean userConfirmed;


    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("authentication_token")
    private String authenticationToken;

    @Expose
    @SerializedName("public_id")
    private String publicId;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isUserConfirmed() {
        return userConfirmed;
    }

    public void setUserConfirmed(boolean userConfirmed) {
        this.userConfirmed = userConfirmed;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    @Override
    public String toString() {
        return "OtpResponse{" +
                "success=" + success +
                ", userConfirmed=" + userConfirmed +
                ", message='" + message + '\'' +
                ", authenticationToken='" + authenticationToken + '\'' +
                ", publicId='" + publicId + '\'' +
                '}';
    }
}
