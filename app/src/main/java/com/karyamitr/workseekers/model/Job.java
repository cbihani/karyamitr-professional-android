package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Job implements Serializable {
    @Expose
    @SerializedName("categories")
    private List<String> categories;

    @Expose
    @SerializedName("employment_types")
    private List<String> employment_types;

    @Expose
    @SerializedName("job_locations")
    private List<String> job_locations;

    @Expose
    @SerializedName("benefits_with_employeers")
    private List<String> benefits_with_employeers;

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getEmployment_types() {
        return employment_types;
    }

    public void setEmployment_types(List<String> employment_types) {
        this.employment_types = employment_types;
    }

    public List<String> getJob_locations() {
        return job_locations;
    }

    public void setJob_locations(List<String> job_locations) {
        this.job_locations = job_locations;
    }

    public List<String> getBenefits_with_employeers() {
        return benefits_with_employeers;
    }

    public void setBenefits_with_employeers(List<String> benefits_with_employeers) {
        this.benefits_with_employeers = benefits_with_employeers;
    }

    @Override
    public String toString() {
        return "Job{" +
                "categories=" + categories +
                ", employment_types=" + employment_types +
                ", job_locations=" + job_locations +
                ", benefits_with_employeers=" + benefits_with_employeers +
                '}';
    }
}
