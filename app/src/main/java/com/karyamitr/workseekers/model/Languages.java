package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Languages implements Serializable {


    @Expose
    @SerializedName("name")
    String name;

    @Expose
    @SerializedName("can_read")
    boolean can_read = false;

    @Expose
    @SerializedName("can_speak")
    boolean can_speak = false;

    @Expose
    @SerializedName("can_write")
    boolean can_write = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCan_read() {
        return can_read;
    }

    public void setCan_read(boolean can_read) {
        this.can_read = can_read;
    }

    public boolean isCan_speak() {
        return can_speak;
    }

    public void setCan_speak(boolean can_speak) {
        this.can_speak = can_speak;
    }

    public boolean isCan_write() {
        return can_write;
    }

    public void setCan_write(boolean can_write) {
        this.can_write = can_write;
    }

    @Override
    public String toString() {
        return "Languages{" +
                "name='" + name + '\'' +
                ", can_read=" + can_read +
                ", can_speak=" + can_speak +
                ", can_write=" + can_write +
                '}';
    }
}
