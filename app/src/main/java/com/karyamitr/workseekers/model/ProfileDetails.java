package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileDetails implements Serializable {
    @Expose
    @SerializedName("is_fresher")
    private boolean is_fresher;

    @Expose
    @SerializedName("skills_updated")
    private boolean skills_updated;

    @Expose
    @SerializedName("work_experience_updated")
    private boolean work_experience_updated;

    @Expose
    @SerializedName("qualification_updated")
    private boolean qualification_updated;

    @Expose
    @SerializedName("basic_details_updated")
    private boolean basic_details_updated;

    @Expose
    @SerializedName("pofile_pic_updated")
    private boolean pofile_pic_updated;

    @Expose
    @SerializedName("profile_completeness_percentage")
    private int profile_completeness_percentage;

    public boolean isIs_fresher() {
        return is_fresher;
    }

    public void setIs_fresher(boolean is_fresher) {
        this.is_fresher = is_fresher;
    }

    public boolean isSkills_updated() {
        return skills_updated;
    }

    public void setSkills_updated(boolean skills_updated) {
        this.skills_updated = skills_updated;
    }

    public boolean isWork_experience_updated() {
        return work_experience_updated;
    }

    public void setWork_experience_updated(boolean work_experience_updated) {
        this.work_experience_updated = work_experience_updated;
    }

    public boolean isQualification_updated() {
        return qualification_updated;
    }

    public void setQualification_updated(boolean qualification_updated) {
        this.qualification_updated = qualification_updated;
    }

    public boolean isBasic_details_updated() {
        return basic_details_updated;
    }

    public void setBasic_details_updated(boolean basic_details_updated) {
        this.basic_details_updated = basic_details_updated;
    }

    public boolean isPofile_pic_updated() {
        return pofile_pic_updated;
    }

    public void setPofile_pic_updated(boolean pofile_pic_updated) {
        this.pofile_pic_updated = pofile_pic_updated;
    }

    public int getProfile_completeness_percentage() {
        return profile_completeness_percentage;
    }

    public void setProfile_completeness_percentage(int profile_completeness_percentage) {
        this.profile_completeness_percentage = profile_completeness_percentage;
    }

    @Override
    public String toString() {
        return "ProfileDetails{" +
                "is_fresher=" + is_fresher +
                ", skills_updated=" + skills_updated +
                ", work_experience_updated=" + work_experience_updated +
                ", qualification_updated=" + qualification_updated +
                ", basic_details_updated=" + basic_details_updated +
                ", pofile_pic_updated=" + pofile_pic_updated +
                ", profile_completeness_percentage=" + profile_completeness_percentage +
                '}';
    }
}
