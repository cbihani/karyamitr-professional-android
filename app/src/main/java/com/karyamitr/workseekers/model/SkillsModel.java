package com.karyamitr.workseekers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkillsModel implements Serializable {

//    String name;
//    boolean isSelected=false;
//    int greyImage;
//    int colorImage;


    @Expose
    @SerializedName("skill_name")
    String skill_name;
    @Expose
    @SerializedName("currently_using")
    boolean currently_using;
    @Expose
    @SerializedName("last_used")
    String last_used;
    @Expose
    @SerializedName("experience")
    String experience;
    @Expose
    @SerializedName("skill_set_public_id")
    String skill_set_public_id;
    @Expose
    @SerializedName("public_id")
    String public_id;

    int greyImage;

    String colorImage;

    boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getSkill_name() {
        return skill_name;
    }

    public void setSkill_name(String skill_name) {
        this.skill_name = skill_name;
    }

    public boolean isCurrently_using() {
        return currently_using;
    }

    public void setCurrently_using(boolean currently_using) {
        this.currently_using = currently_using;
    }

    public String getLast_used() {
        return last_used;
    }

    public void setLast_used(String last_used) {
        this.last_used = last_used;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSkill_set_public_id() {
        return skill_set_public_id;
    }

    public void setSkill_set_public_id(String skill_set_public_id) {
        this.skill_set_public_id = skill_set_public_id;
    }

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public int getGreyImage() {
        return greyImage;
    }

    public void setGreyImage(int greyImage) {
        this.greyImage = greyImage;
    }

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }


    @Override
    public String toString() {
        return "Skills{" +
                "skill_name='" + skill_name + '\'' +
                ", currently_using=" + currently_using +
                ", last_used='" + last_used + '\'' +
                ", experience='" + experience + '\'' +
                ", skill_set_public_id='" + skill_set_public_id + '\'' +
                ", public_id='" + public_id + '\'' +
                ", greyImage=" + greyImage +
                ", colorImage=" + colorImage +
                '}';
    }
}
