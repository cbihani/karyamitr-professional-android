package com.karyamitr.workseekers.model;

import java.io.Serializable;

public class Company implements Serializable {

    String name;
    String logo;
    String corporate_name;
    boolean verified;
    String public_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCorporate_name() {
        return corporate_name;
    }

    public void setCorporate_name(String corporate_name) {
        this.corporate_name = corporate_name;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", corporate_name='" + corporate_name + '\'' +
                ", verified=" + verified +
                ", public_id='" + public_id + '\'' +
                '}';
    }
}
