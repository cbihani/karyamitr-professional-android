package com.karyamitr.workseekers.utils;

public interface OnLoadMoreListener {
    void onLoadMore();
}