package com.karyamitr.workseekers.utils;

import android.util.Log;

import com.karyamitr.workseekers.api.ResultCallBack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallBackHelper implements Callback {

    private final ResultCallBack listner;
    private static String TAG= CallBackHelper.class.getSimpleName();

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {
        Log.d(TAG, "onFailure: "+ t.getMessage());

        listner.onFailure(""+t.getLocalizedMessage());

    }

    public CallBackHelper(ResultCallBack listner) {
        this.listner = listner;
    }
}
