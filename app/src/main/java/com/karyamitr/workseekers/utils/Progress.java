package com.karyamitr.workseekers.utils;

import android.app.ProgressDialog;
import android.content.Context;


public class Progress {

    private ProgressDialog progressBar;

    public static Progress customProgress = null;


    public static Progress getInstance() {
        if (customProgress == null) {
            customProgress = new Progress();
        }
        return customProgress;
    }


    public ProgressDialog showProgressDialog(Context context, String message) {
        progressBar = new ProgressDialog(context);
        progressBar.setMessage(message);
        //progressBar.getWindow().setBackgroundDrawableResource(R.color.transparent);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setCancelable(false);
        if (!progressBar.isShowing())
            progressBar.show();
        return progressBar;
    }


    public void hideProgressBar() {
        try {
            if (progressBar.isShowing())
                progressBar.dismiss();

        } catch (Exception ignored) {
        }
    }
}
