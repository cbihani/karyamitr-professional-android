package com.karyamitr.workseekers.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Prefs {

    private static final String PREF_LAUNCH_ACTIVITY = "launch_activity";
    private static final String USER_TOKEN = "user_token";
    private static final String AUTH_TOKEN = "token";
    private static final String USER_MOBILE = "mobile";
    private static final String ACTIVATION_CODE = "activation_code";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setLaunchActivity(Context ctx, String launch_activity) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_LAUNCH_ACTIVITY, launch_activity);
        editor.apply();
    }

    public static String getLaunchActivity(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_LAUNCH_ACTIVITY, "1");
    }


    public static void setFirstTimeLaunch(Context ctx, boolean isFirstTime) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.apply();

    }

    public static boolean isFirstTimeLaunch(Context ctx) {
        return getSharedPreferences(ctx).getBoolean(IS_FIRST_TIME_LAUNCH, true);

    }


    public static void setUserToken(Context ctx, String str) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_TOKEN, str);
        editor.apply();
    }

    public static String getUserToken(Context ctx) {
        return getSharedPreferences(ctx).getString(USER_TOKEN, "");
    }

    public static void setToken(Context ctx, String str) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(AUTH_TOKEN, str);
        editor.apply();
    }

    public static String getToken(Context ctx) {
        return getSharedPreferences(ctx).getString(AUTH_TOKEN, "");
    }

    public static void setUserMobile(Context ctx, String str) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_MOBILE, str);
        editor.apply();
    }

    public static String getUserMobile(Context ctx) {
        return getSharedPreferences(ctx).getString(USER_MOBILE, "");
    }


    public static void setActivationCode(Context ctx, String str) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(ACTIVATION_CODE, str);
        editor.apply();
    }

    public static String getActivationCode(Context ctx) {
        return getSharedPreferences(ctx).getString(ACTIVATION_CODE, "");
    }
}