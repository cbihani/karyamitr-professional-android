package com.karyamitr.workseekers.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {
    Boolean b;
    String otp;
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {

            Bundle data = intent.getExtras();
            if (data != null) {
                //---retrieve the SMS message received---
                try {

                    Object[] pdus = (Object[]) data.get("pdus");
                    for (int i = 0; i < pdus.length; i++) {
                        SmsMessage smsMessage;
                        if (Build.VERSION.SDK_INT <= 22) {
                            smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        } else {
                            smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i], data.getString("format"));
                        }
                        String sender = smsMessage.getDisplayOriginatingAddress();

                        String messageBody = smsMessage.getMessageBody();
                        otp = messageBody.replaceAll("[^0-9]", "");   // here abcd contains otp

                        //Pass on the text to our listener.
                        mListener.messageReceived(otp);  // attach value to interface


//                        final String DISPLAY_MESSAGE_ACTION = context.getPackageName() + ".CodeSmsReceived";
//                        Intent intentCodeSms = new Intent(DISPLAY_MESSAGE_ACTION);
//                        intentCodeSms.putExtra("varificationCode", otp);
//                        context.sendBroadcast(intentCodeSms);

                    }
                } catch (Exception e) {
                    Log.d("Exception caught", e.getMessage());
                }
            }
        }
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

}
