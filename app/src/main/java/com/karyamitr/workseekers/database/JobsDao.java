package com.karyamitr.workseekers.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.karyamitr.workseekers.model.Jobs;

import java.util.List;

@Dao
public interface JobsDao {

    @Insert
    void insert(Jobs jobs);

    @Query("SELECT * from jobs ORDER BY id DESC")
    List<Jobs> getAllJobs();
}
