package com.karyamitr.workseekers.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.karyamitr.workseekers.model.Jobs;

@Database(entities = {Jobs.class}, version = 1,exportSchema = false)
public abstract class AppRoomDatabase extends RoomDatabase {

    public abstract JobsDao jobDao();


}