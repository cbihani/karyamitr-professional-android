package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.WorkExperience;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ExperienceRecyclerViewAdapter extends RecyclerView.Adapter<ExperienceRecyclerViewAdapter.ViewHolder> {

    private List<WorkExperience> experienceList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;


    Context context;

    // data is passed into the constructor
    public ExperienceRecyclerViewAdapter(Context context, List<WorkExperience> experienceList) {
        this.mInflater = LayoutInflater.from(context);
        this.experienceList = experienceList;
        this.context = context;

    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.qualification_rv_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        final WorkExperience experience = experienceList.get(position);
        holder.city.setVisibility(View.VISIBLE);
        holder.designationName.setText(experienceList.get(position).getTitle());
        holder.companyName.setText(experienceList.get(position).getCompany());
        holder.city.setText(experienceList.get(position).getCity());
        holder.fromAndToYear.setText(experienceList.get(position).getJoining_year() + " - " + experienceList.get(position).getRelieving_year());
        if (experienceList.get(position).isWorking_presently())
            holder.isCurrentlyWorking.setText(R.string.working_here);
        else {
            holder.isCurrentlyWorking.setVisibility(View.GONE);
        }

        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(experienceList.get(position), position);

            }
        });

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return experienceList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.name)
        TextView designationName;

        @BindView(R.id.university)
        TextView companyName;

        @BindView(R.id.year)
        TextView fromAndToYear;

        @BindView(R.id.city)
        TextView city;

        @BindView(R.id.is_pursuing)
        TextView isCurrentlyWorking;

        @BindView(R.id.cardLayout_qualification)
        RelativeLayout cardLayout;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

    }

    public void getQualificationList(List<WorkExperience> experienceList) {
        this.experienceList = experienceList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ExperienceRecyclerViewAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(WorkExperience experienceList, int position);
    }


}