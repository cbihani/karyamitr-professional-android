package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.PlaceAPI;

import java.util.ArrayList;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    ArrayList<String> resultList;

    Context mContext;
    int mResource;

    PlaceAPI mPlaceAPI = new PlaceAPI();
    LoaderEvent loaderEvent;
    private int type;

    public PlacesAutoCompleteAdapter(Context context, int resource, int type, LoaderEvent loaderEvent) {
        super(context, resource);

        mContext = context;
        mResource = resource;
        this.type = type;
        this.loaderEvent = loaderEvent;

    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.spinner_item, null);
        TextView textSpinnerItem = view.findViewById(R.id.textSpinnerItem);
        if(position != 0 && resultList.size() !=0) {
            if (position != (resultList.size() - 1))
                textSpinnerItem.setText(resultList.get(position));
        }
        return view;
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View view;
//
//        //if (convertView == null) {
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        if (position != (resultList.size() - 1))
//            view = inflater.inflate(R.layout.autocomplete_list_item, null);
//        else
//            view = inflater.inflate(R.layout.autocomplete_list_item, null);
//
//
//
//
//        //}
//        //else {
//        //    view = convertView;
//        //}
//
////        if (position != (resultList.size() - 1)) {
////            TextView autocompleteTextView = (TextView) view.findViewById(R.id.autocompleteText);
////            autocompleteTextView.setText(resultList.get(position));
////        }
////        else {
////           // ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
////            // not sure what to do 😀
////        }
//
//        return view;
//    }


    /*
    {
   "error_message" : "You have exceeded your daily request quota for this API. If you did not set a custom daily request quota, verify your project has an active billing account: http://g.co/dev/maps-no-account",
   "predictions" : [],
   "status" : "OVER_QUERY_LIMIT"
}

     */
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                loaderEvent.showLoader(type);
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultList = mPlaceAPI.autocomplete(constraint.toString());


//                    resultList.add("footer");

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                loaderEvent.hideLoader(type);
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }


    public interface LoaderEvent {

        void hideLoader(int type);

        void showLoader(int type);
    }
}