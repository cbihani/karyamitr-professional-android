package com.karyamitr.workseekers.adapter;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.karyamitr.workseekers.view.fragments.BasicProfile;
import com.karyamitr.workseekers.view.fragments.LanguagesKnown;
import com.karyamitr.workseekers.view.fragments.QualificationFragment;
import com.karyamitr.workseekers.view.fragments.RegisterFragment;
import com.karyamitr.workseekers.view.fragments.RegistrationDone;
import com.karyamitr.workseekers.view.fragments.Skills;
import com.karyamitr.workseekers.view.fragments.WorkExperienceFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return RegisterFragment.newInstance(position, position == getCount());
            case 1:
                return BasicProfile.newInstance(position, position == getCount());
            case 2:
                return Skills.newInstance(position, position == getCount());
            case 3:
                return WorkExperienceFragment.newInstance(position, position == getCount());
            case 4:
                return QualificationFragment.newInstance(position, position == getCount());
            case 5:
                return LanguagesKnown.newInstance(position, position == getCount());
            case 6:
                return RegistrationDone.newInstance(position, position == getCount());
            default:
                return RegistrationDone.newInstance(position, position == getCount());
        }
    }

    @Override
    public int getCount() {
        return 7;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}
