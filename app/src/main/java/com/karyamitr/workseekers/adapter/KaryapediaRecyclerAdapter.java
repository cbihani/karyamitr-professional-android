package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Posts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KaryapediaRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<Posts> mPostItems;
    private List<Posts> contactListFiltered;
    private ItemClickListener mClickListener;
    Context context;

    public KaryapediaRecyclerAdapter(Context context, List<Posts> postItems) {
        this.context = context;
        this.mPostItems = postItems;
        this.contactListFiltered = postItems;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.karyapedia_item_view, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {

            populateItemRows((ItemViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return contactListFiltered == null ? 0 : contactListFiltered.size();
    }

    @Override
    public int getItemViewType(int position) {
        return contactListFiltered.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = mPostItems;
                } else {
                    List<Posts> filteredList = new ArrayList<>();
                    for (Posts row : mPostItems) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getMeta_description().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCreated_at().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getAuthor().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Posts>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.karyapedia_title)
        TextView karyapediaTitle;

        @BindView(R.id.karyapedia_date_author)
        TextView dateandAuthor;

        @BindView(R.id.meta_description)
        TextView metaDescription;

        @BindView(R.id.karyapedia_image)
        ImageView karyapediaImage;

        @BindView(R.id.job_item_layout)
        LinearLayout jobLayout;

        @BindView(R.id.job_list_card)
        CardView jobListCard;

        @BindView(R.id.button_read_more)
        Button readMore;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {

        Posts item = contactListFiltered.get(position);

        viewHolder.karyapediaTitle.setText(item.getTitle());
        Glide.with(context).load(item.getHeader_image()).into(viewHolder.karyapediaImage);
        viewHolder.metaDescription.setText(item.getMeta_description());
        if (item.getCreated_at() != null) {
            String date = item.getCreated_at().substring(0, 10);
            viewHolder.dateandAuthor.setText(date + " | " + item.getAuthor());
        } else {
            viewHolder.dateandAuthor.setText(item.getAuthor());
        }


        viewHolder.jobListCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(mPostItems, position);
            }
        });

        viewHolder.readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(mPostItems, position);
            }
        });

    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(List<Posts> jobList, int position);
    }

    public void filterList(List<Posts> filterdNames) {
        mPostItems = filterdNames;
        notifyDataSetChanged();
    }


}
