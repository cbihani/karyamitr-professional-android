package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Qualification;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.view.activity.EditProfileActivity;
import com.karyamitr.workseekers.view.activity.Home;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileQualificationRecyclerAdapter extends RecyclerView.Adapter<ProfileQualificationRecyclerAdapter.ViewHolder> {

    private List<Qualification> jobsList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    SkillsModel skillsModel;
    View view;

    Context context;

    // data is passed into the constructor
    public ProfileQualificationRecyclerAdapter(Context context, List<Qualification> jobsList) {
        this.mInflater = LayoutInflater.from(context);
        this.jobsList = jobsList;
        this.context = context;

        notifyDataSetChanged();

    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = mInflater.inflate(R.layout.profile_qualification_view, parent, false);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        holder.title.setText((position + 1) + ". " + jobsList.get(position).getDegree());
        holder.companyName.setText(jobsList.get(position).getUniversity());
        if (jobsList.get(position).isPursuing()) {
            holder.city.setText("Pursuing Presently");
        } else {
            holder.city.setText(jobsList.get(position).getCourse_start_year() + " - " + jobsList.get(position).getCourse_end_year());
        }
        holder.working_presently.setVisibility(View.GONE);

//        holder.city.setText(jobsList.get(position).getCourse_start_year() + " " + jobsList.get(position).getCourse_end_year());
//        holder.working_presently.setText(jobsList.get(position).get);

        holder.qualificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Home)
                    ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                Intent in = new Intent(context, EditProfileActivity.class);
                in.putExtra("data", jobsList.get(position));
                in.putExtra("Type", "Qualification");
                context.startActivity(in);
            }
        });

        holder.ivDeleteQualification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
                alertDialogBuilder.setMessage("Are you sure you want to delete?");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                mClickListener.onQualificationDelete(jobsList.get(position));
                            }
                        });

                alertDialogBuilder.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

    }


//    }

    // total number of cells
    @Override
    public int getItemCount() {
        return jobsList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.companyName)
        TextView companyName;

        @BindView(R.id.city)
        TextView city;

        @BindView(R.id.working_presently)
        TextView working_presently;

        @BindView(R.id.qualificationLayout)
        LinearLayout qualificationLayout;

        @BindView(R.id.ivDeleteQualification)
        ImageView ivDeleteQualification;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    public void setSkillImageList(List<Qualification> skillsModelList) {
        this.jobsList = skillsModelList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onQualificationDelete(Qualification qualification);
    }
}