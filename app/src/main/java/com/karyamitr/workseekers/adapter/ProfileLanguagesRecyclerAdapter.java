package com.karyamitr.workseekers.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Languages;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.view.activity.Home;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileLanguagesRecyclerAdapter extends RecyclerView.Adapter<ProfileLanguagesRecyclerAdapter.ViewHolder> {

    private List<Languages> languageList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    SkillsModel skillsModel;

    View view;
    Context context;

    // data is passed into the constructor
    public ProfileLanguagesRecyclerAdapter(Context context, List<Languages> languageList) {
        this.mInflater = LayoutInflater.from(context);
        this.languageList = languageList;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = mInflater.inflate(R.layout.profile_languages_view, parent, false);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.title.setText(languageList.get(position).getName());
        if (languageList.get(position).isCan_read()) {
            holder.read.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_green_24dp, 0, 0, 0);
        } else {
            holder.read.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_black_24dp, 0, 0, 0);
        }

        if (languageList.get(position).isCan_speak()) {
            holder.speak.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_green_24dp, 0, 0, 0);
        } else {
            holder.speak.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_black_24dp, 0, 0, 0);
        }

        if (languageList.get(position).isCan_write()) {
            holder.write.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_green_24dp, 0, 0, 0);
        } else {
            holder.write.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_black_24dp, 0, 0, 0);
        }
        // holder.read.setText("" + languageList.get(position).isCan_read());
        //  holder.write.setText("" + languageList.get(position).isCan_write());
        //holder.speak.setText("" + languageList.get(position).isCan_speak());
        holder.action.setImageResource(R.drawable.ic_delete_black_24dp);
        holder.languageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Home)
                    ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.language_dialog);
                TextView tvDialogHeader = dialog.findViewById(R.id.tvDialogHeader);
                tvDialogHeader.setText(languageList.get(position).getName());
                TextView tvDialogSave = dialog.findViewById(R.id.tvDialogSave);
                CheckBox checkbox_read, checkbox_write, checkbox_speak;
                checkbox_read = dialog.findViewById(R.id.checkbox_read);
                checkbox_read.setChecked(languageList.get(position).isCan_read());
                checkbox_write = dialog.findViewById(R.id.checkbox_write);
                checkbox_write.setChecked(languageList.get(position).isCan_write());
                checkbox_speak = dialog.findViewById(R.id.checkbox_speak);
                checkbox_speak.setChecked(languageList.get(position).isCan_speak());

                checkbox_read.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        languageList.get(position).setCan_read(isChecked);
                        notifyDataSetChanged();
                    }
                });
                checkbox_write.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        languageList.get(position).setCan_write(isChecked);
                        notifyDataSetChanged();

                    }
                });
                checkbox_speak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        languageList.get(position).setCan_speak(isChecked);
                        notifyDataSetChanged();

                    }
                });

                tvDialogSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (context instanceof Home)
                            ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                        dialog.dismiss();
                        mClickListener.onItemClick(languageList.get(position), position);
                    }
                });
                dialog.show();
            }
        });

        holder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
                alertDialogBuilder.setMessage("Are you sure you want to delete?");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                mClickListener.onItemClick(languageList.get(position), -1);
                            }
                        });

                alertDialogBuilder.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return languageList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lang_title)
        TextView title;

        @BindView(R.id.languages_read)
        TextView read;

        @BindView(R.id.languages_write)
        TextView write;

        @BindView(R.id.languages_speak)
        TextView speak;

        @BindView(R.id.languages_action)
        ImageView action;

        @BindView(R.id.languageLayout)
        LinearLayout languageLayout;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    public void setSkillImageList(List<Languages> skillsModelList) {
        this.languageList = skillsModelList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(Languages jobList, int position);
    }
}