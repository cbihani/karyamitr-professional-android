package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Jobs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder> {

    private List<Jobs> jobsList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;


    Context context;

    // data is passed into the constructor
    public HomeRecyclerAdapter(Context context, List<Jobs> jobsList) {
        this.mInflater = LayoutInflater.from(context);
        this.jobsList = jobsList;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.demo_item_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.jobTitle.setText(jobsList.get(position).getTitle());
        holder.jobCompany.setText(jobsList.get(position).getCompany().getName());
        holder.jobExperience.setText(Html.fromHtml("<b> " + jobsList.get(position).getMinimum_experience() + " - " + jobsList.get(position).getMaximum_experience() + "</b>"));
        holder.jobLocation.setText(Html.fromHtml("<b> " + jobsList.get(position).getLocation() + " </b>"));
        holder.jobSalary.setText(Html.fromHtml("<b> " + jobsList.get(position).getMinimum_salary() + " - " + jobsList.get(position).getMaximum_salary()));
        holder.jobType.setText(Html.fromHtml("Job type: " + "<b>" + jobsList.get(position).getEmployment_type() + "</b>"));

        holder.jobListCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(jobsList.get(position), position);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return jobsList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.job_title)
        TextView jobTitle;

        @BindView(R.id.job_location)
        TextView jobLocation;

        @BindView(R.id.job_experience)
        TextView jobExperience;

        @BindView(R.id.job_salary)
        TextView jobSalary;

        @BindView(R.id.job_type)
        TextView jobType;

        @BindView(R.id.job_company)
        TextView jobCompany;

        @BindView(R.id.job_item_layout)
        LinearLayout jobLayout;

        @BindView(R.id.job_list_card)
        CardView jobListCard;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public void setSkillImageList(List<Jobs> skillsModelList) {
        this.jobsList = skillsModelList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(Jobs jobList, int position);
    }
}