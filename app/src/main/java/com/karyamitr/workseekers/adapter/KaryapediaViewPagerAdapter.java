package com.karyamitr.workseekers.adapter;


import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Posts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KaryapediaViewPagerAdapter extends RecyclerView.Adapter<KaryapediaViewPagerAdapter.ViewHolder> {

    private List<Posts> experienceList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    View view;
    Context context;

    // data is passed into the constructor
    public KaryapediaViewPagerAdapter(Context context, List<Posts> experienceList) {
        this.mInflater = LayoutInflater.from(context);
        this.experienceList = experienceList;
        this.context = context;


    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = mInflater.inflate(R.layout.activity_karyapedia_details, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        final Posts posts = experienceList.get(position);

        Glide.with(context).load(posts.getHeader_image()).into(holder.karyapediaIcon);
        holder.title.setText(posts.getTitle());
        holder.karyapediaMetaDescription.setText(posts.getMeta_description());
        holder.authorAndDate.setText(posts.getAuthor());
        holder.karyapediaDescription.setText(Html.fromHtml(posts.getContent()));
        holder.viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).onBackPressed();

            }
        });


    }

    // total number of cells
    @Override
    public int getItemCount() {
        return experienceList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.karyapedia_icon)
        ImageView karyapediaIcon;

        @BindView(R.id.title_karyapedia)
        TextView title;

        @BindView(R.id.author_date)
        TextView authorAndDate;

        @BindView(R.id.karyapedia_meta_description)
        TextView karyapediaMetaDescription;

        @BindView(R.id.karyapedia_description)
        TextView karyapediaDescription;

        @BindView(R.id.button_view_all)
        Button viewAll;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

    }

    public void getQualificationList(List<Posts> experienceList) {
        this.experienceList = experienceList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(KaryapediaViewPagerAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(Posts experienceList, int position);
    }
}
