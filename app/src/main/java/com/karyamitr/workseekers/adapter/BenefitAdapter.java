package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.karyamitr.workseekers.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BenefitAdapter extends RecyclerView.Adapter<BenefitAdapter.ViewHolder> {

    private List<String> benefitsList;
    private LayoutInflater mInflater;
    private Context context;

    // data is passed into the constructor
    public BenefitAdapter(Context context, List<String> benefits) {
        this.mInflater = LayoutInflater.from(context);
        this.benefitsList = benefits;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.beneifit_item, parent, false);
        return new BenefitAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.chipBenefit.setText(benefitsList.get(position));
    }

    @Override
    public int getItemCount() {
        return benefitsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.chipBenefit)
        Chip chipBenefit;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
