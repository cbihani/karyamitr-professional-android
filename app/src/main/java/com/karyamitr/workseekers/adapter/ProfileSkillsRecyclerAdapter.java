package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.view.activity.EditProfileActivity;
import com.karyamitr.workseekers.view.activity.Home;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileSkillsRecyclerAdapter extends RecyclerView.Adapter<ProfileSkillsRecyclerAdapter.ViewHolder> {

    private List<SkillsModel> jobsList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    SkillsModel skillsModel;
    View view;

    Context context;

    // data is passed into the constructor
    public ProfileSkillsRecyclerAdapter(Context context, List<SkillsModel> jobsList) {
        this.mInflater = LayoutInflater.from(context);
        this.jobsList = jobsList;
        this.context = context;


    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = mInflater.inflate(R.layout.profile_skills_view, parent, false);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.logo_grey);
        requestOptions.error(R.drawable.logo_grey);

//        holder.image.setImageResource(R.drawable.maid);
        holder.title.setText(jobsList.get(position).getSkill_name());
        if (jobsList.get(position).getExperience() != null) {
            holder.experience.setText(jobsList.get(position).getExperience() + " years");
        } else {
            holder.experience.setText("-");
        }
        if (jobsList.get(position).getLast_used() != null) {
            holder.lastUsed.setText(jobsList.get(position).getLast_used());
        } else {
            holder.lastUsed.setText("");
        }

        holder.currently.setText("" + jobsList.get(position).isCurrently_using());
        holder.action.setImageResource(R.drawable.ic_delete_black_24dp);
        holder.actionText.setVisibility(View.GONE);
        holder.imageText.setVisibility(View.GONE);
        holder.action.setVisibility(View.VISIBLE);


        holder.skillsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Home)
                    ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                Intent in = new Intent(context, EditProfileActivity.class);
                in.putExtra("data", jobsList.get(position));
                in.putExtra("Type", "skills");
                context.startActivity(in);
            }
        });

        holder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Home)
                    ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
                alertDialogBuilder.setMessage("Are you sure you want to delete?");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                mClickListener.onItemDelete(jobsList.get(position), position);
                            }
                        });

                alertDialogBuilder.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


    }

    // total number of cells
    @Override
    public int getItemCount() {
        return jobsList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.skillsLayout)
        LinearLayout skillsLayout;

        @BindView(R.id.currently)
        TextView currently;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.experience)
        TextView experience;

        @BindView(R.id.lastUsed)
        TextView lastUsed;

        @BindView(R.id.action)
        ImageView action;

        @BindView(R.id.action_text)
        TextView actionText;

        @BindView(R.id.image_text)
        TextView imageText;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    public void setSkillImageList(List<SkillsModel> skillsModelList) {
        this.jobsList = skillsModelList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemDelete(SkillsModel skill, int position);
    }
}