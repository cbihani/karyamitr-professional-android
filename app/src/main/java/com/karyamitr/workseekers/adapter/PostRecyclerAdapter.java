package com.karyamitr.workseekers.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Jobs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<Jobs> mPostItems;
    private List<Jobs> contactListFiltered;
    String type = "details";
    private ItemClickListener mClickListener;

    public PostRecyclerAdapter(List<Jobs> postItems) {
        this.mPostItems = postItems;
        this.contactListFiltered = postItems;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.demo_item_view, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {

            populateItemRows((ItemViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return contactListFiltered == null ? 0 : contactListFiltered.size();
    }

    @Override
    public int getItemViewType(int position) {
        return contactListFiltered.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = mPostItems;
                } else {
                    List<Jobs> filteredList = new ArrayList<>();
                    for (Jobs row : mPostItems) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getLocation().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getEmployment_type().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCompany().getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Jobs>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.job_title)
        TextView jobTitle;

        @BindView(R.id.job_location)
        TextView jobLocation;

        @BindView(R.id.job_experience)
        TextView jobExperience;

        @BindView(R.id.job_salary)
        TextView jobSalary;

        @BindView(R.id.job_type)
        TextView jobType;

        @BindView(R.id.job_company)
        TextView jobCompany;

        @BindView(R.id.job_item_layout)
        LinearLayout jobLayout;

        @BindView(R.id.job_list_card)
        CardView jobListCard;

        @BindView(R.id.button_add_skill)
        Button applyDirectly;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {

        Jobs item = contactListFiltered.get(position);

        viewHolder.jobTitle.setText(item.getTitle());
        if (item.getCompany() != null)
            viewHolder.jobCompany.setText(item.getCompany().getName());

        if (item.getMinimum_experience() != null && item.getMaximum_experience() != null) {
            viewHolder.jobExperience.setText(Html.fromHtml("<b> " + item.getMinimum_experience() + " - " + item.getMaximum_experience() + "</b>"));
        } else {
            viewHolder.jobExperience.setText(Html.fromHtml("<b> " + "0.6" + "</b>"));
        }

        viewHolder.jobLocation.setText(Html.fromHtml("<b> " + item.getLocation() + " </b>"));
        if (item.getMinimum_salary() != null && item.getMaximum_salary() != null) {
            viewHolder.jobSalary.setText(Html.fromHtml("<b> " + item.getMinimum_salary() + " - " + item.getMaximum_salary()));
        } else {
            viewHolder.jobSalary.setText(Html.fromHtml("<b> " + "NA"));
        }

        viewHolder.jobType.setText(Html.fromHtml("Job type: " + "<b>" + item.getEmployment_type() + "</b>"));


        viewHolder.jobListCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "details";
                if (mClickListener != null)
                    mClickListener.onItemClick(mPostItems.get(position), position, type);
            }
        });

        viewHolder.applyDirectly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "direct";
                if (mClickListener != null)
                    mClickListener.onItemClick(mPostItems.get(position), position, type);
            }
        });

    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(Jobs jobList, int position, String type);
    }

    public void filterList(List<Jobs> filterdNames) {
        mPostItems = filterdNames;
        notifyDataSetChanged();
    }


}
