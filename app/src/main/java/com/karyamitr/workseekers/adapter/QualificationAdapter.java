package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Qualification;
import com.karyamitr.workseekers.view.activity.Home;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QualificationAdapter extends RecyclerView.Adapter<QualificationAdapter.ViewHolder> {

    private List<Qualification> languagesList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;


    Context context;

    // data is passed into the constructor
    public QualificationAdapter(Context context, List<Qualification> languagesList) {
        this.mInflater = LayoutInflater.from(context);
        this.languagesList = languagesList;
        this.context = context;

    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.qualification_rv_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Qualification qualification = languagesList.get(position);
        holder.qualificationName.setText(languagesList.get(position).getDegree());
        holder.universityName.setText(languagesList.get(position).getUniversity());
        holder.fromAndToYear.setText(languagesList.get(position).getCourse_start_year() + " - " + languagesList.get(position).getCourse_end_year());
        if (languagesList.get(position).isPursuing())
            holder.isPursuing.setText(R.string.persuing);
        else {
            holder.isPursuing.setVisibility(View.GONE);
        }
        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Home)
                    ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                if (mClickListener != null)
                    mClickListener.onItemClick(languagesList.get(position), position);

            }
        });

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return languagesList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.name)
        TextView qualificationName;

        @BindView(R.id.university)
        TextView universityName;

        @BindView(R.id.year)
        TextView fromAndToYear;

        @BindView(R.id.cardLayout_qualification)
        RelativeLayout cardLayout;

        @BindView(R.id.is_pursuing)
        TextView isPursuing;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

    }

    public void getQualificationList(List<Qualification> languagesList) {
        this.languagesList = languagesList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(Qualification languagesList, int position);
    }


//    public List<Qualification> getStudentList() {
//        mClickListener.onItemClick(getQualificationList();
//        return languagesList;
//
//    }
}
