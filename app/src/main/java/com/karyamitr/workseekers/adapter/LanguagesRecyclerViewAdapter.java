package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Languages;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LanguagesRecyclerViewAdapter extends RecyclerView.Adapter<LanguagesRecyclerViewAdapter.ViewHolder> {

    private List<Languages> languagesList;
    private List<Languages> selectedList = new ArrayList<>();
    ;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;


    Context context;

    // data is passed into the constructor
    public LanguagesRecyclerViewAdapter(Context context, List<Languages> languagesList) {
        this.mInflater = LayoutInflater.from(context);
        this.languagesList = languagesList;
        this.context = context;

    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.languages_item_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Languages languages = languagesList.get(position);
        holder.textLanguage.setText(languagesList.get(position).getName());

        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    holder.layoutCheckbox.setVisibility(View.VISIBLE);

                selectedList.add(languages);
                holder.read.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            languages.setCan_read(true);
                            selectedList.set(selectedList.size() - 1, languages);
                            mClickListener.onItemClick(selectedList);
                        } else {
                            languages.setCan_read(false);
                            selectedList.set(selectedList.size() - 1, languages);
                            mClickListener.onItemClick(selectedList);
                        }
                    }
                });

                holder.write.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            languages.setCan_write(true);
                            selectedList.set(selectedList.size() - 1, languages);
                            mClickListener.onItemClick(selectedList);
                        } else {
                            languages.setCan_write(false);
                            selectedList.set(selectedList.size() - 1, languages);
                            mClickListener.onItemClick(selectedList);
                        }
                    }
                });

                holder.speak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            languages.setCan_speak(true);
                            selectedList.set(selectedList.size() - 1, languages);
                            mClickListener.onItemClick(selectedList);
                        }else {
                            languages.setCan_speak(false);
                            selectedList.set(selectedList.size() - 1, languages);
                            mClickListener.onItemClick(selectedList);
                        }
                    }
                });


            }
        });

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return languagesList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.textLanguage)
        TextView textLanguage;

        @BindView(R.id.checkbox_read)
        CheckBox read;

        @BindView(R.id.checkbox_write)
        CheckBox write;

        @BindView(R.id.checkbox_speak)
        CheckBox speak;

//        @BindView(R.id.line)
//        View viewLine;

        @BindView(R.id.relativeLanguageLayout)
        RelativeLayout cardLayout;

        @BindView(R.id.layoutCheckbox)
        LinearLayout layoutCheckbox;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

    }

    public void setSkillImageList(List<Languages> languagesList) {
        this.languagesList = languagesList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(List<Languages> languagesList);
    }

}