package com.karyamitr.workseekers.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.JobApplications;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppliedJobstRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<JobApplications> mPostItems;
    private List<JobApplications> contactListFiltered;
    private ItemClickListener mClickListener;

    public AppliedJobstRecyclerAdapter(List<JobApplications> postItems) {
        this.mPostItems = postItems;
        this.contactListFiltered = postItems;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.demo_item_view, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {

            populateItemRows((ItemViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return contactListFiltered == null ? 0 : contactListFiltered.size();
    }

    @Override
    public int getItemViewType(int position) {
        return contactListFiltered.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = mPostItems;
                } else {
                    List<JobApplications> filteredList = new ArrayList<>();
                    for (JobApplications row : mPostItems) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getJobs().getTitle().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getJobs().getLocation().toLowerCase().contains(charString.toLowerCase())
                        ) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<JobApplications>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.job_title)
        TextView jobTitle;

        @BindView(R.id.job_location)
        TextView jobLocation;

        @BindView(R.id.job_experience)
        TextView jobExperience;

        @BindView(R.id.job_salary)
        TextView jobSalary;

        @BindView(R.id.job_type)
        TextView jobType;

        @BindView(R.id.job_company)
        TextView jobCompany;

        @BindView(R.id.job_item_layout)
        LinearLayout jobLayout;

        @BindView(R.id.job_list_card)
        CardView jobListCard;

        @BindView(R.id.button_add_skill)
        Button viewDetails;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            viewDetails.setText("View Details");

        }


    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {

        JobApplications item = contactListFiltered.get(position);

        viewHolder.jobTitle.setText(item.getJobs().getTitle());
        viewHolder.jobExperience.setText(Html.fromHtml("Functional Area: " + "<b> " + item.getJobs().getFunctional_area() + "</b>"));
        viewHolder.jobCompany.setText(Html.fromHtml("Application status: " + "<b> " + item.getStatus() + "</b>"));


        viewHolder.jobLocation.setVisibility(View.GONE);
        viewHolder.jobSalary.setVisibility(View.GONE);
        String date = item.getCreated_at().substring(0, 10);
        viewHolder.jobType.setText(Html.fromHtml("Applied On: " + "<b> " + date + "</b>"));
        viewHolder.jobListCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(mPostItems.get(position), position);
            }
        });

        viewHolder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(mPostItems.get(position), position);
            }
        });


    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(JobApplications jobList, int position);
    }

    public void filterList(List<JobApplications> filterdNames) {
        mPostItems = filterdNames;
        notifyDataSetChanged();
    }


}
