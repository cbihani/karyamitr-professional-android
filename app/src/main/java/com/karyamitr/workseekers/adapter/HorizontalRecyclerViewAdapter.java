package com.karyamitr.workseekers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.SkillsModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalRecyclerViewAdapter.ViewHolder> {

    private List<SkillsModel> jobsList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    Context context;

    // data is passed into the constructor
    public HorizontalRecyclerViewAdapter(Context context, List<SkillsModel> jobsList) {
        this.mInflater = LayoutInflater.from(context);
        this.jobsList = jobsList;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.job_item_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.logo_grey);
        requestOptions.error(R.drawable.logo_grey);
        Glide.with(context).setDefaultRequestOptions(requestOptions).load(jobsList.get(position).getColorImage()).into(holder.imageViewCategory);
        holder.jobTitle.setText(jobsList.get(position).getSkill_name());
//         holder.imageViewCategory.setImageResource(jobsList.get(position).getColorImage());

        holder.hrvCategoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(position);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return jobsList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.job_title)
        TextView jobTitle;

        @BindView(R.id.job_location)
        TextView jobLocation;

        @BindView(R.id.image_catogory)
        ImageView imageViewCategory;

        @BindView(R.id.hrvCategoryLayout)
        RelativeLayout hrvCategoryLayout;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            ColorMatrix matrix = new ColorMatrix();
//            matrix.setSaturation(0);
//            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
//            imageViewCategory.setColorFilter(filter);
        }

    }

    public void setSkillImageList(List<SkillsModel> skillsModelList) {
        this.jobsList = skillsModelList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);
    }
}