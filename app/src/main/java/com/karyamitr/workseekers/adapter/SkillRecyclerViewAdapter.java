package com.karyamitr.workseekers.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.view.activity.Home;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SkillRecyclerViewAdapter extends RecyclerView.Adapter<SkillRecyclerViewAdapter.ViewHolder> {

    private List<SkillsModel> skillsModelList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    SkillsModel skillsModel;
    boolean isSingleSelect = false;
    Context context;
    int previousPosition;

    public void setSingleSelect(boolean singleSelect) {
        isSingleSelect = singleSelect;
    }

    public void setSkillsModel(SkillsModel skillsModel) {
        this.skillsModel = skillsModel;
        for (int i = 0; i < skillsModelList.size(); i++) {
            if (skillsModel.getSkill_name().equalsIgnoreCase(skillsModelList.get(i).getSkill_name())) {
                skillsModelList.get(i).setSelected(true);
                previousPosition = i;
            }
        }
    }

    // data is passed into the constructor
    public SkillRecyclerViewAdapter(Context context, List<SkillsModel> skillsModelList) {
        this.mInflater = LayoutInflater.from(context);
        this.skillsModelList = skillsModelList;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.skills_item_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.skills_placeholder);
        requestOptions.error(R.drawable.skills_placeholder);
        holder.myTextView.setText(skillsModelList.get(position).getSkill_name());
        if (skillsModelList.get(position).isSelected()) {
            previousPosition = position;
            if (skillsModelList.get(position).getColorImage() != null) {
                Glide.with(context).setDefaultRequestOptions(requestOptions).load(skillsModelList.get(position).getColorImage()).into(holder.imageView);
                holder.imageView.setColorFilter(null);
            } else {
                Glide.with(context).setDefaultRequestOptions(requestOptions).load(skillsModelList.get(position).getColorImage()).into(holder.imageView);
                holder.imageView.setColorFilter(null);
            }
        } else {
            if (skillsModelList.get(position).getColorImage() != null) {
                Glide.with(context).setDefaultRequestOptions(requestOptions).load(skillsModelList.get(position).getColorImage()).into(holder.imageView);
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                holder.imageView.setColorFilter(filter);
            } else {

            }
        }


        if (skillsModelList.get(position).isSelected()) {
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(skillsModelList.get(position).getColorImage()).into(holder.imageView);
            holder.imageView.setColorFilter(null);
        } else {
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(skillsModelList.get(position).getColorImage()).into(holder.imageView);
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.imageView.setColorFilter(filter);
//
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Home)
                    ((Home)context).setBottomBarVisivelty(View.VISIBLE);

                if (isSingleSelect) {
                    skillsModelList.get(previousPosition).setSelected(false);
                    skillsModelList.get(position).setSelected(true);
                    previousPosition = position;
                    notifyDataSetChanged();
                    if (mClickListener != null)
                        mClickListener.onSingleItemClick(position);
                } else {
                    if (mClickListener != null)
                        mClickListener.onItemClick(position);
                }


            }
        });
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return skillsModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.skill_name)
        TextView myTextView;

        @BindView(R.id.imageview)
        ImageView imageView;

        @BindView(R.id.skillCard)
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public void setSkillImageList(List<SkillsModel> skillsModelList) {
        this.skillsModelList = skillsModelList;
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return "";
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);

        void onSingleItemClick(int position);
    }
}