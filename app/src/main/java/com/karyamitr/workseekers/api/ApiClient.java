package com.karyamitr.workseekers.api;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.karyamitr.workseekers.helper.FilePathHelper;
import com.karyamitr.workseekers.utils.AppConstants;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import okhttp3.CertificatePinner;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


    private static Retrofit retrofit = null;
    public static String userId = "";
    public static String token = "";


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }

    public static void getUuid(String uuid) {
        userId = uuid;
    }

    public static void getTokenId(String tokenId) {
        token = tokenId;
    }

    static CertificatePinner certificatePinner = new CertificatePinner.Builder()
            .add(AppConstants.BASE_URL, "sha256/WoiWRyIOVNa9ihaBciRSC7XHjliYS9VwUGOIud4PB18=")
            .build();

    static OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            //.certificatePinner(certificatePinner)
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(@NonNull Chain chain) throws IOException {

                    Request request = chain.request()
                            .newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("X-User-Uuid", userId)
                            .addHeader("X-Auth-Token", token)
                            .build();
                    Log.d("response: ", "intercept: " + chain.proceed(request));
//                    Log.d("response: ", "intercept: " + request.body().toString());
                    return chain.proceed(request);
                }
            }).build();


    public void sendMultipart(Context context, Uri artifact) {
        try {
            Log.e("sendMultipart", " start");

            new MultipartUploadRequest(context, AppConstants.BASE_URL + "professionals/update_basic_details")
                    // starting from 3.1+, you can also use content:// URI string instead of absolute file

                    .addFileToUpload(getRealPathFromURI(artifact, context), "professional[profile_pic]")
                    .addHeader("Content-Type", "multipart/form-data")
                    .addHeader("accept", "application/json")
                    .addHeader("X-User-Uuid", userId)
                    .addHeader("X-Auth-Token", token)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {
                            // your code here
                            Log.e("sendMultipart", "--1");
                            Log.e("onProgress", "onProgress");

                        }

                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse,
                                            Exception exception) {
                            Log.e("sendMultipart", "--2");
                            Log.e("onError", "-" + serverResponse.getBodyAsString());

                        }

                        @Override
                        public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
                            Log.e("sendMultipart", "--3");
                            Log.e("ServerResponse", "--" + serverResponse.getBodyAsString());


                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            Log.e("sendMultipart", "--4");
                            Log.e("onCancelled", "-onCancelled");

                        }
                    })
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        } catch (FileNotFoundException e) {
            Log.e("sendMultipart", " FileNotFoundException " + e.getMessage());

            e.printStackTrace();
        } catch (MalformedURLException e) {
            Log.e("sendMultipart", " MalformedURLException " + e.getMessage());
            e.printStackTrace();
        }
    }

    public String getRealPathFromURI(Uri fileUri, Context c) {
        FilePathHelper filePathHelper = new FilePathHelper();
        String path = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (filePathHelper.getPathnew(fileUri, c) != null) {
                path = filePathHelper.getPathnew(fileUri, c).toLowerCase();
            } else {
                path = filePathHelper.getFilePathFromURI(fileUri, c).toLowerCase();
            }
        } else {
            path = filePathHelper.getPath(fileUri, c).toLowerCase();
        }
        return path;
    }


}
