package com.karyamitr.workseekers.api;

public interface DeleteCallBacks {
    void onDeleteSuccess(Object object);

    void onDeleteFailure(String message);
}
