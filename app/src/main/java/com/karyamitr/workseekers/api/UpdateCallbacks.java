package com.karyamitr.workseekers.api;

public interface UpdateCallbacks extends ResultCallBack {


    void onUpdateSuccess(Object object);

    void onUpdateFailure(String message);
}
