package com.karyamitr.workseekers.api;

import android.util.Log;

import com.karyamitr.workseekers.utils.CallBackHelper;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteCallBackHelper implements Callback {

    private final DeleteCallBacks listner;
    private static String TAG= CallBackHelper.class.getSimpleName();

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {
        Log.d(TAG, "onFailure: "+ t.getMessage());

        listner.onDeleteFailure(""+t.getLocalizedMessage());

    }

    public DeleteCallBackHelper(DeleteCallBacks listner) {
        this.listner = listner;
    }
}
