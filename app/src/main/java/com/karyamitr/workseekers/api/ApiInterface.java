package com.karyamitr.workseekers.api;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.model.Jobs;
import com.karyamitr.workseekers.model.OtpResponse;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.ResponseModel;
import com.karyamitr.workseekers.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @Headers({"Content-Type: application/json"})
    @POST("users/sign_in_sign_up_user.json")
    Call<ResponseData> getUser(@Body User user);


    // @Headers({"Content-Type: application/json", "X-User-Uuid"+ header})
    @POST("users/otp_confirmation.json")
    Call<OtpResponse> confirmOtp(@Body JsonObject jsonObject);


    @POST("users/register.json")
    Call<ResponseData> registerUser(@Body JsonObject jsonObject);


    @POST("professionals/update_basic_details.json")
    Call<ResponseData> updateProfile(@Body Professional professional);

    @POST("qualifications.json")
    Call<ResponseData> createQualification(@Body JsonObject qualification);

    @POST("work_experiences.json")
    Call<ResponseData> createExperience(@Body JsonObject workExperience);

    @GET("validate_auth_token.json")
    Call<ResponseData> validateAuthToken();

    @POST("skills.json")
    Call<ResponseData> addSkills(@Body JsonObject skills);

    @GET("jobs.json")
    Call<ResponseModel> getJobs(@Query("page") int number);

//    @GET("jobs/{public_id}.json")
//    Call<Job> getJobDetail(@Path("public_id") String public_id);

    @POST("users/resend_otp.json")
    Call<OtpResponse> resend_otp();

    @GET("professionals/profile.json")
    Call<Professional> getProfile();

    @GET("skill_sets/search.json")
    Call<List<String>> setSkills(@Query("query") String skills);

    @GET("jobs.json")
    Call<ResponseModel> applyFilter(@Query("query") JsonObject filter);

    @POST("job_applications.json")
    Call<ResponseData> applyJob(@Body JsonObject jobApplication);

    @GET("job_applications.json")
    Call<ResponseData> getJobApplications();

    @GET("posts.json")
    Call<ResponseData> getKaryapediaData();

    @GET("jobs/utilities.json")
    Call<ResponseData> getUtilities();

    @PUT("skills/{public_id}.json")
    Call<ResponseData> updateSkills(@Path("public_id") String publicId,
                                    @Body JsonObject jsonObject);

    @HTTP(method = "DELETE", path = "skills/{public_id}.json", hasBody = true)
    Call<ResponseData> deleteSkill(@Path("public_id") String publicId,
                                   @Body JsonObject jsonObject);

    @HTTP(method = "DELETE", path = "work_experiences/{public_id}.json", hasBody = true)
    Call<ResponseData> deleteWorkEx(@Path("public_id") String publicId);

    @HTTP(method = "DELETE", path = "qualifications/{public_id}.json", hasBody = true)
    Call<ResponseData> deleteQualification(@Path("public_id") String publicId);

    @GET("skill_sets.json")
    Call<ResponseData> getSkillSet();

    @PUT("work_experiences/{public_id}.json")
    Call<ResponseData> updateExperience(@Path("public_id") String publicId,
                                        @Body JsonObject jsonObject);

    @PUT("qualifications/{public_id}.json")
    Call<ResponseData> updateQualification(@Path("public_id") String publicId,
                                           @Body JsonObject jsonObject);

    @GET("jobs/{public_id}.json")
    Call<Jobs> getAppliedJobDetails(@Path("public_id") String publicId);

    @POST("professionals/update_basic_details/modify_languages.json")
    Call<ResponseData> updateProfileLanguages(@Body Professional professional);

    @GET("jobs/utilities.json")
    Call<ResponseData> getCategories();
}
