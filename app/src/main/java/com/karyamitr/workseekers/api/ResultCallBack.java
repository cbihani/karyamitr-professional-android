package com.karyamitr.workseekers.api;

public interface ResultCallBack {

    void onSuccess(Object object);

    void onFailure(String message);

    void onValidationFailed(String message);
}
