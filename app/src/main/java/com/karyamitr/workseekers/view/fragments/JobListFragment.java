package com.karyamitr.workseekers.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.HorizontalRecyclerViewAdapter;
import com.karyamitr.workseekers.adapter.PostRecyclerAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Jobs;
import com.karyamitr.workseekers.model.SkillSets;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.DocumentsNeeded;
import com.karyamitr.workseekers.view.activity.FilterActivity;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.JobDetails;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JobListFragment extends Fragment implements ResultCallBack, PostRecyclerAdapter.ItemClickListener, HorizontalRecyclerViewAdapter.ItemClickListener,
        HomeViewModel.SkillInterface, HomeViewModel.JobApplyInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = JobListFragment.class.getSimpleName();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerView_horizontal)
    RecyclerView horizontalRecyclerView;
    @BindView(R.id.edittext_search_jobs)
    EditText searchJobs;
    @BindView(R.id.all)
    FrameLayout all;
    @BindView(R.id.loaderLayout)
    RelativeLayout loaderLayout;
    @BindView(R.id.noDataLayout)
    RelativeLayout noDataLayout;
    @BindView(R.id.textForError)
    TextView textForError;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mySwipeRefreshLayout;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;

    View view;

    HorizontalRecyclerViewAdapter horizontalAdapter;
    List<Jobs> jobsList = new ArrayList<>();
    private StaggeredGridLayoutManager _sGridLayoutManager;
    Progress progress = Progress.getInstance();
    HomeViewModel homeViewModel;
    List<SkillsModel> skills = new ArrayList<>();
    PostRecyclerAdapter recyclerViewAdapter;
    boolean isLoading = false;

    Jobs joblistForApply;

    boolean flag = true;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    public JobListFragment() {
        // Required empty public constructor
    }

    public static JobListFragment newInstance(String param1, String param2) {
        JobListFragment fragment = new JobListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_job_list, container, false);

        ButterKnife.bind(this, view);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.getListners(this);
        homeViewModel.setSkillInterface(this);
        homeViewModel.setJobApplyInterface(this);

        getCategories();
        getJobsData();
        initAdapter();
        initScrollListener();

        searchJobs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                recyclerViewAdapter.getFilter().filter(s);
            }
        });


        mySwipeRefreshLayout.setOnRefreshListener(
                () -> {
                    Log.i("JobsFragment", "onRefresh called from SwipeRefreshLayout");
                    currentPage = PAGE_START;
                    jobsList.clear();
                    isLoading = false;
                    flag = true;
                    getJobsData();
                }
        );
        all.setOnClickListener(v -> {
            jobsList.clear();
            Log.e("all.", "setOnClickListener");
            loaderLayout.setVisibility(View.VISIBLE);
            currentPage = PAGE_START;
            isLoading = false;
            flag = true;
            getJobsData();
        });
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @OnClick(R.id.filter)
    public void openFilterOptions() {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        Intent in = new Intent(getActivity(), FilterActivity.class);
        if (filterData != null) {
            in.putExtra("selectedFilters", filterData.toString());
        }
        startActivityForResult(in, 1234);
    }

    JsonObject filterData, filterDataForJob;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == Activity.RESULT_OK) {
            Gson gson = new Gson();
            filterData = gson.fromJson(data.getStringExtra("filterData"), JsonObject.class);
            Log.d(TAG, "onActivityResult: " + filterData.toString());
            if (Connectivity.hasConnection(getContext())) {
                jobsList.clear();
                flag = true;
                loaderLayout.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                homeViewModel.callApiForFilteredData(filterData);
            }

        }

        if (requestCode == 1111 && resultCode == Activity.RESULT_OK) {
            Gson gson = new Gson();
            filterDataForJob = gson.fromJson(data.getStringExtra("applicant_document_list"), JsonObject.class);
            Log.d(TAG, "onActivityResult: " + filterDataForJob.toString());
            callApplyJob(joblistForApply);
        }
    }

    private void getJobsData() {
        if (Connectivity.hasConnection(getContext())) {
            Log.e("TAG :", "getJobsData ");
//            progress.showProgressDialog(getContext(), "Loading.. Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            homeViewModel.callApi(currentPage);
        } else {
            loaderLayout.setVisibility(View.GONE);
            ((BaseActivity) getActivity()).showToast(String.valueOf(R.string.checkInternet));
        }

    }

    private void getCategories() {
        if (Connectivity.hasConnection(getContext())) {
            progress.showProgressDialog(getContext(), "Please Wait..");
            homeViewModel.getSkillSets();
        } else {
            ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onSuccess(Object object) {
        loaderLayout.setVisibility(View.GONE);
        List<Jobs> jobs = (List<Jobs>) object;
        if (jobsList.size() > 0) {
            jobsList.remove(jobsList.size() - 1);
            int scrollPosition = jobsList.size();
            recyclerViewAdapter.notifyItemRemoved(scrollPosition);
        }
        jobsList.addAll(jobs);
        isLoading = false;
        mShimmerViewContainer.stopShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.GONE);
        recyclerViewAdapter.notifyDataSetChanged();
        loaderLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        if (jobsList.size() == 0) {
            noDataLayout.setVisibility(View.VISIBLE);
            textForError.setText("No jobs found");
        } else {
            noDataLayout.setVisibility(View.GONE);
        }
        mySwipeRefreshLayout.setRefreshing(false);

    }

    private void initAdapter() {
        int numberOfColumns = 1;
        _sGridLayoutManager = new StaggeredGridLayoutManager(numberOfColumns,
                StaggeredGridLayoutManager.VERTICAL);
        recyclerViewAdapter = new PostRecyclerAdapter(jobsList);
        recyclerView.setLayoutManager(_sGridLayoutManager);
        recyclerViewAdapter.setClickListener(this);
        recyclerView.setAdapter(recyclerViewAdapter);

        horizontalRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        horizontalAdapter = new HorizontalRecyclerViewAdapter(getActivity(), skills);
        horizontalAdapter.setClickListener(this);
        horizontalRecyclerView.setAdapter(horizontalAdapter);

    }

    private void initScrollListener() {

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                int visibleItemCount = _sGridLayoutManager.getChildCount();
                int totalItemCount = _sGridLayoutManager.getItemCount();
                int pastVisiblesItems = _sGridLayoutManager.findLastCompletelyVisibleItemPositions(null)[0];
                Log.d(TAG, "onScrollChange:  v " + visibleItemCount + "  t " + totalItemCount + " p " + pastVisiblesItems);

                if (!isLoading) {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        isLoading = true;
                        if (flag)
                            loadMore();  //desired function call
                    }
                }

            }
        });


    }

    private void loadMore() {
        currentPage++;
        jobsList.add(null);
        recyclerViewAdapter.notifyItemInserted(jobsList.size() - 1);
        getJobsData();
    }


    @Override
    public void onFailure(String message) {
//        Log.e("TAG :","onFailure "+message);
//        progress.hideProgressBar();
        loaderLayout.setVisibility(View.GONE);

        if (jobsList.size() > 0) {
            jobsList.remove(jobsList.size() - 1);
            int scrollPosition = jobsList.size();
            recyclerViewAdapter.notifyItemRemoved(scrollPosition);
        }
        isLoading = false;
        flag = false;
        loaderLayout.setVisibility(View.GONE);
        mySwipeRefreshLayout.setRefreshing(false);
//        ((BaseActivity) getActivity()).showToast("No more data");
    }

    @Override
    public void onValidationFailed(String message) {

    }

    @Override
    public void onItemClick(Jobs jobList, int position, String type) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);

        Log.d(TAG, "onItemClick: " + jobList.getPublic_id());
        joblistForApply = jobList;
        Intent in = null;
        if (type.equalsIgnoreCase("details")) {
            in = new Intent(getActivity(), JobDetails.class);
            in.putExtra("Jobs", jobList);
            startActivity(in);
        } else {
            if (jobList.getApplicantDocumentList() != null && jobList.getApplicantDocumentList().isDocument_check()) {
                in = new Intent(getActivity(), DocumentsNeeded.class);
                in.putExtra("applicant_documents", jobList.getApplicantDocumentList());
                startActivityForResult(in, 1111);
            } else {

                callApplyJob(jobList);

            }
        }


    }

    private void callApplyJob(Jobs jobList) {
        if (Connectivity.hasConnection(getActivity())) {
            progress.showProgressDialog(getActivity(), "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(getActivity()));
            ApiClient.getTokenId(Prefs.getToken(getActivity()));
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("job_public_id", jobList.getPublic_id());
            jsonObject.add("job_specific_answers", filterDataForJob);
            JsonObject jobApplication = new JsonObject();
            jobApplication.add("job_application", jsonObject);
            homeViewModel.callApiForJob(jobApplication);
        } else {
            ((Home) getActivity()).showToast(R.string.checkInternet);
        }
    }

    @Override
    public void onItemClick(int position) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);

        SkillsModel skill = skills.get(position);
        List<String> skillList = new ArrayList<>();
        skillList.add(skill.getSkill_name());
        JsonObject json = new JsonObject();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("category", getArrayListFromList(skillList));
        jsonObject.add("employment_type", getArrayListFromList(new ArrayList<>()));
        jsonObject.add("location", getArrayListFromList(new ArrayList<>()));
        json.add("filter", jsonObject);

        if (Connectivity.hasConnection(getContext())) {
            jobsList.clear();
            flag = true;
            loaderLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            homeViewModel.callApiForFilteredData(json);
        }
    }


    private JsonArray getArrayListFromList(List<String> data) {
        JsonArray builder = new JsonArray();

        for (String filaterData : data) {
            builder.add(filaterData);
        }
        return builder;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void getSkillSet(List<SkillSets> skillSets) {
        progress.hideProgressBar();
        for (int i = 0; i < skillSets.size(); i++) {
            SkillsModel skillsModel = new SkillsModel();
            skillsModel.setSkill_name(skillSets.get(i).getName());
            //skillsModel.setGreyImage(imageData[i]);
            skillsModel.setColorImage(skillSets.get(i).getColored_icon());
            skills.add(skillsModel);
        }

        horizontalAdapter.notifyDataSetChanged();

    }

    @Override
    public void getresponse(Object object) {
        progress.hideProgressBar();
//        Intent in = new Intent(getActivity(), Home.class);
//        startActivity(in);
//        getActivity().finish();
        getJobsData();
        ((BaseActivity) getActivity()).showToast("Applied successfully");
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
