package com.karyamitr.workseekers.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.AppliedJobstRecyclerAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.JobApplications;
import com.karyamitr.workseekers.model.Jobs;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.JobDetails;
import com.karyamitr.workseekers.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AppliedJobListFragment extends Fragment implements ResultCallBack, AppliedJobstRecyclerAdapter.ItemClickListener, HomeViewModel.JobApplyInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.edittext_search_jobs)
    EditText searchJobs;
    @BindView(R.id.loaderLayout)
    RelativeLayout loaderLayout;
    @BindView(R.id.noDataLayout)
    RelativeLayout noDataLayout;
    @BindView(R.id.textForError)
    TextView textForError;

    HomeViewModel homeViewModel;
    List<JobApplications> jobsList = new ArrayList<>();
    private StaggeredGridLayoutManager _sGridLayoutManager;
    Progress progress = Progress.getInstance();
    private OnFragmentInteractionListener mListener;
    AppliedJobstRecyclerAdapter recyclerViewAdapter;
    public static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;

    public AppliedJobListFragment() {
        // Required empty public constructor


    }


    // TODO: Rename and change types and number of parameters
    public static AppliedJobListFragment newInstance(String param1, String param2) {
        AppliedJobListFragment fragment = new AppliedJobListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_applied_job_list, container, false);
        ButterKnife.bind(this, view);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.getListners(this);
        homeViewModel.setJobApplyInterface(this);
        getJobsData();
        initAdapter();

        searchJobs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                recyclerViewAdapter.getFilter().filter(s);


            }
        });
        ((BaseActivity) getActivity()).hideKeyboard();
        return view;
    }

    private void getJobsData() {
        if (Connectivity.hasConnection(getContext())) {
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            homeViewModel.callAppliedJobListApi();
        } else {
            ((BaseActivity) getActivity()).showToast(String.valueOf(R.string.checkInternet));
        }
    }

    private void initAdapter() {
        int numberOfColumns = 1;
        _sGridLayoutManager = new StaggeredGridLayoutManager(numberOfColumns,
                StaggeredGridLayoutManager.VERTICAL);
        recyclerViewAdapter = new AppliedJobstRecyclerAdapter(jobsList);
        recyclerView.setLayoutManager(_sGridLayoutManager);
        recyclerViewAdapter.setClickListener(this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onSuccess(Object object) {
        List<JobApplications> applications = (List<JobApplications>) object;
        jobsList.addAll(applications);
        mShimmerViewContainer.stopShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.GONE);
        recyclerViewAdapter.notifyDataSetChanged();
        loaderLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        if (jobsList.size() == 0) {
            noDataLayout.setVisibility(View.VISIBLE);
            textForError.setText("No jobs found");
        } else {
            noDataLayout.setVisibility(View.GONE);
        }


    }

    @Override
    public void onFailure(String message) {
        loaderLayout.setVisibility(View.GONE);
        ((BaseActivity) getActivity()).showToast("No more data");
    }

    @Override
    public void onValidationFailed(String message) {

    }

    @Override
    public void onItemClick(JobApplications jobList, int position) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);

        if (Connectivity.hasConnection(getContext())) {
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            homeViewModel.callAppliedJobDetails(jobList.getJobs().getPublic_id());
        } else {
            ((BaseActivity) getActivity()).showToast(String.valueOf(R.string.checkInternet));
        }


    }

    @Override
    public void getresponse(Object object) {
        Log.d("AppliedJobListFragment", "getresponse: " + object.toString());
        Jobs job = (Jobs) object;
        Intent in = new Intent(getActivity(), JobDetails.class);
        in.putExtra("Jobs", job);
        in.putExtra("pageType", "applied");
        startActivity(in);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
