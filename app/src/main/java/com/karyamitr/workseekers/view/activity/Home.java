package com.karyamitr.workseekers.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.net.Uri;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.view.fragments.AppliedJobListFragment;
import com.karyamitr.workseekers.view.fragments.JobListFragment;
import com.karyamitr.workseekers.view.fragments.KaryapediaFragment;
import com.karyamitr.workseekers.view.fragments.ProfileViewFragment;

import butterknife.BindView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class Home extends BaseActivity implements View.OnClickListener,
        JobListFragment.OnFragmentInteractionListener, KaryapediaFragment.OnFragmentInteractionListener,
        ProfileViewFragment.OnFragmentInteractionListener, AppliedJobListFragment.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener {
    Fragment fragment = null;
    private static final String TAG = Home.class.getSimpleName();
    @BindView(R.id.bar)
    BottomAppBar bottomAppBar;
    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;
    @BindView(R.id.home)
    ImageButton home;
    @BindView(R.id.find)
    ImageButton find;
    @BindView(R.id.karyapedia)
    ImageButton karyapedia;
    @BindView(R.id.menu)
    ImageButton menu;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    boolean mSlideState = false;
    DrawerLayout drawer;
    @BindView(R.id.text_jobs)
    TextView textJobs;
    @BindView(R.id.text_applied)
    TextView textApplied;
    @BindView(R.id.text_karyapedia)
    TextView textKaryapedia;
    @BindView(R.id.text_menu)
    TextView textMenu;
    @BindView(R.id.job_layout)
    LinearLayout jobLayout;
    @BindView(R.id.applied_layout)
    LinearLayout appliedLayout;
    @BindView(R.id.karyapedia_layout)
    LinearLayout karyapediaLayout;
    @BindView(R.id.menu_layout)
    LinearLayout menuLayout;


    private static final String SHOWCASE_ID = "1";
    private ActionBar actionBar;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initViews() {
        Prefs.setLaunchActivity(this, "7");
        jobLayout.setOnClickListener(this);
        appliedLayout.setOnClickListener(this);
        karyapediaLayout.setOnClickListener(this);
        menuLayout.setOnClickListener(this);
        fab.setOnClickListener(this);
        fragment = new JobListFragment();
        loadFragment(fragment);
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        home.setColorFilter(filter);
        karyapedia.setColorFilter(filter);
        find.setColorFilter(filter);
        menu.setColorFilter(filter);
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view


        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();

        drawer.setDrawerListener(new ActionBarDrawerToggle(this,
                drawer,
                null,
                0,
                0) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mSlideState = false;//is Closed
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mSlideState = true;//is Opened
            }
        });
        navigationView.setNavigationItemSelectedListener(this);

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWCASE_ID);

        sequence.setConfig(config);

        sequence.addSequenceItem(jobLayout,
                "You can see all the jobs here", "GOT IT");

        sequence.addSequenceItem(appliedLayout,
                "You can track your job applications", "GOT IT");

        sequence.addSequenceItem(fab,
                "To view your profile. Click here", "GOT IT");

        sequence.addSequenceItem(karyapediaLayout,
                "To know about job trades. Click here", "GOT IT");

        sequence.addSequenceItem(menuLayout,
                "For Menu. Click here", "GOT IT");

        sequence.start();
    }


    @Override
    public void onClick(View v) {

        setBottomBarVisivelty(View.VISIBLE);
        Log.e("TAG", "--" + bottomAppBar.isShown());
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        switch (v.getId()) {
            case R.id.job_layout:
                textJobs.setTextColor(getResources().getColor(R.color.white));
                home.setColorFilter(null);
                find.setColorFilter(filter);
                karyapedia.setColorFilter(filter);
                menu.setColorFilter(filter);
                fragment = new JobListFragment();
                loadFragment(fragment);
                break;
            case R.id.applied_layout: // Fragment # 0 - This will show FirstFragment different title
                textApplied.setTextColor(getResources().getColor(R.color.white));
                find.setColorFilter(null);
                home.setColorFilter(filter);
                karyapedia.setColorFilter(filter);
                menu.setColorFilter(filter);
                fragment = new AppliedJobListFragment();
                loadFragment(fragment);
                break;
            case R.id.karyapedia_layout: // Fragment # 1 - This will show SecondFragment
                textKaryapedia.setTextColor(getResources().getColor(R.color.white));
                karyapedia.setColorFilter(null);
                home.setColorFilter(filter);
                find.setColorFilter(filter);
                menu.setColorFilter(filter);
                fragment = new KaryapediaFragment();
                loadFragment(fragment);
                break;
            case R.id.menu_layout: // Fragment # 1 - This will show SecondFragment
                textMenu.setTextColor(getResources().getColor(R.color.white));
                menu.setColorFilter(null);
                home.setColorFilter(filter);
                find.setColorFilter(filter);
                karyapedia.setColorFilter(filter);
                if (mSlideState) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
                break;
            case R.id.fab: // Fragment # 1 - This will show SecondFragment
                fragment = new ProfileViewFragment();
                loadFragment(fragment);
                break;
            default:
                fragment = new JobListFragment();
                loadFragment(fragment);
                break;
        }


    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Are you sure you want to Exit?");
            alertDialogBuilder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                        }
                    });

            alertDialogBuilder.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main2_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        item.setChecked(true);

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_tools) {
            Prefs.setToken(this, "");
            Prefs.setUserToken(this, "");
            Intent in = new Intent(this, Login.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(in);
            finish();

        }
//        else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_tools) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    public void setBottomBarVisivelty(int visibility) {
        Log.e("Home: ", "setBottomBarVisivelty");
        HideBottomViewOnScrollBehavior behavior = (HideBottomViewOnScrollBehavior) bottomAppBar.getBehavior();
//        behavior.
//                bottomAppBar.clearAnimation();
        bottomAppBar.animate().translationY(0).setDuration(0);
//        bottomAppBar.setVisibility(visibility);
    }

    public void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager imm =
                    (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
