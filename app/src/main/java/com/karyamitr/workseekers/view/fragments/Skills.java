package com.karyamitr.workseekers.view.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.SkillRecyclerViewAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.SkillSets;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.EditProfileActivity;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.RegisterViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Skills extends Fragment implements SkillRecyclerViewAdapter.ItemClickListener, ResultCallBack, RegisterViewModel.SkillDataInterface {

    private final String TAG = Skills.class.getSimpleName();

    SkillRecyclerViewAdapter adapter;
    List<SkillsModel> skills = new ArrayList<>();
    List<String> selectedSkills = new ArrayList<String>();
    Progress progress = Progress.getInstance();
    RegisterViewModel registerViewModel;
    View view;
    private boolean _hasLoadedOnce = false;

    SkillsModel skillsModel;

    public Skills() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static Skills newInstance(int page, boolean isLast) {
        Bundle args = new Bundle();
        args.putInt("page", page);
        if (isLast)
            args.putBoolean("isLast", true);
        final Skills fragment = new Skills();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.edit_skills_layout)
    LinearLayout editSkillsLayout;

    @BindView(R.id.edittext_experience)
    EditText experience;

    @BindView(R.id.last_used)
    EditText lastUsed;

    @BindView(R.id.checkbox_currently_using)
    CheckBox checkboxCurrentlyUsing;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {

                if (Connectivity.hasConnection(getContext())) {
                    progress.showProgressDialog(getContext(), "Please Wait..");
                    registerViewModel.getSkillSets();
                } else {
                    ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
                }

                _hasLoadedOnce = true;
            }
        }
    }

    @BindView(R.id.etSkills)
    AutoCompleteTextView newSkills;


    @OnClick(R.id.textview_add_more)
    public void addSkills(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (!newSkills.getText().toString().equalsIgnoreCase("")) {
            SkillsModel model = new SkillsModel();
            model.setSkill_name(newSkills.getText().toString());
            model.setGreyImage(R.drawable.chef_grey);
            // model.setColorImage(R.drawable.chef);
            model.setSelected(true);
            skills.add(model);
            selectedSkills.add(model.getSkill_name());
            adapter.notifyDataSetChanged();
            newSkills.getText().clear();
        } else {
            Toast.makeText(getActivity(), "Enter Skill first", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_skills, container, false);

        ButterKnife.bind(this, view);
        // set up the RecyclerView
        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
        registerViewModel.getSkillsListener(this);
        // getSkillsData();
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
        adapter = new SkillRecyclerViewAdapter(getContext(), skills);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        registerViewModel.setSkillDataInterface(this);
        newSkills.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {
                    if (Connectivity.hasConnection(getContext())) {
                        String skill = s.toString();
                        registerViewModel.callApiForSetSkills(skill);
                    } else {
                        ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        skillsModel = (SkillsModel) getActivity().getIntent().getSerializableExtra("data");
        if (skillsModel != null) {
            adapter.setSingleSelect(true);
            adapter.setSkillsModel(skillsModel);
            if (Connectivity.hasConnection(getContext())) {
                progress.showProgressDialog(getContext(), "Please Wait..");
                registerViewModel.getSkillSets();
            } else {
                ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
            }
        }
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }

    private void setDataToViews() {

    }


    @Override
    public void getSkillSet(List<SkillSets> skillSets) {
        progress.hideProgressBar();
        for (int i = 0; i < skillSets.size(); i++) {
            SkillsModel skill = new SkillsModel();
            skill.setSkill_name(skillSets.get(i).getName());
            skill.setPublic_id(skillSets.get(i).getPublic_id());
            //skillsModel.setGreyImage(imageData[i]);
            skill.setColorImage(skillSets.get(i).getColored_icon());
            if (skillsModel != null && skillsModel.getSkill_name() != null && skillsModel.getSkill_name().equalsIgnoreCase(skill.getSkill_name())) {
                skill.setSelected(skillsModel.isSelected());
            }
            skills.add(skill);

        }
        adapter.notifyDataSetChanged();

    }

    private String skillName;

    @OnClick(R.id.button_add_skill)
    public void addAllSkills(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);

        if (Connectivity.hasConnection(getContext())) {
            progress.showProgressDialog(getContext(), "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            String allSkills = android.text.TextUtils.join(",", selectedSkills);
            if (skillsModel != null && skillsModel.getPublic_id() != null) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("experience", experience.getText().toString());
                jsonObject.addProperty("last_used", lastUsed.getText().toString());
                jsonObject.addProperty("skill_name", skillName);
                jsonObject.addProperty("currently_using", checkboxCurrentlyUsing.isChecked());
                JsonObject json = new JsonObject();
                json.add("skill", jsonObject);
                registerViewModel.callApiForUpdateSkill(json, skillsModel.getPublic_id());
            } else {
                registerViewModel.callApiForSkill(allSkills);

            }
        } else {
            ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
        }

    }


    @Override
    public void onItemClick(int position) {

        if (skills.get(position).isSelected()) {
            skills.get(position).setSelected(false);
        } else {
            skills.get(position).setSelected(true);
        }

        if (skills.get(position).isSelected()) {
            selectedSkills.add(skills.get(position).getSkill_name());
        } else {
            if (selectedSkills.contains(skills.get(position).getSkill_name())) {
                selectedSkills.remove(skills.get(position).getSkill_name());
            }
        }

        adapter.notifyDataSetChanged();
        Log.i("TAG", "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

    }

    @Override
    public void onSingleItemClick(int position) {
        selectedSkills.clear();
        selectedSkills.add(skills.get(position).getSkill_name());
        skillName = skills.get(position).getSkill_name();
        if (skillsModel.getPublic_id() != null)
            editSkillsLayout.setVisibility(View.VISIBLE);

    }

    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
        ResponseData data = (ResponseData) object;

        Log.d(TAG, "onSuccess: " + data.toString());
        if (data.isSuccess() && getActivity() instanceof Register) {
            ((Register) getActivity()).setCurrentItem(3, true);
            ((BaseActivity) getActivity()).showToast(data.getMessage());
            Prefs.setLaunchActivity(getActivity(), "3");
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        if (getActivity() instanceof Register)
            ((BaseActivity) getActivity()).showToast(message);
        else
            ((EditProfileActivity) getActivity()).showToast(message);

    }

    @Override
    public void onValidationFailed(String message) {

    }

    @Override
    public void getSkillList(List<String> skillList) {
        String[] skills = new String[skillList.size()];
        skillList.toArray(skills);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), R.layout.maps_spinner_item, skills);
        newSkills.setAdapter(adapter);


    }

}
