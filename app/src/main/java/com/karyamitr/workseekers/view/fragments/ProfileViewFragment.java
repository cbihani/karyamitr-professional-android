package com.karyamitr.workseekers.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.ProfileExperienceRecyclerAdapter;
import com.karyamitr.workseekers.adapter.ProfileLanguagesRecyclerAdapter;
import com.karyamitr.workseekers.adapter.ProfileQualificationRecyclerAdapter;
import com.karyamitr.workseekers.adapter.ProfileSkillsRecyclerAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.DeleteCallBacks;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.api.UpdateCallbacks;
import com.karyamitr.workseekers.model.Languages;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.Qualification;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.model.WorkExperience;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.EditProfileActivity;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.viewmodel.HomeViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;


public class ProfileViewFragment extends Fragment implements ResultCallBack, DeleteCallBacks, ProfileSkillsRecyclerAdapter.ItemClickListener, ProfileExperienceRecyclerAdapter.ItemClickListener, ProfileQualificationRecyclerAdapter.ItemClickListener, ProfileLanguagesRecyclerAdapter.ItemClickListener, UpdateCallbacks {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    @BindView(R.id.skills_add)
    TextView addMoreSkills;
    @BindView(R.id.skills_title)
    TextView skillView;
    @BindView(R.id.experience_title)
    TextView experienceView;
    @BindView(R.id.qualification_title)
    TextView qualificationView;
    @BindView(R.id.language_title)
    TextView languageView;

    @BindView(R.id.professional_name)
    TextView professionalName;
    @BindView(R.id.professional_mobile)
    TextView professionalMobile;
    @BindView(R.id.professional_address)
    TextView professionalAddress;
    @BindView(R.id.professional_age)
    TextView professionalAge;
    @BindView(R.id.professional_gender)
    TextView professionalGender;
    @BindView(R.id.professional_experience)
    TextView professionalExperience;
    @BindView(R.id.professional_salary)
    TextView professionalSalary;
    @BindView(R.id.skills_count)
    TextView skillsCount;
    @BindView(R.id.experience_count)
    TextView experienceCount;
    @BindView(R.id.qualification_count)
    TextView qualificationCount;
    @BindView(R.id.language_count)
    TextView languageCount;
    @BindView(R.id.imageViewLogo)
    ImageView profilePic;
    @BindView(R.id.progress_seek)
    ProgressBar progress_seek;
    @BindView(R.id.profile_complete_text)
    TextView profileCompletePercent;
    @BindView(R.id.ivEditBasicProfile)
    TextView ivEditBasicProfile;

    @BindView(R.id.recylerview_skill)
    RecyclerView skillsRecyclerView;
    ProfileSkillsRecyclerAdapter adapter;
    List<SkillsModel> jobsList = new ArrayList<>();

    @BindView(R.id.recylerview_experience)
    RecyclerView experienceRecyclerView;
    ProfileExperienceRecyclerAdapter adapterExperience;
    List<WorkExperience> experienceList = new ArrayList<>();

    @BindView(R.id.recylerview_qualification)
    RecyclerView qualificationRecyclerView;
    ProfileQualificationRecyclerAdapter adapterQualification;
    List<Qualification> qualificationList = new ArrayList<>();

    @BindView(R.id.recylerview_languages)
    RecyclerView languagesRecyclerView;

    @BindView(R.id.languageHeader)
    LinearLayout languageHeader;

    @BindView(R.id.skillsHeader)
    LinearLayout skillsHeader;

    @BindView(R.id.skills_no_data)
    TextView skillsNoData;

    @BindView(R.id.experience_no_data)
    TextView experienceNoData;

    @BindView(R.id.qualification_no_data)
    TextView qualificationNoData;

    @BindView(R.id.languages_no_data)
    TextView languagesNoData;


    ProfileLanguagesRecyclerAdapter adapterLanguages;
    List<Languages> languagesList = new ArrayList<>();
    ShowcaseConfig config;
    Progress progress = Progress.getInstance();
    HomeViewModel homeViewModel;
    Professional professional;
    private static final String SHOWCASE = "2";


    public ProfileViewFragment() {
    }


    @Override
    public void onResume() {
        super.onResume();
        getProfileDetails();
        initAdapter();

    }

    @OnClick(R.id.skills_card)
    public void onSkillsCardClick(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (skillsRecyclerView.getVisibility() == View.VISIBLE) {
            skillsRecyclerView.setVisibility(View.GONE);
            skillsHeader.setVisibility(View.GONE);
        } else {
            skillsRecyclerView.setVisibility(View.VISIBLE);
            skillsHeader.setVisibility(View.VISIBLE);
            experienceRecyclerView.setVisibility(View.GONE);
            qualificationRecyclerView.setVisibility(View.GONE);
            languagesRecyclerView.setVisibility(View.GONE);
            languageHeader.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.experience_card)
    public void onExperienceCardClick(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (experienceRecyclerView.getVisibility() == View.VISIBLE) {
            experienceRecyclerView.setVisibility(View.GONE);
        } else {
            experienceRecyclerView.setVisibility(View.VISIBLE);
            skillsRecyclerView.setVisibility(View.GONE);
            skillsHeader.setVisibility(View.GONE);
            qualificationRecyclerView.setVisibility(View.GONE);
            languagesRecyclerView.setVisibility(View.GONE);
            languageHeader.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.qualification_card)
    public void onQualificationCardClick(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (qualificationRecyclerView.getVisibility() == View.VISIBLE) {
            qualificationRecyclerView.setVisibility(View.GONE);
        } else {
            qualificationRecyclerView.setVisibility(View.VISIBLE);
            skillsRecyclerView.setVisibility(View.GONE);
            skillsHeader.setVisibility(View.GONE);
            experienceRecyclerView.setVisibility(View.GONE);
            languagesRecyclerView.setVisibility(View.GONE);
            languageHeader.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.languages_card)
    public void onLanguagesCardClick(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (languagesRecyclerView.getVisibility() == View.VISIBLE) {
            languagesRecyclerView.setVisibility(View.GONE);
            languageHeader.setVisibility(View.GONE);
        } else {
            languagesRecyclerView.setVisibility(View.VISIBLE);
            languageHeader.setVisibility(View.VISIBLE);
            skillsRecyclerView.setVisibility(View.GONE);
            skillsHeader.setVisibility(View.GONE);
            experienceRecyclerView.setVisibility(View.GONE);
            qualificationRecyclerView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.skills_add)
    public void addMoreSkills(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        Intent in = new Intent(getContext(), EditProfileActivity.class);
        in.putExtra("Type", "skills");
        in.putExtra("data", new SkillsModel());
        startActivity(in);
    }


    @OnClick(R.id.languages_add)
    public void addLanguages(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        Intent in = new Intent(getActivity(), EditProfileActivity.class);
        in.putExtra("Type", "language");
        Professional professional = new Professional();
        professional.setLanguages(languagesList);
        in.putExtra("data", professional);
        startActivity(in);
    }


    @OnClick(R.id.experience_add)
    public void addExperience(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        Intent in = new Intent(getActivity(), EditProfileActivity.class);
        in.putExtra("data", new WorkExperience());
        in.putExtra("Type", "Experience");
        getActivity().startActivity(in);
    }

    @OnClick(R.id.qualification_add)
    public void addQualification(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        Intent in = new Intent(getActivity(), EditProfileActivity.class);
        in.putExtra("data", new Qualification());
        in.putExtra("Type", "Qualification");
        getActivity().startActivity(in);
    }


    @OnClick(R.id.ivEditBasicProfile)
    public void onClickBasicEdit(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        Intent in = new Intent(getActivity(), EditProfileActivity.class);
        in.putExtra("data", professional);
        in.putExtra("Type", "BasicDetails");
        getActivity().startActivity(in);
    }

    public static ProfileViewFragment newInstance(String param1, String param2) {
        ProfileViewFragment fragment = new ProfileViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_view, container, false);
        ButterKnife.bind(this, view);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.getListners(this);
        homeViewModel.setDeleteCallBacks(this);
        homeViewModel.setUpdateCallbacks(this);
        config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }

    private void initAdapter() {
        adapter = new ProfileSkillsRecyclerAdapter(getContext(), jobsList);
        skillsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setClickListener(this);
        skillsRecyclerView.setAdapter(adapter);

        adapterExperience = new ProfileExperienceRecyclerAdapter(getContext(), experienceList);
        experienceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterExperience.setClickListener(this);
        experienceRecyclerView.setAdapter(adapterExperience);

        adapterQualification = new ProfileQualificationRecyclerAdapter(getContext(), qualificationList);
        qualificationRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterQualification.setClickListener(this);
        qualificationRecyclerView.setAdapter(adapterQualification);

        adapterLanguages = new ProfileLanguagesRecyclerAdapter(getContext(), languagesList);
        languagesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterLanguages.setClickListener(this);
        languagesRecyclerView.setAdapter(adapterLanguages);

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity(), SHOWCASE);

        sequence.setConfig(config);

        sequence.addSequenceItem(skillView,
                "You can view your skills when you click over here", "GOT IT");

        sequence.addSequenceItem(addMoreSkills,
                "You can add more skills from here", "GOT IT");

        sequence.addSequenceItem(experienceView,
                "You can view your earlier added experience when you click over here", "GOT IT");

        sequence.addSequenceItem(qualificationView,
                "You can view your earlier added qualification when you click over here", "GOT IT");

        sequence.addSequenceItem(languageView,
                "You can view your  earlier added languages when you click over here", "GOT IT");

        sequence.start();

    }

    private void getProfileDetails() {
        if (Connectivity.hasConnection(getContext())) {
            progress.showProgressDialog(getContext(), "Loading.. Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            homeViewModel.callGetProfile();
        } else {
            ((BaseActivity) getActivity()).showToast(String.valueOf(R.string.checkInternet));
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
        professional = (Professional) object;
        professionalName.setText(professional.getFirst_name() + " " + professional.getLast_name());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.logo_grey);
        requestOptions.error(R.drawable.logo_grey);
        Glide.with(getContext())
                .load(professional.getProfile_pic())
                .into(profilePic);

        // professionalAddress.setText(professional.getCurrent_address());
        if (professional.getDate_of_birth() != null) {
            int age = getAge(professional.getDate_of_birth());
            professionalAge.setText("" + age);
        } else {
            professionalAge.setText("NA");
        }
        profileCompletePercent.setText("" + professional.getProfile_details().getProfile_completeness_percentage() + "% profile completed");
        progress_seek.setProgress(professional.getProfile_details().getProfile_completeness_percentage());
        professionalGender.setText(professional.getGender());
        professionalAddress.setText(professional.getCurrent_address());
        professionalMobile.setText(professional.getAlternate_contact_number());
        professionalExperience.setText(professional.getTotal_experience());
        List<SkillsModel> skills = professional.getSkills();
        experienceList.clear();
        if (professional.getWork_experiences() != null) {
            List<WorkExperience> experience = professional.getWork_experiences();
            experienceList.addAll(experience);
            adapterExperience.notifyDataSetChanged();
        }

        qualificationList.clear();
        if (professional.getQualifications() != null) {
            List<Qualification> qualification = professional.getQualifications();
            qualificationList.addAll(qualification);
            adapterQualification.notifyDataSetChanged();
        }
        languagesList.clear();
        if (professional.getLanguages() != null) {
            List<Languages> languages = professional.getLanguages();
            languagesList.addAll(languages);
            adapterLanguages.notifyDataSetChanged();

        }
        jobsList.clear();
        for (SkillsModel data : skills) {
            data.setSelected(true);
            jobsList.add(data);
        }

        skillsCount.setText("" + (jobsList.size()));
        experienceCount.setText("" + experienceList.size());
        qualificationCount.setText("" + qualificationList.size());
        languageCount.setText("" + languagesList.size());
        if (jobsList.size() == 0) {
            skillsNoData.setVisibility(View.VISIBLE);
        } else {
            skillsNoData.setVisibility(View.GONE);
        }

        if (experienceList.size() == 0) {
            experienceNoData.setVisibility(View.VISIBLE);
        } else {
            experienceNoData.setVisibility(View.GONE);
        }

        if (qualificationList.size() == 0) {
            qualificationNoData.setVisibility(View.VISIBLE);
        } else {
            qualificationNoData.setVisibility(View.GONE);
        }

        if (languagesList.size() == 0) {
            languagesNoData.setVisibility(View.VISIBLE);
        } else {
            languagesNoData.setVisibility(View.GONE);
        }

        adapter.notifyDataSetChanged();
    }


    public static int getAge(String dobString) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }


        return age;
    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
    }

    @Override
    public void onValidationFailed(String message) {

    }


    @Override
    public void onItemDelete(SkillsModel skill, int position) {
        JsonObject jsonObject = new JsonObject();
        JsonObject data = new JsonObject();
        data.addProperty("skill_name", skill.getSkill_name());
        data.addProperty("last_worked", skill.getLast_used());
        data.addProperty("currently_using", skill.isCurrently_using());
        data.addProperty("experience", skill.getExperience());
        jsonObject.add("skill", data);
        progress.showProgressDialog(getActivity(), "Deleting");
        homeViewModel.deleteSkillApi(skill.getPublic_id(), jsonObject);
    }

//    @Override
//    public void onItemClick(Jobs jobList, int position) {
//
//    }

    @Override
    public void onDeleteSuccess(Object object) {
        progress.hideProgressBar();
        getProfileDetails();
        initAdapter();
    }

    @Override
    public void onDeleteFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast(message);
    }

    @Override
    public void OnExperienceDelete(WorkExperience workEx, int position) {
        progress.showProgressDialog(getActivity(), "Deleting");
        homeViewModel.deleteWorkExApi(workEx.getPublic_id());
    }

    @Override
    public void onQualificationDelete(Qualification qualification) {
        progress.showProgressDialog(getActivity(), "Deleting");
        homeViewModel.deleteQUalificationApi(qualification.getPublic_id());
    }

    @Override
    public void onItemClick(Languages language, int position) {
        if (position == -1) {
            languagesList.remove(language);
        } else {
            for (Languages data : languagesList) {
                if (data.getName().equalsIgnoreCase(language.getName())) {
                    languagesList.remove(data);
                    languagesList.add(language);
                    break;
                }
            }
        }

        callLanguagesUpdate();

    }

    private void callLanguagesUpdate() {
        if (Connectivity.hasConnection(getContext())) {
            progress.showProgressDialog(getContext(), "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            Professional professional = new Professional();
            professional.setLanguages(languagesList);
            homeViewModel.callLanguagesApi(professional);

        }
    }

    @Override
    public void onUpdateSuccess(Object object) {
        progress.hideProgressBar();
        getProfileDetails();
        initAdapter();
    }

    @Override
    public void onUpdateFailure(String message) {
        progress.hideProgressBar();
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
