package com.karyamitr.workseekers.view.activity;

import android.content.Intent;
import android.os.Handler;

import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.widget.ImageView;
import android.view.animation.Animation;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.viewmodel.SplashViewModel;

import butterknife.BindView;
import io.fabric.sdk.android.Fabric;

public class Splash extends BaseActivity implements ResultCallBack {

    private static final String TAG = Splash.class.getSimpleName();
    protected Animation fadeIn;
    private static int splashTimeOut = 3000;

    Progress progress = Progress.getInstance();

    SplashViewModel splashViewModel;

    @BindView(R.id.imageView_splash)
    ImageView splashImage;


    @Override
    protected int getLayoutId() {
        setFullscreen();
        return R.layout.activity_splash;
    }

    @Override
    protected void initViews() {
        splashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        splashViewModel.getListners(this);
        Fabric.with(this, new Crashlytics());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Connectivity.hasConnection(getApplicationContext())) {
                    ApiClient.getUuid(Prefs.getUserToken(getApplicationContext()));
                    ApiClient.getTokenId(Prefs.getToken(getApplicationContext()));
                    splashViewModel.callApi();

                } else {
                    Toast.makeText(Splash.this, R.string.checkInternet, Toast.LENGTH_SHORT).show();
                }
            }
        }, splashTimeOut);
    }

    @Override
    public void onSuccess(Object object) {
        ResponseData data = (ResponseData) object;
        String launchMode = Prefs.getLaunchActivity(Splash.this);
        if (launchMode != null && Integer.parseInt(launchMode) >= 2) {
            Intent i = new Intent(Splash.this, Home.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        } else {
            Intent i = new Intent(Splash.this, Register.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }

        finish();
        Log.d(TAG, "onSuccess: " + data.toString());
    }

    @Override
    public void onFailure(String message) {
        Intent i = new Intent(Splash.this, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    public void onValidationFailed(String message) {

    }
}