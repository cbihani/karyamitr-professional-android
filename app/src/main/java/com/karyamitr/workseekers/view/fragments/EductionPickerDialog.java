package com.karyamitr.workseekers.view.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.view.activity.Home;

public class EductionPickerDialog extends DialogFragment {

    private static final int MAX_YEAR = 2099;
    View view;
    private RadioButton.OnCheckedChangeListener listener;

    public void setListener(RadioButton.OnCheckedChangeListener listener) {
        this.listener = listener;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.education_picker_dialog, null);
        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_education);
        AppCompatRadioButton other = view.findViewById(R.id.other);
        AppCompatRadioButton hiden = view.findViewById(R.id.hiden);
        EditText name = view.findViewById(R.id.name);
        other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    name.setVisibility(View.VISIBLE);
                } else {
                    name.setVisibility(View.GONE);
                }
            }
        });
        builder.setView(view).setPositiveButton(Html.fromHtml("<font color='#FF4081'>Ok</font>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (getActivity() instanceof Home)
                    ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

                int selectedId = radioGroup.getCheckedRadioButtonId();
                final RadioButton radioButton = (RadioButton) view.findViewById(selectedId);
                if (name.getVisibility() == View.GONE)
                { listener.onCheckedChanged(radioButton, true);}
                else {hiden.setText(name.getText().toString());
                    listener.onCheckedChanged(hiden, true);
                }

            }
        }).setNegativeButton(Html.fromHtml("<font color='#FF4081'>Cancel</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                EductionPickerDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }

    public void setOtherEditTextVisivilty() {

    }
}