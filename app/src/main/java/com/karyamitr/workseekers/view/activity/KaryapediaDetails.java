package com.karyamitr.workseekers.view.activity;

import android.content.Intent;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.Posts;

import butterknife.BindView;

public class KaryapediaDetails extends BaseActivity {

    @BindView(R.id.karyapedia_icon)
    ImageView karyapediaIcon;

    @BindView(R.id.title_karyapedia)
    TextView title;

    @BindView(R.id.author_date)
    TextView authorAndDate;

    @BindView(R.id.karyapedia_meta_description)
    TextView karyapediaMetaDescription;

    @BindView(R.id.karyapedia_description)
    TextView karyapediaDescription;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_karyapedia_details;
    }

    @Override
    protected void initViews() {

        Intent in = getIntent();
        Posts post = (Posts) in.getExtras().get("post");
        Glide.with(this).load(post.getHeader_image()).into(karyapediaIcon);
        title.setText(post.getTitle());
        authorAndDate.setText(post.getAuthor());
        karyapediaMetaDescription.setText(post.getMeta_description());
        karyapediaDescription.setText(Html.fromHtml(post.getContent()));


    }
}
