package com.karyamitr.workseekers.view.activity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.view.fragments.BasicProfile;
import com.karyamitr.workseekers.view.fragments.LanguagesKnown;
import com.karyamitr.workseekers.view.fragments.QualificationFragment;
import com.karyamitr.workseekers.view.fragments.Skills;
import com.karyamitr.workseekers.view.fragments.WorkExperienceFragment;

public class EditProfileActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_profile;
    }

    @Override
    protected void initViews() {
        String type = getIntent().getStringExtra("Type");
        Fragment fragment;
        if (type.equalsIgnoreCase("Experience")) {
            fragment = new WorkExperienceFragment();
        } else if (type.equalsIgnoreCase("Qualification")) {
            fragment = new QualificationFragment();
        } else if (type.equalsIgnoreCase("skills")) {
            fragment = new Skills();
        } else if (type.equalsIgnoreCase("BasicDetails")) {
            fragment = new BasicProfile();
        } else {
            fragment = new LanguagesKnown();
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_layout, fragment);
        transaction.commit();
    }
}
