package com.karyamitr.workseekers.view.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.view.activity.Home;

import java.util.Calendar;

public class YearPickerDialog extends DialogFragment {

    private  int MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    private int MIN_YEAR=1960;
    private DatePickerDialog.OnDateSetListener listener;

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }


    public void setMAX_YEAR(int MAX_YEAR) {
        this.MAX_YEAR = MAX_YEAR;
    }

    public void setMIN_YEAR(int MIN_YEAR) {
        this.MIN_YEAR = MIN_YEAR;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        Calendar cal = Calendar.getInstance();

        View dialog = inflater.inflate(R.layout.year_picker_dialog, null);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

        int year = cal.get(Calendar.YEAR);
        yearPicker.setMinValue(MIN_YEAR);
        yearPicker.setMaxValue(MAX_YEAR);
        yearPicker.setValue(year);

        builder.setView(dialog).setPositiveButton(Html.fromHtml("<font color='#FF4081'>Ok</font>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (getActivity() instanceof Home)
                    ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

                listener.onDateSet(null, yearPicker.getValue(), 0, 0);
            }
        }).setNegativeButton(Html.fromHtml("<font color='#FF4081'>Cancel</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (getActivity() instanceof Home)
                    ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

                YearPickerDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }
}