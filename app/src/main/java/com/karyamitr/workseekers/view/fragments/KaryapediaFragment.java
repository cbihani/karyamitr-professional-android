package com.karyamitr.workseekers.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.KaryapediaRecyclerAdapter;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Posts;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.Example;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.viewmodel.HomeViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KaryapediaFragment extends Fragment implements ResultCallBack, KaryapediaRecyclerAdapter.ItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    List<Posts> postList = new ArrayList<>();
    private StaggeredGridLayoutManager _sGridLayoutManager;
    Progress progress = Progress.getInstance();
    HomeViewModel homeViewModel;
    KaryapediaRecyclerAdapter recyclerViewAdapter;

    @BindView(R.id.edittext_search_jobs)
    EditText searchJobs;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public KaryapediaFragment() {
        // Required empty public constructor
    }


    public static KaryapediaFragment newInstance(String param1, String param2) {
        KaryapediaFragment fragment = new KaryapediaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_karyapedia, container, false);
        ButterKnife.bind(this, view);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.getListners(this);
        getKaryapediaData();
        initAdapter();

        searchJobs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                recyclerViewAdapter.getFilter().filter(s);


            }
        });
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }

    private void getKaryapediaData() {
        if (Connectivity.hasConnection(getContext())) {
            progress.showProgressDialog(getContext(), "Loading.. Please Wait..");
            homeViewModel.callKaryapediaApi();
        } else {
            ((BaseActivity) getActivity()).showToast(String.valueOf(R.string.checkInternet));
        }
    }

    private void initAdapter() {
        int numberOfColumns = 2;
        _sGridLayoutManager = new StaggeredGridLayoutManager(numberOfColumns,
                StaggeredGridLayoutManager.VERTICAL);

        recyclerViewAdapter = new KaryapediaRecyclerAdapter(getContext(), postList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewAdapter.setClickListener(this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
        List<Posts> posts = (List<Posts>) object;
        postList.addAll(posts);
        recyclerViewAdapter.notifyDataSetChanged();

    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast("No more data");
    }

    @Override
    public void onValidationFailed(String message) {

    }

    @Override
    public void onItemClick(List<Posts> post, int position) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        Intent in = new Intent(getContext(), Example.class);
        in.putExtra("post", (Serializable) post);
        in.putExtra("position", position);
        startActivity(in);

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
