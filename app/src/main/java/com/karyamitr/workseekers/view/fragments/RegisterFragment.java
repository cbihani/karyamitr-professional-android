package com.karyamitr.workseekers.view.fragments;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.utils.TermsAndConditions;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.RegisterViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterFragment extends Fragment implements ResultCallBack {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "RegisterFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private TextView text;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.edittext_first_name)
    EditText firstName;

    @BindView(R.id.edittext_last_name)
    EditText lastName;

    @BindView(R.id.checkbox_tc)
    CheckBox termsAndConditions;

    @BindView(R.id.text_terms)
    TextView terms;

    @BindView(R.id.text_termsclick)
    TextView textClickable;

    @BindView(R.id.privacy)
    TextView privacy;

    Progress progress = Progress.getInstance();
    RegisterViewModel registerViewModel;
    View view;
    static int currentPage;

    public static RegisterFragment newInstance(int page, boolean isLast) {

        currentPage = page;
        Bundle args = new Bundle();
        args.putInt("page", page);
        if (isLast)
            args.putBoolean("isLast", true);
        final RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
        registerViewModel.getListners(this);
        firstName.setHint(Html.fromHtml("First Name  <font color='red'>*</font>"));
        lastName.setHint(Html.fromHtml("Last Name   <font color='red'>*</font>"));

        String checkBoxText = "I agree to all the ";
        String clickText = "<a href=''> Terms & Conditions</a> <font color='red'>*</font>";
        terms.setText(Html.fromHtml(checkBoxText));
        textClickable.setText(Html.fromHtml(clickText));
        textClickable.setClickable(true);
        textClickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), TermsAndConditions.class);
                in.putExtra("url", "https://professionals-development-api.herokuapp.com/terms_of_use");
                startActivity(in);
            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), TermsAndConditions.class);
                in.putExtra("url", "https://professionals-development-api.herokuapp.com/privacy_policy");
                startActivity(in);
            }
        });
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;

    }


    @OnClick(R.id.button_register)
    public void onClickRegister(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (Connectivity.hasConnection(getContext())) {
            if (registerViewModel.checkValidation(firstName.getText().toString(), lastName.getText().toString(),termsAndConditions.isChecked())) {
                progress.showProgressDialog(getContext(), "Please Wait..");
                ApiClient.getUuid(Prefs.getUserToken(getContext()));
                ApiClient.getTokenId(Prefs.getToken(getContext()));
                registerViewModel.callApi(firstName.getText().toString(), lastName.getText().toString());
            }
        } else {
            ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
        }


    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final int page = getArguments().getInt("page", 0);
//        if (getArguments().containsKey("isLast"))
//          //  text.setText("You're done!");
//        else
//            text.setText(Integer.toString(page));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
        ResponseData data = (ResponseData) object;

        Log.d(TAG, "onSuccess: " + data.toString());
        if (data.isSuccess()) {
            ((Register) getActivity()).setCurrentItem(1, true);
        }
        ((BaseActivity) getActivity()).showToast("Details added successfully");
        Prefs.setLaunchActivity(getActivity(), "1");
    }


    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {
        ((Register) getActivity()).showSnackbar(view, message);
        if (message.equalsIgnoreCase("Please enter first name")) {
            firstName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_black_24dp, 0);
            firstName.setBackground(getResources().getDrawable(R.drawable.edittext_background_error));
        }
        if (message.equalsIgnoreCase("Please enter last name")) {
            lastName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_black_24dp, 0);
            lastName.setBackground(getResources().getDrawable(R.drawable.edittext_background_error));
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
