package com.karyamitr.workseekers.view.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.KaryapediaViewPagerAdapter;
import com.karyamitr.workseekers.model.Posts;
import com.karyamitr.workseekers.view.fragments.KaryapediaFragment;
import com.karyamitr.workseekers.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Example extends AppCompatActivity implements View.OnClickListener, KaryapediaFragment.OnFragmentInteractionListener {

    @BindView(R.id.recycler_example)
    RecyclerView recycler_example;

    KaryapediaViewPagerAdapter adapter;

    HomeViewModel homeViewModel;

    List<Posts> postList = new ArrayList<>();
    int position = 0;
    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.next)
    TextView next;

    @BindView(R.id.previous)
    TextView previous;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_example);
        ButterKnife.bind(this);
        next.setOnClickListener(this);
        previous.setOnClickListener(this);
        Intent in = getIntent();
        List<Posts> post = (List<Posts>) in.getExtras().get("post");
        position = in.getExtras().getInt("position");
        adapter = new KaryapediaViewPagerAdapter(this, post);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler_example.setLayoutManager(linearLayoutManager);
        recycler_example.getLayoutManager().scrollToPosition(position);

        recycler_example.setAdapter(adapter);
        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recycler_example);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                recycler_example.getLayoutManager().scrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                break;
            case R.id.previous:
                recycler_example.getLayoutManager().scrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                break;
        }


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
