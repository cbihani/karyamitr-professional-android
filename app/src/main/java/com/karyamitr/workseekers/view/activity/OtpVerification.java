package com.karyamitr.workseekers.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karyamitr.workseekers.MySMSBroadcastReceiver;
import com.mukesh.OtpView;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.helper.SmsListener;
import com.karyamitr.workseekers.helper.SmsReceiver;
import com.karyamitr.workseekers.model.OtpResponse;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.viewmodel.OtpViewModel;


import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

public class OtpVerification extends BaseActivity implements ResultCallBack, MySMSBroadcastReceiver.Action {

    Progress progress = Progress.getInstance();
    OtpViewModel otpViewModel;
    String uuid;

    private static final String TAG = OtpVerification.class.getSimpleName();
    @BindView(R.id.otp_view)
    OtpView otpView;

    @BindView(R.id.otp_sub_header)
    TextView otpSubHeader;

    @BindView(R.id.resend_otp)
    TextView resendOtp;

    @BindView(R.id.otp_root)
    RelativeLayout rootLayout;

    int count = 1;
    int time = 30000;

    @Override
    protected int getLayoutId() {
        setFullscreen();
        return R.layout.activity_otp_verification;
    }

    @Override
    protected void initViews() {

        Intent intent = getIntent();
        String mobile = intent.getExtras().getString("mobile");
        otpSubHeader.setText("OTP is sent to registered mobile number. \n +91 xxxxxxxxx" + mobile.substring(8, 10));
        uuid = Prefs.getUserToken(this);
//        SmsReceiver.bindListener(new SmsListener() {
//            @Override
//            public void messageReceived(String messageText) {
//                showToast(messageText);
//                otpView.setText(messageText);
//                onOtpVerification(otpView);
//                otpView.setTextColor(getResources().getColor(R.color.colorPrimary));
//            }
//        });
        getRetriver();
        otpViewModel = ViewModelProviders.of(this).get(OtpViewModel.class);
        otpViewModel.getListners(this);

        callTimer();

    }

    private void callTimer() {

        new CountDownTimer(time, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                resendOtp.setText("" + String.format("%d MM : %d SS",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                resendOtp.setText(R.string.resend_otp);
                if (count < 3) {
                    count++;

                } else {
                    resendOtp.setVisibility(View.GONE);
                }

            }
        }.start();
    }


    @OnClick(R.id.button_verify_otp)
    public void onOtpVerification(View view) {

//        hideKeyboard();
        if (Connectivity.hasConnection(this)) {
            if (otpViewModel.checkValidation(otpView.getText().toString())) {
                progress.showProgressDialog(this, "Verifying OTP..  Please Wait..");
                otpViewModel.callApi(uuid, otpView.getText().toString());
            }
        } else {
            showSnackbar(view, "Please check internet connection");
        }

    }

    @OnClick(R.id.change_number)
    public void changeMobileNo(View view) {
        Intent in = new Intent(this, Login.class);
        startActivity(in);
        finish();
    }

    @OnClick(R.id.resend_otp)
    public void onResendOtp(View view) {
        Log.e("onResendOtp", "--1");
        if (count == 1) {
            time = 30000;
        }
        if (count == 2) {
            time = 60000;
        }
        if (count == 3) {
            time = 90000;
        }

        try {
            if (Connectivity.hasConnection(this)) {
                ApiClient.getUuid(Prefs.getUserToken(this));
                progress.showProgressDialog(this, "Please Wait..");
                otpViewModel.callApiForResendOtp();

            } else {
                showSnackbar(view, "Please check internet connection");
            }
        } catch (Exception e) {
            Log.d(TAG, "onOtpVerification: " + e.getMessage());
        }

        callTimer();

    }


    @Override
    public void onSuccess(Object object) {

        Log.d(TAG, "onSuccess: " + object.toString());
//        ResponseData obj = (ResponseData) object;
//        if (obj.getMessage().equals("OTP has been sent succesfully to your registered number.")) {
//            showToast(obj.getMessage());
//        } else {
        OtpResponse data = (OtpResponse) object;
        Log.d(TAG, "onSuccess: " + data.toString());
        progress.hideProgressBar();
        if (data.getMessage().equals("OTP has been sent succesfully to your registered number.")) {
            showToast(data.getMessage());
        } else {
            if (data.isSuccess()) {
                Intent in;
                if (data.isUserConfirmed()) {
                    in = new Intent(this, Home.class);
                } else {
                    in = new Intent(this, Register.class);
                }
                Prefs.setToken(this, data.getAuthenticationToken());
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }

            showToast(data.getMessage());
        }
    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {
        showSnackbar(rootLayout, message);
    }

    MySMSBroadcastReceiver smsReceiver;

    private void getRetriver() {

        Task<Void> task = SmsRetriever.getClient(this /* context */).startSmsUserConsent(null);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                smsReceiver = new MySMSBroadcastReceiver();
                smsReceiver.setAction(OtpVerification.this);
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
                registerReceiver(smsReceiver, intentFilter);
                // Successfully started retriever, expect broadcast intent
                // ...
//                Intent broadcastIntent = new Intent();
//                broadcastIntent.setAction(SmsRetriever.SMS_RETRIEVED_ACTION);
//                broadcastIntent.setClass(OtpVerification.this, MySMSBroadcastReceiver.class);
//                sendBroadcast(broadcastIntent);
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Intent i = new Intent("com.mycompany.myapp.SOME_MESSAGE");
                sendBroadcast(i);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // ...
            case MySMSBroadcastReceiver.SMS_CONSENT_REQUEST:
                if (resultCode == RESULT_OK) {
                    // Get SMS message content
                    String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                    Log.e("TAG", "--" + message);
                    otpView.setText(message.replaceAll("[^0-9]", ""));

                } else {
                    // Consent canceled, handle the error ...
                }
                break;
        }
    }

    @Override
    public void onSucessSMS(Intent consentIntent, int SMS_CONSENT_REQUEST) {
        startActivityForResult(consentIntent, SMS_CONSENT_REQUEST);

    }
}

