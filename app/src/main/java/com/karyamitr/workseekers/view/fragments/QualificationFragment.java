package com.karyamitr.workseekers.view.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.QualificationAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Qualification;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.RegisterViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class QualificationFragment extends Fragment implements ResultCallBack, QualificationAdapter.ItemClickListener {

    String type = "new";
    int adapterPosition;

    public QualificationFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.textview_start_course)
    TextView startCourse;

    @BindView(R.id.textview_end_course)
    TextView endCourse;

    @BindView(R.id.edittext_education)
    EditText editTextEducation;

    @BindView(R.id.edittext_university)
    EditText edittextUniversity;

    @BindView(R.id.checkbox_pursuing)
    CheckBox isPursuing;

    @BindView(R.id.edittext_course)
    EditText edittext_course;

    @BindView(R.id.relativeLayout_qualification)
    RelativeLayout relativeLayout_qualification;

    @BindView(R.id.recycler_view_qualification)
    RecyclerView recycler_view_qualification;

    @BindView(R.id.cancel)
    TextView cancel;

    Progress progress = Progress.getInstance();
    RegisterViewModel registerViewModel;
    Qualification qualification;
    List<Qualification> qualificationList = new ArrayList<>();

    QualificationAdapter adapter;

    View view;

    public static QualificationFragment newInstance(int page, boolean isLast) {
        Bundle args = new Bundle();
        args.putInt("page", page);
        if (isLast)
            args.putBoolean("isLast", true);
        final QualificationFragment fragment = new QualificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.skip_option)
    public void skipOtion(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
//        Intent i = new Intent(getContext(), Home.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(i);
//        getActivity().finish();
        ((Register) getActivity()).setCurrentItem(5, true);
    }

    @OnClick(R.id.cancel)
    public void onCancelClick(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (relativeLayout_qualification.getVisibility() == View.VISIBLE)
            relativeLayout_qualification.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_qualification, container, false);
        ButterKnife.bind(this, view);
        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
        registerViewModel.getQualificationListner(this);
        editTextEducation.setHint(Html.fromHtml("Qualification <font color='red'>*</font>"));
        edittextUniversity.setHint(Html.fromHtml("University name <font color='red'>*</font>"));
//        city.setHint(Html.fromHtml("City <font color='red'>*</font>"));

        recycler_view_qualification.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        adapter = new QualificationAdapter(getContext(), qualificationList);
        adapter.setClickListener(this);
        recycler_view_qualification.setAdapter(adapter);
        qualification = (Qualification) getActivity().getIntent().getSerializableExtra("data");
//        isPursuing.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            if (isChecked) {
//                endCourse.setText("");
//            }
//        });

        if (qualification != null)
            setDataToViews();
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }

    private void setDataToViews() {
//        callLayerView();
        if (relativeLayout_qualification.getVisibility() == View.GONE)
            relativeLayout_qualification.setVisibility(View.VISIBLE);
        editTextEducation.setText(qualification.getDegree());
        edittextUniversity.setText(qualification.getUniversity());
        startCourse.setText(qualification.getCourse_start_year());
        endCourse.setText(qualification.getCourse_end_year());
//        edittext_course.setText(qualification.c);
        cancel.setVisibility(View.GONE);

    }


    @OnClick(R.id.textview_start_course)
    public void openDialogForStartCourse(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        showYearPickerForStartCourse();
    }

    @OnClick(R.id.textview_end_course)
    public void openDialogForEndCourse(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
//        if (!isPursuing.isChecked())
            showYearPickerForEndCourse();
    }


    @OnClick(R.id.edittext_education)
    public void openDialogForEducation(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (!isPursuing.isChecked())
            showDialogForEducation();
    }


    private void showDialogForEducation() {
        EductionPickerDialog educationType = new EductionPickerDialog();
        educationType.show(getFragmentManager(), "Date Picker");
        educationType.setListener(onDataChanged);

    }

    RadioButton.OnCheckedChangeListener onDataChanged = new RadioButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView != null)
                editTextEducation.setText(buttonView.getText());

        }
    };


    DatePickerDialog.OnDateSetListener ondateForEndcourse = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            endCourse.setText(String.valueOf(year));
        }
    };


    private void showYearPickerForStartCourse() {
        YearPickerDialog date = new YearPickerDialog();
        date.setListener(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    private void showYearPickerForEndCourse() {
        YearPickerDialog date = new YearPickerDialog();
        try {
            date.setMIN_YEAR(Integer.parseInt(startCourse.getText().toString()));
        } catch (Exception ex) {
        } finally {
            date.setListener(ondateForEndcourse);
            date.show(getFragmentManager(), "Date Picker");
        }
    }


    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            startCourse.setText(String.valueOf(year));
            if (!isPursuing.isChecked()) endCourse.setText("");
        }
    };

    @OnClick(R.id.textview_add_qualification)
    public void add(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        callLayerView();
    }


    @OnClick(R.id.button_save_all_qualifications)
    public void saveAllQualifications(View view) {
        Log.e("Add qualification","--1");

        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (qualificationList.size() > 0) {
            if (Connectivity.hasConnection(getContext())) {
                progress.showProgressDialog(getContext(), "Please Wait..");
                ApiClient.getUuid(Prefs.getUserToken(getContext()));
                ApiClient.getTokenId(Prefs.getToken(getContext()));
                JsonObject object = null;
//            code to be made generic
                JsonArray jsonArray = new JsonArray();
                for (Qualification mQualification : qualificationList) {
                    object = new JsonObject();
                    object.addProperty("degree", mQualification.getDegree());
                    object.addProperty("university", mQualification.getUniversity());
                    object.addProperty("pursuing", mQualification.isPursuing());
                    object.addProperty("course_start_year", mQualification.getCourse_start_year());
                    object.addProperty("course_end_year", mQualification.getCourse_end_year());
                    jsonArray.add(object);
                }

                JsonObject obj = new JsonObject();
                obj.add("qualifications", jsonArray);

                JsonObject qualify = new JsonObject();
                qualify.add("qualification", object);
                Log.e("Add qualification","--"+object);
                if (qualification != null && qualification.getPublic_id() != null) {
                    registerViewModel.callApiForUpdateQualification(qualification.getPublic_id(), qualify);
                } else {
                    registerViewModel.callQualificationApi(obj);
                }

//            ----------End----------------


            }
//            final Animation animationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
//            relativeLayout_qualification.startAnimation(animationFadeOut);
            else {
                ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
            }
        } else {
            ((Register) getActivity()).showSnackbar(view, "Please add qualification");
        }


    }


    @OnClick(R.id.button_save_qualification)
    public void addQualification(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (registerViewModel.checkValidationForQualification(editTextEducation.getText().toString(), edittextUniversity.getText().toString(),startCourse.getText().toString(),endCourse.getText().toString())) {
            if (qualification == null)
                if (relativeLayout_qualification.getVisibility() == View.VISIBLE)
                    relativeLayout_qualification.setVisibility(View.GONE);

            Qualification qualification = new Qualification();
            qualification.setDegree(editTextEducation.getText().toString());
            qualification.setUniversity(edittextUniversity.getText().toString());
            qualification.setCourse_start_year(startCourse.getText().toString());
            qualification.setCourse_end_year(endCourse.getText().toString());
            qualification.setPursuing(isPursuing.isChecked());

            if (this.qualification == null) {
                if (type.equals("new")) {
                    qualificationList.add(qualification);
                    setUpRecyclerView();
                } else {
                    qualificationList.set(adapterPosition, qualification);
                    adapter.notifyDataSetChanged();
                    adapter.notifyItemChanged(adapterPosition);

                }

                editTextEducation.getText().clear();
                edittextUniversity.getText().clear();
                startCourse.setText("");
                endCourse.setText("");
                isPursuing.setChecked(false);
            } else {
                qualificationList.add(qualification);
                saveAllQualifications(null);
            }
        }

    }

    private void setUpRecyclerView() {
        if (qualificationList.size() >= 1) {
            adapter.notifyDataSetChanged();
        }


    }


    @Override
    public void onSuccess(Object object) {


        progress.hideProgressBar();
        ResponseData data = (ResponseData) object;

        if (qualification == null) {
            if (data.isSuccess()) {
                ((Register) getActivity()).setCurrentItem(5, true);
            }
            ((BaseActivity) getActivity()).showToast(data.getMessage());
            Prefs.setLaunchActivity(getActivity(), "5");
        } else if (getActivity()!=null)  {
            getActivity().finish();
        }


    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {
        if (getActivity()!=null)
        ((BaseActivity) getActivity()).showSnackbar(view, message);
    }

    @Override
    public void onItemClick(Qualification mQualification, int position) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        Log.d(TAG, "onItemClick: " + mQualification.getDegree());
        editTextEducation.setText(mQualification.getDegree());
        edittextUniversity.setText(mQualification.getUniversity());
        startCourse.setText(mQualification.getCourse_start_year());
        endCourse.setText(mQualification.getCourse_end_year());
        isPursuing.setChecked(mQualification.isPursuing());

        type = "edit";
        adapterPosition = position;
        callLayerView();

    }

    private void callLayerView() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        relativeLayout_qualification.startAnimation(animationFadeIn);
        if (relativeLayout_qualification.getVisibility() == View.GONE)
            relativeLayout_qualification.setVisibility(View.VISIBLE);
    }


}
