package com.karyamitr.workseekers.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.model.ApplicantDocumentList;

import butterknife.BindView;
import butterknife.OnClick;

public class DocumentsNeeded extends BaseActivity {

    @BindView(R.id.layout_rc)
    LinearLayout layoutRc;
    @BindView(R.id.layout_pan)
    LinearLayout layoutPan;
    @BindView(R.id.layout_aadhar)
    LinearLayout layoutAadhar;
    @BindView(R.id.layout_driving_licence)
    LinearLayout layoutDrivingLicence;
    @BindView(R.id.layout_bike)
    LinearLayout layoutBike;
    @BindView(R.id.layout_cycle)
    LinearLayout layoutCycle;
    @BindView(R.id.layout_local_address)
    LinearLayout layoutLocalAddress;
    @BindView(R.id.radio_group_aadhar)
    RadioGroup radioGroupAadhar;
    @BindView(R.id.radio_group_address)
    RadioGroup radioGroupAddress;
    @BindView(R.id.radio_group_bike)
    RadioGroup radioGroupBike;
    @BindView(R.id.radio_group_cycle)
    RadioGroup radioGroupCycle;
    @BindView(R.id.radio_group_driving)
    RadioGroup radioGroupDriving;
    @BindView(R.id.radio_group_pan)
    RadioGroup radioGroupPan;
    @BindView(R.id.radio_group_rc)
    RadioGroup radioGroupRc;

    JsonObject json = new JsonObject();

    @OnClick(R.id.button_apply)
    public void onApply(View view) {


        Intent resultIntent = new Intent();
        resultIntent.putExtra("applicant_document_list", json.toString());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_documents_needed;
    }

    @Override
    protected void initViews() {

        Intent in = getIntent();
        ApplicantDocumentList applicantDocumentList = (ApplicantDocumentList) in.getExtras().get("applicant_documents");
        if (applicantDocumentList.isHas_rc()) {
            layoutRc.setVisibility(View.VISIBLE);
            radioGroupRc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_rc", true);
                    } else {
                        json.addProperty("has_rc", false);
                    }
                }
            });


        }
        if (applicantDocumentList.isHas_aadhar()) {
            layoutAadhar.setVisibility(View.VISIBLE);
            radioGroupAadhar.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_aadhar", true);
                    } else {
                        json.addProperty("has_aadhar", false);
                    }
                }
            });
        }
        if (applicantDocumentList.isHas_pan()) {
            layoutPan.setVisibility(View.VISIBLE);
            radioGroupPan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_pan", true);
                    } else {
                        json.addProperty("has_pan", false);
                    }
                }
            });
        }
        if (applicantDocumentList.isHas_driving_license()) {
            layoutDrivingLicence.setVisibility(View.VISIBLE);
            radioGroupDriving.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_driving_license", true);
                    } else {
                        json.addProperty("has_driving_license", false);
                    }
                }
            });
        }
        if (applicantDocumentList.isHas_bike()) {
            layoutBike.setVisibility(View.VISIBLE);
            radioGroupBike.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_bike", true);
                    } else {
                        json.addProperty("has_bike", false);
                    }
                }
            });
        }
        if (applicantDocumentList.isHas_cycle()) {
            layoutCycle.setVisibility(View.VISIBLE);
            radioGroupCycle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_cycle", true);
                    } else {
                        json.addProperty("has_cycle", false);
                    }
                }
            });
        }
        if (applicantDocumentList.isHas_local_address_proof()) {
            layoutLocalAddress.setVisibility(View.VISIBLE);
            radioGroupAddress.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = (RadioButton) findViewById(checkedId);
                    String rc = rb.getText().toString();
                    if (rc.equalsIgnoreCase("yes")) {
                        json.addProperty("has_local_address_proof", true);
                    } else {
                        json.addProperty("has_local_address_proof", false);
                    }
                }
            });
        }
    }


}
