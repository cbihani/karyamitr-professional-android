package com.karyamitr.workseekers.view.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.BuildConfig;
import com.karyamitr.workseekers.MyApplication;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.PlacesAutoCompleteAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.AppExecutors;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.EditProfileActivity;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.RegisterViewModel;

import net.gotev.uploadservice.UploadService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class BasicProfile extends Fragment implements ResultCallBack, PlacesAutoCompleteAdapter.LoaderEvent {


    public BasicProfile() {

    }

    @BindView(R.id.spinner_total_experience)
    AppCompatEditText spinnerTotalExperience;

    @BindView(R.id.imageProfile)
    ImageView imageProfile;

    @BindView(R.id.loaderCurrent)
    ProgressBar loaderCurrent;

    @BindView(R.id.loaderParmanent)
    ProgressBar loaderParmanent;

    @BindView(R.id.textUploadImage)
    TextView textUploadImage;

    @BindView(R.id.spinner_current_salary)
    Spinner spinnerCurrentSalary;

    @BindView(R.id.spinner_expected_salary)
    Spinner spinnerExpectedSalary;

    @BindView(R.id.img_view_female)
    ImageView imageViewFemale;

    @BindView(R.id.img_view_male)
    ImageView imageViewMale;

    @BindView(R.id.edittext_current_locality)
    TextView currentLocality;

    @BindView(R.id.edittext_permanent_locality)
    TextView permanentLocality;

    @BindView(R.id.edittext_birthdate)
    EditText birthDate;

    @BindView(R.id.edittext_mobile)
    EditText mobileNo;

    @BindView(R.id.edittext_current_salary)
    EditText currentSalary;

    @BindView(R.id.edittext_expected_salary)
    EditText expectedSalary;

    @BindView(R.id.checkbox_fresher)
    CheckBox isFresher;

    @BindView(R.id.img_btn_blank)
    TextView textGender;

    String gender;
    private static final String TAG = "BasicProfile";
    View view;

    Progress progress = Progress.getInstance();
    RegisterViewModel registerViewModel;
    String encoded;
    Professional professional;

    // TODO: Rename and change types and number of parameters
    public static BasicProfile newInstance(int page, boolean isLast) {
        Bundle args = new Bundle();
        args.putInt("page", page);
        if (isLast)
            args.putBoolean("isLast", true);
        final BasicProfile fragment = new BasicProfile();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

// Initialize Places.
        Places.initialize(getActivity().getApplicationContext(), "AIzaSyB_pSVdaaZ95D2BlsqKID9fUFa-Oi_Ul2I");

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_basic_profile, container, false);
        ButterKnife.bind(this, view);
//
        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
        registerViewModel.getBasicProfileListner(this);
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

        UploadService.NAMESPACE = "com.karyamitr.workseekers";

        currentLocality.setHint(Html.fromHtml("Current Residence Locality <font color='red'>*</font>"));
        //mobileNo.setHint(Html.fromHtml("Alternate mobile <font color='red'>*</font>"));
        textGender.setText(Html.fromHtml("Gender <font color='red'>*</font>"));
        birthDate.setHint(Html.fromHtml("BirthDate <font color='red'>*</font>"));

        ArrayAdapter<CharSequence> totalExperienceAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.totalYearsExperience, R.layout.custom_textview);
        totalExperienceAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
//        spinnerTotalExperience.setAdapter(totalExperienceAdapter);


        ArrayAdapter<CharSequence> currentSalaryAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.currentSalary, R.layout.custom_textview);
        currentSalaryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerCurrentSalary.setAdapter(currentSalaryAdapter);


        ArrayAdapter<CharSequence> expectedSalaryAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.expectedSalary, R.layout.custom_textview);
        expectedSalaryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerExpectedSalary.setAdapter(expectedSalaryAdapter);


        isFresher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    spinnerTotalExperience.setEnabled(false);
                    spinnerTotalExperience.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                    currentSalary.setEnabled(false);
                    currentSalary.setBackgroundColor(getResources().getColor(R.color.colorBackground));

                } else {
                    spinnerTotalExperience.setEnabled(true);
                    currentSalary.setEnabled(true);
                    spinnerTotalExperience.setBackground(getResources().getDrawable(R.drawable.edittext_background));
                    currentSalary.setBackground(getResources().getDrawable(R.drawable.edittext_background));
                }
            }
        });
//relativeCurrent.setOnTouchListener((v, event) -> {
//    Log.d(TAG, "onClickCurrentLocality: clicked1");
//    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
//
//    Intent intent = new Autocomplete.IntentBuilder(
//            AutocompleteActivityMode.FULLSCREEN, fields)
//            .build(getActivity());
//    startActivityForResult(intent, CURRENT_REQUEST_CODE);
//});
        currentLocality.setOnClickListener((v) -> {
            Log.e(TAG, "onClickCurrentLocality: clicked1");
            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

            Intent intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields)
                    .build(getActivity());
            startActivityForResult(intent, CURRENT_REQUEST_CODE);
        });

        relativePermanent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "onClickCurrentLocality: clicked2");
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(getActivity());
                startActivityForResult(intent, PERMENANT_REQUEST_CODE);
            }
        });

        professional = (Professional) getActivity().getIntent().getSerializableExtra("data");

        if (professional != null)
            setDataToView();
        ((BaseActivity) getActivity()).hideKeyboard();
        return view;

    }

    private void setDataToView() {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.logo_grey);
        requestOptions.error(R.drawable.logo_grey);

        if (professional.getProfile_pic() != null && !(professional.getProfile_pic().equals("/assets/default_profile-71be094f2379b1fffc1f595181f962403d5eca32ef2c063a6b62bbdbe678d0ff.png"))) {
            textUploadImage.setVisibility(View.GONE);
            Glide.with(getContext())
                    .load(professional.getProfile_pic())
                    .into(imageProfile);
            imageProfile.setVisibility(View.VISIBLE);
        }

        if (professional.isIs_fresher()) {
            isFresher.setChecked(true);
        }

        if (professional.getGender() != null) {
            if (professional.getGender().equalsIgnoreCase("Female")) {
                gender = "Female";
                imageViewFemale.setImageDrawable(getResources().getDrawable(R.drawable.girlcolored));
                imageViewMale.setImageDrawable(getResources().getDrawable(R.drawable.boy));
            } else {
                gender = "Male";
                imageViewFemale.setImageDrawable(getResources().getDrawable(R.drawable.girl));
                imageViewMale.setImageDrawable(getResources().getDrawable(R.drawable.boycolored));
            }
        }

        if (professional.getTotal_experience() != null)
            spinnerTotalExperience.setText(professional.getTotal_experience());

        if (professional.getCurrent_salary() != null)
            currentSalary.setText(professional.getCurrent_salary());

        if (professional.getExpected_salary() != null)
            expectedSalary.setText(professional.getExpected_salary());

        mobileNo.setText(professional.getAlternate_contact_number());
        currentLocality.setText(professional.getCurrent_address());
        permanentLocality.setText(professional.getPermanent_address());
        if (professional.getDate_of_birth() != null) {
            //  int age = ProfileViewFragment.getAge(professional.getDate_of_birth());
            birthDate.setText(professional.getDate_of_birth());
        } else {
            birthDate.setText("NA");
        }
    }

    int CURRENT_REQUEST_CODE = 1;
    int PERMENANT_REQUEST_CODE = 2;


//    @BindView(R.id.edittext_current_locality)
//    View relativeCurrent;

    @BindView(R.id.relativePermanent)
    View relativePermanent;

    @OnClick(R.id.button_add_profile)
    public void addProfile(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (Connectivity.hasConnection(getContext())) {

            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
            Professional professional = new Professional();
            professional.setCurrent_address(currentLocality.getText().toString());
            professional.setPermanent_address(permanentLocality.getText().toString());
            professional.setAlternate_contact_number(mobileNo.getText().toString());
            professional.setGender(gender);
            professional.setDate_of_birth(birthDate.getText().toString());
            professional.setIs_fresher(isFresher.isChecked());
            professional.setTotal_experience(spinnerTotalExperience.getText().toString());
            professional.setCurrent_salary(currentSalary.getText().toString());
            professional.setExpected_salary(expectedSalary.getText().toString());
            // professional.setProfile_pic(encoded);
            Log.d(TAG, "addProfile: " + professional.toString());
            if (registerViewModel.checkValidationForProfessional(professional)) {
                progress.showProgressDialog(getContext(), "Please Wait..");
                registerViewModel.callApi(professional);
            }
            if (uri != null) {
                AppExecutors.getInstance().diskIO().execute(() -> {
                    (new ApiClient()).sendMultipart(getContext(), uri);
                });

            }

        } else {
            ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
        }
    }


    @OnClick(R.id.imageUpload)
    public void getProfileImage(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        callDialog();
    }


    @OnClick(R.id.imageProfile)
    public void setImage(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        callDialog();
    }

    private void callDialog() {
        Dialog uploadPhoto = new Dialog(getActivity());
        uploadPhoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        uploadPhoto.setContentView(R.layout.upload_image);
        TextView tvCamera, tvGallery;
        tvCamera = uploadPhoto.findViewById(R.id.tvCamera);
        tvGallery = uploadPhoto.findViewById(R.id.tvGallery);
        uploadPhoto.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        uploadPhoto.show();

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof Home)
                    ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            2001);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,
                            1111);

                }
                uploadPhoto.dismiss();
            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof Home)
                    ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
                uploadPhoto.dismiss();
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            2000);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, 2222);
                }
            }
        });

    }

    Uri uri;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("--", "--" + requestCode);
        Bitmap bmp = null;
        if (requestCode == 1111) {
            if (resultCode == RESULT_OK) {
                bmp = (Bitmap) data.getExtras().get("data");
                uri = getImageUri(getContext(), bmp);
            }
        } else if (requestCode == 2222) {
            uri = data.getData();

            try {
                bmp = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (requestCode == CURRENT_REQUEST_CODE) {
            Log.e("--", "--2");

            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                currentLocality.setText(MyApplication.getAddress(place));
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == PERMENANT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                permanentLocality.setText(MyApplication.getAddress(place));
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (bmp != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
            byte[] byteArray = stream.toByteArray();

            // convert byte array to Bitmap
            Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imageProfile.setVisibility(View.VISIBLE);
            imageProfile.setImageBitmap(bitmap);
            textUploadImage.setVisibility(View.GONE);

//            encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
            try {
                encoded = URLEncoder.encode(Base64.encodeToString(stream.toByteArray(), Base64.NO_WRAP), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.d(TAG, "onActivityResult: Phaata");
            }
            Log.d(TAG, "onActivityResult: " + encoded);


        }

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @OnClick(R.id.img_view_female)
    public void onImageClickFemale(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        gender = "Female";
        imageViewFemale.setImageDrawable(getResources().getDrawable(R.drawable.girlcolored));
        imageViewMale.setImageDrawable(getResources().getDrawable(R.drawable.boy));
    }

    @OnClick(R.id.img_view_male)
    public void onImageClickMale(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        gender = "Male";
        imageViewFemale.setImageDrawable(getResources().getDrawable(R.drawable.girl));
        imageViewMale.setImageDrawable(getResources().getDrawable(R.drawable.boycolored));
    }

    @OnClick(R.id.edittext_birthdate)
    public void onDatePicker(View view) {
        if (getActivity() instanceof Home)
            ((Home) getActivity()).setBottomBarVisivelty(View.VISIBLE);
        showDatePicker();
    }


    private void showDatePicker() {

        DatePickerFragment date = new DatePickerFragment();
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            birthDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1)
                    + "/" + String.valueOf(year));
        }
    };


    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
        ResponseData data = (ResponseData) object;
        Log.d(TAG, "onSuccess: " + data.toString());
        if (data.isSuccess() && getActivity() instanceof Register) {
            ((Register) getActivity()).setCurrentItem(2, true);
            ((Register) getActivity()).showToast(data.getMessage());
        } else {
            ((BaseActivity) getActivity()).showToast(data.getMessage());
            getActivity().finish();
        }

        Prefs.setLaunchActivity(getActivity(), "2");
    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {
        if (getActivity() instanceof Register)
            ((Register) getActivity()).showSnackbar(view, message);
        else
            ((EditProfileActivity) getActivity()).showSnackbar(view, message);

    }

    @Override
    public void hideLoader(int type) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (type == 1) {
                    loaderCurrent.setVisibility(View.GONE);
                } else if (type == 2) {
                    loaderParmanent.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void showLoader(int type) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (type == 1) {
                    loaderCurrent.setVisibility(View.VISIBLE);
                } else if (type == 2) {
                    loaderParmanent.setVisibility(View.VISIBLE);
                }
            }
        });

    }

}
