package com.karyamitr.workseekers.view.activity;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.LanguagesRecyclerViewAdapter;
import com.karyamitr.workseekers.adapter.SkillRecyclerViewAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Languages;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.SkillsModel;
import com.karyamitr.workseekers.utils.CallBackHelper;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class EditSkillsAndLanguages extends BaseActivity implements SkillRecyclerViewAdapter.ItemClickListener, LanguagesRecyclerViewAdapter.ItemClickListener, ResultCallBack {

    @BindView(R.id.relativeLayout)
    RelativeLayout skillsLayout;

    @BindView(R.id.languageScroll)
    NestedScrollView languageScroll;

    @BindView(R.id.relativeLayoutLang)
    RelativeLayout languagesLayout;

    @BindView(R.id.rvLanguages)
    RecyclerView rvLanguages;

    @BindView(R.id.root_edit_skills)
    RelativeLayout rootLayout;

    String type;
    SkillsModel skillsModel;
    SkillRecyclerViewAdapter adapter;
    List<SkillsModel> skills = new ArrayList<>();
    int selectedPosition;
    private StaggeredGridLayoutManager _sGridLayoutManager;
    List<Languages> languagesList = new ArrayList<>();
    LanguagesRecyclerViewAdapter languageAdapter;


    Progress progress = Progress.getInstance();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_skills_and_languages;
    }

    @Override
    protected void initViews() {

        Intent in = getIntent();
        skillsModel = (SkillsModel) in.getExtras().get("data");
        type = in.getExtras().getString("type");
        if (type.equals("skills")) {
            skillsModel = (SkillsModel) in.getExtras().get("data");
            skillsLayout.setVisibility(View.VISIBLE);
            languagesLayout.setVisibility(View.GONE);
            skillsLayout.setVisibility(View.VISIBLE);
            languageScroll.setVisibility(View.GONE);
            getSkillsData();
            initAdapter();

        } else {
            skillsLayout.setVisibility(View.GONE);
            languagesLayout.setVisibility(View.VISIBLE);
            skillsLayout.setVisibility(View.GONE);
            languageScroll.setVisibility(View.VISIBLE);
            getLanguageData();
            _sGridLayoutManager = new StaggeredGridLayoutManager(3,
                    StaggeredGridLayoutManager.VERTICAL);
            rvLanguages.setLayoutManager(_sGridLayoutManager);
            languageAdapter = new LanguagesRecyclerViewAdapter(this, languagesList);
            languageAdapter.setClickListener(this);
            rvLanguages.setAdapter(languageAdapter);
        }
    }

    private void initAdapter() {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        int numberOfColumns = 4;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        adapter = new SkillRecyclerViewAdapter(this, skills);
        adapter.setClickListener(this);
        adapter.setSingleSelect(true);
        if (skillsModel != null)
            adapter.setSkillsModel(skillsModel);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        selectedPosition = position;
    }

    @Override
    public void onSingleItemClick(int data) {

    }

    @OnClick(R.id.button_add_skill)
    public void addButtonClick(View view) {
        if (Connectivity.hasConnection(this) && skillsModel != null) {
            progress.showProgressDialog(this, "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(this));
            ApiClient.getTokenId(Prefs.getToken(this));
            JsonObject jsonObject = new JsonObject();
            JsonObject data = new JsonObject();
            data.addProperty("experience", "");
            data.addProperty("last_used", "");
            data.addProperty("currently_using", "");
            data.addProperty("skill_name", skills.get(selectedPosition).getSkill_name());
            data.addProperty("skill_set_public_id", skillsModel == null ? skills.get(selectedPosition).getSkill_set_public_id() : skillsModel.getSkill_set_public_id());
            data.addProperty("public_id", skillsModel == null ? skills.get(selectedPosition).getPublic_id() : skillsModel.getPublic_id());
            jsonObject.add("skill", data);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call call = apiService.updateSkills(skillsModel.getPublic_id(), jsonObject);
            call.enqueue(new CallBackHelper(this) {

                @Override
                public void onResponse(Call call, Response response) {
                    super.onResponse(call, response);
                    if (response != null) {
                        ResponseData data = (ResponseData) response.body();
                        if (data != null) {
                            if (data.isSuccess()) {
                                onSuccess(data);
                            } else {

                            }
                        }

                    } else {

                    }
                }
                @Override
                public void onFailure(Call call, Throwable t) {
                    super.onFailure(call, t);
                }
            });
        } else {

        }

    }


    private void getSkillsData() {
//        String[] data = {"chef", "delivery guy", "waitress", "care giver", "driver", "tele caller", "bartender", "welder"};
//
//        int[] imageData = {R.drawable.chef_grey, R.drawable.courier_grey, R.drawable.croupier_grey
//                , R.drawable.maid_grey, R.drawable.taxi_driver_grey, R.drawable.telecaller_grey,
//                R.drawable.waiter_grey, R.drawable.welder_grey};
//        int[] colorImageData = {R.drawable.chef, R.drawable.courier, R.drawable.croupier
//                , R.drawable.maid, R.drawable.taxidriver, R.drawable.telecaller,
//                R.drawable.waiter, R.drawable.welder};
//
//        for (int i = 0; i < data.length; i++) {
//            SkillsModel skillsModel = new SkillsModel();
//            skillsModel.setSkill_name(data[i]);
//            skillsModel.setGreyImage(imageData[i]);
//           // skillsModel.setColorImage(colorImageData[i]);
//            skills.add(skillsModel);
//        }

        if (Connectivity.hasConnection(this)) {
            progress.showProgressDialog(this, "Please Wait..");
            getSkillSets();
        } else {
            showSnackbar(rootLayout, "Please check internet connection");
        }

    }

    private void getSkillSets() {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getSkillSet();
        call.enqueue(new CallBackHelper(this) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
                        progress.hideProgressBar();
                        for (int i = 0; i < data.getSkill_sets().size(); i++) {
                            SkillsModel skillsModel = new SkillsModel();
                            skillsModel.setSkill_name(data.getSkill_sets().get(i).getName());
                            //skillsModel.setGreyImage(imageData[i]);
                            skillsModel.setColorImage(data.getSkill_sets().get(i).getColored_icon());
                            skills.add(skillsModel);
                        }
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    progress.hideProgressBar();
                    showToast(response.message());
                }
            }
        });

    }

    @Override
    public void onSuccess(Object object) {
    }


    private void getLanguageData() {
        Languages language = new Languages();
        language.setName("Marathi");
        Languages language1 = new Languages();
        language1.setName("Hindi");
        Languages language2 = new Languages();
        language2.setName("English");
        Languages language3 = new Languages();
        language3.setName("Malyalam");

        languagesList.add(language);
        languagesList.add(language1);
        languagesList.add(language2);
        languagesList.add(language3);
    }

    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onValidationFailed(String message) {

    }

    @Override
    public void onItemClick(List<Languages> languagesList) {

    }
}
