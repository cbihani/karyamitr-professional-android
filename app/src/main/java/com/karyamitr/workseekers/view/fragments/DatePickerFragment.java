package com.karyamitr.workseekers.view.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    DatePickerDialog.OnDateSetListener ondateSet;
    private int year, month, day;

    public DatePickerFragment() {
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    @SuppressLint("NewApi")
    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
//        year = args.getInt("year");
//        month = args.getInt("month");
//        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.YEAR, 0);
        calendar.set(Calendar.YEAR, 2010);
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), ondateSet, calendar.get(Calendar.YEAR), month, day);
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis()-568024668000L);
        return dpd;
//        return new DatePickerDialog(getActivity(), ondateSet, year, month, day);
    }


}
