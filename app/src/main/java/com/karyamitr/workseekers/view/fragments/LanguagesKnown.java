package com.karyamitr.workseekers.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.LanguagesRecyclerViewAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Languages;
import com.karyamitr.workseekers.model.Professional;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.RegisterViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class LanguagesKnown extends Fragment implements LanguagesRecyclerViewAdapter.ItemClickListener, ResultCallBack {


    public LanguagesKnown() {
        // Required empty public constructor
    }

    Progress progress = Progress.getInstance();
    LanguagesRecyclerViewAdapter adapter;
    List<Languages> languagesList = new ArrayList<>();
    List<Languages> languagesSelectedList = new ArrayList<>();
    RegisterViewModel registerViewModel;
    Professional professional;

    @BindView(R.id.recycler_view_languages)
    RecyclerView recycler_view_languages;

    private StaggeredGridLayoutManager _sGridLayoutManager;


    public static LanguagesKnown newInstance(int page, boolean isLast) {
        Bundle args = new Bundle();
        args.putInt("page", page);
        if (isLast)
            args.putBoolean("isLast", true);
        final LanguagesKnown fragment = new LanguagesKnown();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.button_add)
    public void addLanguages(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (Connectivity.hasConnection(getContext())) {
            progress.showProgressDialog(getContext(), "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(getContext()));
            ApiClient.getTokenId(Prefs.getToken(getContext()));
//            Languages lLangauges = new Languages();
//            for (Languages lang : languagesSelectedList) {
//                lLangauges.setName(lang.getName());
//                lLangauges.setCan_read(lang.isCan_read());
//                lLangauges.setCan_write(lang.isCan_write());
//                lLangauges.setCan_speak(lang.isCan_speak());
//                Log.d(TAG, "addLanguages: " + lang.getName());
//                Log.d(TAG, "addLanguages: " + lang.isCan_read());
//                Log.d(TAG, "addLanguages: " + lang.isCan_write());
//                Log.d(TAG, "addLanguages: " + lang.isCan_speak());
//            }

            if (professional != null && professional.getLanguages() != null)
                languagesSelectedList.addAll(professional.getLanguages());
            Professional professional = new Professional();
            professional.setLanguages(languagesSelectedList);
            registerViewModel.callLanguagesApi(professional);

        } else {
            ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
        }
    }

    private void getLanguageData() {
        Languages language = new Languages();
        language.setName("Marathi");
        Languages language1 = new Languages();
        language1.setName("Hindi");
        Languages language2 = new Languages();
        language2.setName("English");
        Languages language3 = new Languages();
        language3.setName("Sanskrit");
        Languages language4 = new Languages();
        language4.setName("French");
        Languages language5 = new Languages();
        language5.setName("Spanish");
        Languages language6 = new Languages();
        language6.setName("Gujarati");
        Languages language7 = new Languages();
        language7.setName("Bengali");


        if (!checkForExistingLanguage(language))
            languagesList.add(language);

        if (!checkForExistingLanguage(language1))
            languagesList.add(language1);

        if (!checkForExistingLanguage(language2))
            languagesList.add(language2);

        if (!checkForExistingLanguage(language3))
            languagesList.add(language3);

        if (!checkForExistingLanguage(language4))
            languagesList.add(language4);

        if (!checkForExistingLanguage(language5))
            languagesList.add(language5);

        if (!checkForExistingLanguage(language6))
            languagesList.add(language6);

        if (!checkForExistingLanguage(language7))
            languagesList.add(language7);

    }

    private boolean checkForExistingLanguage(Languages language) {
        if (professional != null)
            for (Languages data : professional.getLanguages()) {
                if (data.getName().equalsIgnoreCase(language.getName()))
                    return true;
            }
        return false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_languages_known, container, false);
        ButterKnife.bind(this, view);
        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
        registerViewModel.getLanguagesListener(this);
        _sGridLayoutManager = new StaggeredGridLayoutManager(3,
                StaggeredGridLayoutManager.VERTICAL);
        recycler_view_languages.setLayoutManager(_sGridLayoutManager);
        // getLanguageData();

        professional = (Professional) getActivity().getIntent().getSerializableExtra("data");
//        if (professional != null && professional.getLanguages() != null)
//            languagesList = professional.getLanguages();
        getLanguageData();
        adapter = new LanguagesRecyclerViewAdapter(getContext(), languagesList);
        adapter.setClickListener(this);
        recycler_view_languages.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }


    @Override
    public void onItemClick(List<Languages> langSelectedList) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        languagesSelectedList = langSelectedList;

    }

    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
        ResponseData data = (ResponseData) object;
        Log.d(TAG, "onSuccess: " + data.toString());
        if (data.isSuccess() && !getActivity().isFinishing()) {
//            ((Register) getActivity()).setCurrentItem(6, true);
            if (getActivity().getIntent().getStringExtra("Type") == null) {
                Intent i = new Intent(getContext(), Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                ((Register) getActivity()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
            getActivity().finish();
        }
        if (getActivity() instanceof Register) {
            ((Register) getActivity()).showToast(data.getMessage());
            Prefs.setLaunchActivity(getActivity(), "6");
        } else {
            ((BaseActivity) getActivity()).showToast(data.getMessage());

        }


    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {

    }
}
