package com.karyamitr.workseekers.view.activity;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.MyApplication;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.viewmodel.LoginViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

//7899278418
public class Login extends BaseActivity implements ResultCallBack {

    @BindView(R.id.edittext_mobile)
    EditText editTextMobile;

    @BindView(R.id.root)
    RelativeLayout rootLayout;

    Progress progress = Progress.getInstance();
    LoginViewModel loginViewModel;
    public static final int REQUEST_CODE_READ_SMS = 1, REQUEST_ID_MULTIPLE_PERMISSIONS = 2;


    @Override
    protected int getLayoutId() {
        setFullscreen();
        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.getListners(this);
        //checkAndRequestPermissions();


        ActivityCompat.requestPermissions(Login.this, new String[]{android.Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_READ_SMS);
    }


    @OnClick(R.id.button_login)
    public void onLoginClick(View view) {
        if (Connectivity.hasConnection(this)) {
//            Intent in = new Intent(Login.this, Register.class);
//            startActivity(in);

            if (MyApplication.isValidMobile(editTextMobile.getText().toString())) {
                progress.showProgressDialog(this, "Loading.. Please Wait..");
                loginViewModel.callApi(editTextMobile.getText().toString());
            } else {
                editTextMobile.setError("Please enter a valid mobile number");
            }
        } else {
            Snackbar.make(view, "Please check internet connection", Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void onSuccess(Object object) {

        ResponseData data = (ResponseData) object;
        if (data.isSuccess()) {
            Intent in = new Intent(this, OtpVerification.class);
            in.putExtra("mobile", editTextMobile.getText().toString());
            Prefs.setUserToken(this, data.getUserToken());
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            progress.hideProgressBar();
            finish();

        }
        showToast(data.getMessage());
    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {
//        editTextError = (NinePatchDrawable) getResources().getDrawable(R.drawable.ic_cancel_black_24dp);

        Drawable dr = getResources().getDrawable(R.drawable.ic_cancel_black_24dp);
        //add an error icon to yur drawable files
        dr.setBounds(0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight(), 0);
//        editTextMobile.setCompoundDrawables(null, null, dr, null);
        editTextMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_black_24dp, 0);
        editTextMobile.setBackground(getResources().getDrawable(R.drawable.edittext_background_error));
        showSnackbar(rootLayout, message);

    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

}
