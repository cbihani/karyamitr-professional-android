package com.karyamitr.workseekers.view.fragments;

import android.app.DatePickerDialog;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.ExperienceRecyclerViewAdapter;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.model.WorkExperience;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.view.activity.Home;
import com.karyamitr.workseekers.view.activity.Register;
import com.karyamitr.workseekers.viewmodel.RegisterViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class WorkExperienceFragment extends Fragment implements ResultCallBack, ExperienceRecyclerViewAdapter.ItemClickListener {

    private static final String TAG = "WorkExperienceFragment";

    @BindView(R.id.textview_joining_year)
    TextView joiningYear;

    @BindView(R.id.textview_worked_till)
    TextView workedTill;

    @BindView(R.id.edittext_designation)
    EditText designation;

    @BindView(R.id.cancel)
    TextView cancel;

    @BindView(R.id.edittext_company_name)
    EditText companyName;

    @BindView(R.id.edittext_city)
    EditText city;

    @BindView(R.id.checkbox_working_here)
    CheckBox isWorking;

    @BindView(R.id.textview_add_more_experience)
    TextView addMore;

    @BindView(R.id.recycler_view_experience)
    RecyclerView recyclerViewExperience;

    @BindView(R.id.relativeLayout_experience)
    RelativeLayout relativeLayoutExperience;


    Progress progress = Progress.getInstance();
    RegisterViewModel registerViewModel;


    List<WorkExperience> workExperienceList = new ArrayList<>();
    WorkExperience workExperience;
    ExperienceRecyclerViewAdapter adapter;
    String type = "new";
    int adapterPosition;
    View view;

    public WorkExperienceFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static WorkExperienceFragment newInstance(int page, boolean isLast) {
        Bundle args = new Bundle();
        args.putInt("page", page);
        if (isLast)
            args.putBoolean("isLast", true);
        final WorkExperienceFragment fragment = new WorkExperienceFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @OnClick(R.id.skip_option)
    public void skipOtion(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

//        Intent i = new Intent(getContext(), Home.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(i);
//        getActivity().finish();
        ((Register) getActivity()).setCurrentItem(4, true);
    }

    @OnClick(R.id.cancel)
    public void onCancelClick(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        if (relativeLayoutExperience.getVisibility() == View.VISIBLE)
            relativeLayoutExperience.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_work_experience, container, false);
        ButterKnife.bind(this, view);

        registerViewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);
        registerViewModel.getExperienceListener(this);
        designation.setHint(Html.fromHtml("Designation <font color='red'>*</font>"));
        companyName.setHint(Html.fromHtml("Company name <font color='red'>*</font>"));
        city.setHint(Html.fromHtml("City <font color='red'>*</font>"));

        recyclerViewExperience.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        adapter = new ExperienceRecyclerViewAdapter(getContext(), workExperienceList);
        adapter.setClickListener(this);
        recyclerViewExperience.setAdapter(adapter);
        workExperience = (WorkExperience) getActivity().getIntent().getSerializableExtra("data");
        isWorking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                workedTill.setText("" + Calendar.getInstance().get(Calendar.YEAR));
            }
        });
        if (workExperience != null)
            setDataToViews();
        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }

    private void setDataToViews() {
//        callLayerView();
        if (relativeLayoutExperience.getVisibility() == View.GONE)
            relativeLayoutExperience.setVisibility(View.VISIBLE);
        designation.setText(workExperience.getTitle());
        companyName.setText(workExperience.getCompany());
        city.setText(workExperience.getCity());
        joiningYear.setText(workExperience.getJoining_year());
        workedTill.setText(workExperience.getRelieving_year());
        cancel.setVisibility(View.GONE);
    }


    @OnClick(R.id.textview_joining_year)
    public void openDialogForJoiningYear(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        showYearPickerForJoiningYear();
    }

    @OnClick(R.id.textview_worked_till)
    public void openDialogForWorkedTill(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        if (!isWorking.isChecked())
            showYearPickerForWorkedTill();
    }


    private void showYearPickerForJoiningYear() {
        YearPickerDialog date = new YearPickerDialog();
        date.setListener(onDateForJoining);
        date.show(getFragmentManager(), "Date Picker");
    }

    private void showYearPickerForWorkedTill() {
        YearPickerDialog date = new YearPickerDialog();
        try {
            date.setMIN_YEAR(Integer.parseInt(joiningYear.getText().toString()));
        } catch (Exception ex) {
        } finally {
            date.setListener(onDateForWorkedtill);
            date.show(getFragmentManager(), "Date Picker");
        }

    }

    DatePickerDialog.OnDateSetListener onDateForJoining = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            if (!isWorking.isChecked())
                workedTill.setText("");
            joiningYear.setText(String.valueOf(year));
        }
    };

    DatePickerDialog.OnDateSetListener onDateForWorkedtill = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            workedTill.setText(String.valueOf(year));
        }
    };


    @OnClick(R.id.button_all_experience)
    public void saveAllExperienceList(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        if (workExperienceList.size() > 0) {
            if (Connectivity.hasConnection(getContext())) {
                Log.e("--", "--1");
                progress.showProgressDialog(getContext(), "Please Wait..");
                ApiClient.getUuid(Prefs.getUserToken(getContext()));
                ApiClient.getTokenId(Prefs.getToken(getContext()));
                JsonObject object = null;
                JsonArray jsonArray = new JsonArray();
                for (WorkExperience mWorkExperience : workExperienceList) {
                    object = new JsonObject();
                    object.addProperty("title", mWorkExperience.getTitle());
                    object.addProperty("company", mWorkExperience.getCompany());
                    object.addProperty("city", mWorkExperience.getCity());
                    object.addProperty("working_presently", mWorkExperience.isWorking_presently());
                    object.addProperty("joining_year", mWorkExperience.getJoining_year());
                    object.addProperty("relieving_year", mWorkExperience.getRelieving_year());
                    jsonArray.add(object);
                }

                JsonObject obj = new JsonObject();
                obj.add("work_experiences", jsonArray);
                Log.d(TAG, "workExperience: " + obj.toString());
                JsonObject workEx = new JsonObject();
                workEx.add("work_experience", object);

                if (workExperience != null && workExperience.getPublic_id() != null) {

                    registerViewModel.callApiForUpdateExperience(workExperience.getPublic_id(), workEx);
                } else {
                    registerViewModel.callApiForExperience(obj);
                }

            } else {
                ((Register) getActivity()).showSnackbar(view, "Please check internet connection");
            }
        } else {
            ((Register) getActivity()).showSnackbar(view, "Please add experience");
        }
    }

    @OnClick(R.id.button_save_experience)
    public void addExperience(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        if (registerViewModel.checkValidationForWorkExperience(designation.getText().toString(), companyName.getText().toString(), city.getText().toString())) {
            if (workExperience == null)
                if (relativeLayoutExperience.getVisibility() == View.VISIBLE)
                    relativeLayoutExperience.setVisibility(View.GONE);

            WorkExperience workExperience = new WorkExperience();
            workExperience.setTitle(designation.getText().toString());
            workExperience.setCompany(companyName.getText().toString());
            workExperience.setCity(city.getText().toString());
            workExperience.setWorking_presently(isWorking.isChecked());
            workExperience.setJoining_year(joiningYear.getText().toString());
            workExperience.setRelieving_year(workedTill.getText().toString());

            if (this.workExperience == null) {
                if (type.equals("new")) {
                    workExperienceList.add(workExperience);
                    setUpRecyclerView();
                } else {
                    workExperienceList.set(adapterPosition, workExperience);
                    adapter.notifyDataSetChanged();
                    adapter.notifyItemChanged(adapterPosition);
                }
                designation.getText().clear();
                companyName.getText().clear();
                city.getText().clear();
                joiningYear.setText("");
                workedTill.setText("");
                isWorking.setChecked(false);
            } else {
                workExperienceList.add(workExperience);
                saveAllExperienceList(null);
            }
        }
    }

    private void setUpRecyclerView() {
        if (workExperienceList.size() >= 1) {
            adapter.notifyDataSetChanged();
        }


    }

    @OnClick(R.id.textview_add_more_experience)
    public void addMoreExperience(View view) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);
        callLayerView();
    }

    private void callLayerView() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        relativeLayoutExperience.startAnimation(animationFadeIn);
        if (relativeLayoutExperience.getVisibility() == View.GONE)
            relativeLayoutExperience.setVisibility(View.VISIBLE);
    }


    @Override
    public void onSuccess(Object object) {


        progress.hideProgressBar();
        ResponseData data = (ResponseData) object;
        Log.d(TAG, "onSuccess: " + data.toString());
        if (workExperience == null) {
            if (data.isSuccess()) {
                ((Register) getActivity()).setCurrentItem(4, true);
            }
            ((BaseActivity) getActivity()).showToast(data.getMessage());
            Prefs.setLaunchActivity(getActivity(), "4");
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        ((BaseActivity) getActivity()).showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {
        ((Register) getActivity()).showSnackbar(view, message);
    }

    @Override
    public void onItemClick(WorkExperience mExperience, int position) {
        if (getActivity() instanceof Home)
            ((Home)getActivity()).setBottomBarVisivelty(View.VISIBLE);

        Log.d(TAG, "onItemClick: " + mExperience.getCompany());
        designation.setText(mExperience.getTitle());
        companyName.setText(mExperience.getCompany());
        city.setText(mExperience.getCity());
        joiningYear.setText(mExperience.getJoining_year());
        workedTill.setText(mExperience.getRelieving_year());
        isWorking.setChecked(mExperience.isWorking_presently());

        type = "edit";
        adapterPosition = position;
        callLayerView();
    }

}
