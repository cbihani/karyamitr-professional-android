package com.karyamitr.workseekers.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.ApplicantDocumentList;
import com.karyamitr.workseekers.model.Company;
import com.karyamitr.workseekers.model.Details;
import com.karyamitr.workseekers.model.Jobs;
import com.karyamitr.workseekers.utils.Prefs;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.viewmodel.JobDetailsViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

public class JobDetails extends BaseActivity implements ResultCallBack, JobDetailsViewModel.ProfileInterface {

    private static final String TAG = JobDetails.class.getSimpleName();
    Progress progress = Progress.getInstance();
    JobDetailsViewModel jobDetailsViewModel;
    ApplicantDocumentList applicantDocumentList = new ApplicantDocumentList();
    String publicId;
    HashMap<String, Boolean> benefits;

    JsonObject filterData = null;

    @BindView(R.id.job_details_icon)
    ImageView jobDetailIcon;

    @BindView(R.id.company_job_details)
    TextView companyName;

    @BindView(R.id.title_job_details)
    TextView title;

    @BindView(R.id.role)
    TextView role;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.location)
    TextView location;

    @BindView(R.id.employment)
    TextView employment;

    @BindView(R.id.salary)
    TextView salary;

    @BindView(R.id.shift)
    TextView shift;

    @BindView(R.id.primary_skill)
    TextView primarySkill;

    @BindView(R.id.secondary_skill)
    TextView secondarySkill;

    @BindView(R.id.experience)
    TextView experience;

    @BindView(R.id.vacancies)
    TextView vacancies;

    @BindView(R.id.cgBenefits)
    ChipGroup cgBenefits;

    @BindView(R.id.interview_layout)
    LinearLayout interviewLayout;

    @BindView(R.id.interview_location)
    TextView interviewLocation;

    @BindView(R.id.interview_contact)
    TextView interviewContact;

    @BindView(R.id.interview_contact_no)
    TextView interviewContactNo;

    @BindView(R.id.applicant_status)
    TextView applicantStatus;

    @BindView(R.id.button_apply_job)
    Button applyJob;

    Details details;

    @OnClick(R.id.button_apply_job)
    public void applyJob(View view) {
        if (applyJob.getText().equals("OK")) {
            onBackPressed();

        } else {


            Log.d(TAG, "applyJob: " + applicantDocumentList.toString());

            if (applicantDocumentList != null && applicantDocumentList.isDocument_check()) {
//            Intent in = new Intent(this, DocumentsNeeded.class);
//            in.putExtra("applicant_documents", applicantDocumentList);
//            startActivity(in);
                Intent in = new Intent(this, DocumentsNeeded.class);
                in.putExtra("applicant_documents", applicantDocumentList);
                startActivityForResult(in, 1111);

            } else {

                callApplyJobApi();

            }
        }
    }

    private void callApplyJobApi() {
        if (Connectivity.hasConnection(this)) {
            progress.showProgressDialog(this, "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(this));
            ApiClient.getTokenId(Prefs.getToken(this));
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("job_public_id", publicId);
            jsonObject.add("job_specific_answers", filterData);
            JsonObject jobApplication = new JsonObject();
            jobApplication.add("job_application", jsonObject);
            jobDetailsViewModel.callApi(jobApplication);
        } else {
            showToast(R.string.checkInternet);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1111 && resultCode == Activity.RESULT_OK) {
            Gson gson = new Gson();
            filterData = gson.fromJson(data.getStringExtra("applicant_document_list"), JsonObject.class);
            Log.d(TAG, "onActivityResult: " + filterData.toString());
            callApplyJobApi();
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_job_details;
    }

    @Override
    protected void initViews() {

        jobDetailsViewModel = ViewModelProviders.of(this).get(JobDetailsViewModel.class);
        jobDetailsViewModel.getListners(this);
        jobDetailsViewModel.ProfileInterface(this);
        // getUtilities();

//        getProfileDetails();
        Intent in = getIntent();

        Jobs job = (Jobs) in.getExtras().get("Jobs");

        String jobType = in.getExtras().getString("pageType");
        if (jobType != null) {

            if (jobType.equalsIgnoreCase("applied")) {
                //applyJob.setVisibility(View.GONE);
                applyJob.setText("OK");
            }
        }

        //getJobDetail(publicId);
        publicId = job.getPublic_id();
        Company company = job.getCompany();
        details = job.getDetails();
        if (job.getApplicantDocumentList() != null)
            applicantDocumentList = job.getApplicantDocumentList();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.logo_grey);
        requestOptions.error(R.drawable.logo_grey);
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(company.getLogo()).into(jobDetailIcon);
        title.setText(job.getTitle());
        companyName.setText(company.getName());
        role.setText(Html.fromHtml("Role: " + "<b>" + job.getFunctional_area()));
        Log.e("TAG", "-" + job.getDescription());
//        description.setText(Html.fromHtml(job.getDescription()));
        description.setText(Html.fromHtml("<font size= 14px>" + job.getDescription() + "</font>"));
        location.setText(Html.fromHtml("Location: " + "<b> " + job.getLocation() + "</b>"));
        employment.setText(Html.fromHtml("employment type: " + "<b> " + job.getEmployment_type() + "</b>"));
        if (job.getSalary() != null) {
            salary.setText(Html.fromHtml("Salary: " + "<b> " + job.getSalary() + "</b>"));
        } else {
            salary.setText(Html.fromHtml("Salary: " + "<b> " + "NA" + "</b>"));
        }
        shift.setText(Html.fromHtml("Shift: " + "<b> " + job.getShift() + "</b>"));
        primarySkill.setText(Html.fromHtml("Primary skill: " + "<b> " + job.getPrimary_skill() + "</b>"));
        secondarySkill.setText(Html.fromHtml("Secondary skill: " + "<b> " + job.getSecondary_skill() + "</b>"));
        experience.setText(Html.fromHtml("Experience: " + "<b> " + job.getMinimum_experience() + "</b>"));
        if (details != null)
            vacancies.setText(Html.fromHtml("Vacancies: " + "<b> " + details.getVacancies() + "</b>"));
        benefits = job.getBenefits();
        if (benefits != null) {
            for (String data : getBenifitsList()) {
                Chip chip = new Chip(cgBenefits.getContext());
                String newData = data.replace("_", " ").toUpperCase();
                chip.setText(newData);
                chip.setTextColor(getResources().getColor(R.color.colorPrimary));
                chip.setClickable(false);
                cgBenefits.addView(chip);
            }
        }


        if (job.getJob_application() != null) {
            applicantStatus.setVisibility(View.VISIBLE);
            applicantStatus.setText(job.getJob_application().getStatus());
            if (job.getJob_application().isShortlisted()) {
                if (job.getInterview_details() != null) {
                    interviewLayout.setVisibility(View.VISIBLE);
                    interviewContact.setText(job.getInterview_details().getContact_person_name());
                    interviewContactNo.setText(job.getInterview_details().getContact_person_number());
                    interviewLocation.setText(job.getInterview_details().getAddress());
                }
            }
        }


    }

    private List<String> getBenifitsList() {
        List<String> benefitsList = new ArrayList<>();
        Set keys = benefits.keySet();
        for (Object key : keys) {
            if (benefits.get(key)) {
                benefitsList.add((String) key);
            }
        }
        return benefitsList;
    }

    private void getUtilities() {
        if (Connectivity.hasConnection(this)) {
            progress.showProgressDialog(this, "Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(this));
            ApiClient.getTokenId(Prefs.getToken(this));
            jobDetailsViewModel.getJobUtilities();
        } else {
            showToast(R.string.checkInternet);
        }
    }

    private void getProfileDetails() {
        if (Connectivity.hasConnection(this)) {
            progress.showProgressDialog(this, "Loading.. Please Wait..");
            ApiClient.getUuid(Prefs.getUserToken(this));
            ApiClient.getTokenId(Prefs.getToken(this));
            jobDetailsViewModel.callGetProfile();
        } else {
            showToast(String.valueOf(R.string.checkInternet));
        }
    }


    @Override
    public void onSuccess(Object object) {
        progress.hideProgressBar();
//        if (object != null) {
//            Job job = (Job) object;
//            List<String> list = job.getBenefits_with_employeers();
//            List<Object> objectList = new ArrayList<Object>(list);
//
//
//        } else {
        Intent in = new Intent(this, Home.class);
        startActivity(in);
        finish();
        showToast("Applied successfully");
//        }

    }

    @Override
    public void onFailure(String message) {
        progress.hideProgressBar();
        showToast(message);
    }

    @Override
    public void onValidationFailed(String message) {

    }


    @Override
    public void getProfessionalDetails(Object professional) {

    }
}
