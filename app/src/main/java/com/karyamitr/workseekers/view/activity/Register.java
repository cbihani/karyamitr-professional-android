package com.karyamitr.workseekers.view.activity;

import android.content.Context;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import androidx.viewpager.widget.ViewPager;

import com.badoualy.stepperindicator.StepperIndicator;
import com.karyamitr.workseekers.BaseActivity;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.adapter.ViewPagerAdapter;
import com.karyamitr.workseekers.utils.ZoomOutPageTransformer;
import com.karyamitr.workseekers.view.fragments.RegisterFragment;
import com.karyamitr.workseekers.view.fragments.RegistrationDone;

import java.lang.reflect.Field;

import butterknife.BindView;

public class Register extends BaseActivity implements RegisterFragment.OnFragmentInteractionListener,
        RegistrationDone.OnFragmentInteractionListener {

    @BindView(R.id.stepper)
    StepperIndicator stepperIndicator;

    @BindView(R.id.pager)
    ViewPager viewPager;


    @Override
    protected int getLayoutId() {
//        setFullscreen();
        return R.layout.activity_register;
    }

    @Override
    protected void initViews() {
        assert viewPager != null;
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        try {
            Interpolator sInterpolator = new AccelerateInterpolator();
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext(), sInterpolator);
            // scroller.setFixedDuration(5000);
            mScroller.set(viewPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }


        stepperIndicator.setViewPager(viewPager, true);
        stepperIndicator.addOnStepClickListener(new StepperIndicator.OnStepClickListener() {
            @Override
            public void onStepClicked(int step) {
                viewPager.setCurrentItem(step, true);
            }
        });


        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void setCurrentItem(int item, boolean smoothScroll) {
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.setCurrentItem(item, smoothScroll);
    }

    public void setPagerTransformer() {
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public class FixedSpeedScroller extends Scroller {

        private int mDuration = 1000;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, interpolator, flywheel);
        }


        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }


}
