package com.karyamitr.workseekers.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.karyamitr.workseekers.R;
import com.karyamitr.workseekers.api.ApiClient;
import com.karyamitr.workseekers.api.ApiInterface;
import com.karyamitr.workseekers.api.Connectivity;
import com.karyamitr.workseekers.api.ResultCallBack;
import com.karyamitr.workseekers.model.Job;
import com.karyamitr.workseekers.model.ResponseData;
import com.karyamitr.workseekers.utils.CallBackHelper;
import com.karyamitr.workseekers.utils.Progress;
import com.karyamitr.workseekers.viewmodel.FilterViewModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class FilterActivity extends AppCompatActivity implements ResultCallBack {
    RecyclerView categoryRv, filterRv;
    HashMap<String, List<String>> filterData;
    FilterAdapter filterAdapter;
    Progress progress = Progress.getInstance();
    FilterViewModel filterViewModel;
    JsonObject filterJsonData;
    CategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        filterViewModel = ViewModelProviders.of(this).get(FilterViewModel.class);
        filterData = new HashMap<>();
        getCategories();
        categoryRv = findViewById(R.id.categoryRv);
        filterRv = findViewById(R.id.filterRv);
    }

    private void getCategories() {
        if (Connectivity.hasConnection(getApplicationContext())) {
            callApi();

        } else {
            Toast.makeText(FilterActivity.this, R.string.checkInternet, Toast.LENGTH_SHORT).show();
        }

    }

    private void callApi() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call call = apiService.getCategories();
        call.enqueue(new CallBackHelper(this) {

            @Override
            public void onResponse(Call call, Response response) {
                super.onResponse(call, response);
                if (response != null) {
                    ResponseData data = (ResponseData) response.body();
                    if (data != null) {
//                        if (data.isSuccess()) {
                        onSuccess(data.getJob());
//                        } else {
//
//                        }
                    } else {

                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                super.onFailure(call, t);
                Toast.makeText(FilterActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setSleectedData() {
        JsonObject data = filterJsonData.getAsJsonObject("filter");
        Set<String> keys = data.keySet();
        JsonArray dataArray;
        for (String key : keys) {
            dataArray = data.getAsJsonArray(key);
            setSelectedDataToList(key, dataArray);
        }
        filterAdapter.setFilterList(filterData.get("Location"), 0);
        filterAdapter.notifyDataSetChanged();
    }

    private void setSelectedDataToList(String key, JsonArray data) {

        Type listType = new TypeToken<List<String>>() {
        }.getType();

        List<String> selectedData = new Gson().fromJson(data, listType);
        switch (key) {
            case "category":
                filterAdapter.getCategoriesList().addAll(selectedData);
                break;
            case "employment_type":
                filterAdapter.getJobTypeList().addAll(selectedData);
                break;
            case "location":
                filterAdapter.getLocationList().addAll(selectedData);
                break;
        }
    }

    private void createData() {


        List<String> filter1 = new ArrayList<>();
        filter1.add("Full Time");
        filter1.add("Part Time");
        filter1.add("Contractual");
        filter1.add("Freelancer");

        List<String> filter2 = new ArrayList<>();

        filter2.add("Mumbai, Maharashtra");
        filter2.add(" Navi Mumbai, Maharashtra");
        filter2.add("Bengaluru, Karnataka");
        filter2.add("Hyderabad, Telangana");
        filter2.add("Delhi");
        filter2.add("Jaipur, Rajasthan");
        filter2.add(" Kannur, Kerala ");
        filter2.add("Kolhapur, Maharashtra ");
        filter2.add("Meerut, Uttar Pradesh");
        filter2.add("Gurugram (gurgaon), Harayana");
        filter2.add("Bihar");
        filter2.add(" Nashik, Maharashtra");
        filter2.add("Oragadam, Tamil Nadu");
        filter2.add("Goa, Maharashtra");
        filter2.add("Coimbatore, Tamil Nadu");
        filter2.add("Eluru, Andhra Pradesh");
        filter2.add("Chennai, Tamil Nadu");
        filter2.add("Pune, Maharashtra");
        filter2.add("Kolkata, West Bengal");
        filter2.add("Hosur, Tamil Nadu");
        filter2.add("Kishangarh, Rajasthan");
        filter2.add("Guntur, Andhra Pradesh");
        filter2.add("Vijayawada, Andhra Pradesh");
        filter2.add("Thane, Maharashtra");
        filter2.add("Madurai, Tamil Nadu");


        List<String> filter3 = new ArrayList<>();

        filter3.add("Others");
        filter3.add("Back office");
        filter3.add("Care Giver/ Hospital Staff");
        filter3.add("Delivery / Logistics");
        filter3.add("Field Services / QA");
        filter3.add("Field Sales");
        filter3.add("Tele - calling (Outbound)");
        filter3.add("Tele - calling (Inbound)");
        filter3.add("Factory Worker / ITI Trades");
        filter3.add("Restaurant Service Staff");
        filter3.add("Retail Outlet Staff");
        filter3.add("Teaching / School / Training Staff");

//
//        List<String> filter4 = new ArrayList<>();
//
//        filter4.add("KKK");
//        filter4.add("LLL");
//        filter4.add("MMM");
//        filter4.add("NNN");
//        filter4.add("OOO");

        filterData.put("Job Type", filter1);
        filterData.put("Location", filter2);
        filterData.put("Categories", filter3);
//        filterData.put("Verified", filter4);

    }

    @OnClick(R.id.button_apply_filter)
    public void onApplyFilter(View view) {
        JsonObject json = new JsonObject();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("category", getArrayListFromList(filterAdapter.getCategoriesList()));
        jsonObject.add("employment_type", getArrayListFromList(filterAdapter.getJobTypeList()));
        jsonObject.add("location", getArrayListFromList(filterAdapter.getLocationList()));
        json.add("filter", jsonObject);
        Log.e("onApplyFilter", "--" + jsonObject);
        Intent resultIntent = new Intent();
        resultIntent.putExtra("filterData", json.toString());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @OnClick(R.id.clear_all)
    public void onClearAllCall(View view) {
        filterAdapter.clearSelectdeData();
    }

    @Override
    public void onSuccess(Object object) {
        Job job = (Job) object;
        List<String> filter1 = new ArrayList<>();
        filter1.addAll(job.getEmployment_types());
        filterData.put("Job Type", filter1);
        filterData.put("Location", job.getJob_locations());
        filterData.put("Categories", job.getCategories());

        categoryRv.setHasFixedSize(true);
        List<String> mainList = new ArrayList<>();
        mainList.addAll(filterData.keySet());
        categoryRv.setItemAnimator(new DefaultItemAnimator());
        categoryRv.setLayoutManager(new LinearLayoutManager(this));
        categoryRv.getLayoutManager().setMeasurementCacheEnabled(false);
        categoryAdapter = new CategoryAdapter(mainList);
        categoryRv.setAdapter(categoryAdapter);
        filterRv.setItemAnimator(new DefaultItemAnimator());
        filterRv.setLayoutManager(new LinearLayoutManager(this));
        filterRv.getLayoutManager().setMeasurementCacheEnabled(false);

        categoriesList = new ArrayList<>();
        jobTypeList = new ArrayList<>();
        locationList = new ArrayList<>();
        selectedData = new ArrayList<>();

        Gson gson = new Gson();
        filterJsonData = gson.fromJson(getIntent().getStringExtra("selectedFilters"), JsonObject.class);
        if (filterJsonData != null) {
            setSleectedData();
        }


    }

    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onValidationFailed(String message) {

    }


    class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

        List<String> categoryList;
        int row_index = 0;

        public CategoryAdapter(List<String> categoryList) {
            this.categoryList = categoryList;
            filterListData = filterData.get(categoryList.get(0));
            filterAdapter = new FilterAdapter(filterListData, 0);
            filterRv.setAdapter(filterAdapter);
            filterAdapter.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(FilterActivity.this).inflate(R.layout.filter_category, null);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(lp);
            return new CategoryViewHolder(view);
        }

        List<String> filterListData;

        @Override
        public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
            holder.categoryItem.setText(categoryList.get(position));

            if (row_index == position) {
                holder.main_layout.setBackgroundColor(getResources().getColor(R.color.white));
            } else {
                holder.main_layout.setBackgroundColor(getResources().getColor(R.color.colorBackground));
            }


            switch (position) {
                case 0:
                    if (filterAdapter.getLocationList().size() > 0) {
                        holder.tv_badge.setText(filterAdapter.getLocationList().size() + "");
                        holder.tv_badge.setVisibility(View.VISIBLE);
                    } else {
                        holder.tv_badge.setVisibility(View.GONE);
                    }
                    break;
                case 1:
                    if (filterAdapter.getCategoriesList().size() > 0) {
                        holder.tv_badge.setText(filterAdapter.getCategoriesList().size() + "");
                        holder.tv_badge.setVisibility(View.VISIBLE);
                    } else {
                        holder.tv_badge.setVisibility(View.GONE);
                    }
                    break;
                case 2:
                    if (filterAdapter.getJobTypeList().size() > 0) {
                        holder.tv_badge.setText(filterAdapter.getJobTypeList().size() + "");
                        holder.tv_badge.setVisibility(View.VISIBLE);
                    } else {
                        holder.tv_badge.setVisibility(View.GONE);
                    }
                    break;
                default:
                    break;
            }


            holder.categoryItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    row_index = position;
                    filterListData = filterData.get(categoryList.get(position));
                    filterAdapter.setFilterList(filterListData, position);
                    notifyDataSetChanged();
                    holder.tv_badge.setVisibility(View.GONE);
                }
            });


        }

        @Override
        public int getItemCount() {
            return categoryList.size();
        }


        class CategoryViewHolder extends RecyclerView.ViewHolder {

            TextView categoryItem, tv_badge;
            LinearLayout main_layout;

            public CategoryViewHolder(@NonNull View itemView) {
                super(itemView);
                main_layout = itemView.findViewById(R.id.main_layout);
                categoryItem = itemView.findViewById(R.id.categoryItem);
                tv_badge = itemView.findViewById(R.id.tv_badge);
            }
        }
    }

    List<String> selectedData;

    List<String> categoriesList;
    List<String> jobTypeList;
    List<String> locationList;

    class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterViewHolder> {

        List<String> filterList;

        int filterType;

        public FilterAdapter(List<String> filterList, int fileterType) {
            this.filterList = filterList;
            this.filterType = fileterType;
        }


        public void setFilterList(List<String> filterList, int filterType) {
            this.filterList = filterList;
            this.filterType = filterType;
            switch (filterType) {
                case 0:
                    selectedData = locationList;
                    break;
                case 1:
                    selectedData = categoriesList;
                    break;
                case 2:
                    selectedData = jobTypeList;
                    break;
                default:
                    selectedData = new ArrayList<>();
                    break;
            }
            notifyDataSetChanged();
        }


        @NonNull
        @Override
        public FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(FilterActivity.this).inflate(R.layout.filter_item, null);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(lp);
            return new FilterViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FilterViewHolder holder, int position) {

            holder.filterItem.setText(filterList.get(position));
            holder.filterCheckbox.setText(filterList.get(position));
            if (selectedData.contains(filterList.get(position))) {
                holder.filterCheckbox.setChecked(true);
            } else {
                holder.filterCheckbox.setChecked(false);
            }

            holder.filterLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("filterType", "--" + filterType);
                    String selectedFilter = filterList.get(position);
                    switch (filterType) {
                        case 0:

                            if (locationList.contains(selectedFilter)) {
                                locationList.remove(selectedFilter);
                                holder.filterCheckbox.setChecked(false);
                            } else {
                                locationList.add(selectedFilter);
                                holder.filterCheckbox.setChecked(true);
                            }

                            break;
                        case 1:
                            if (categoriesList.contains(selectedFilter)) {
                                categoriesList.remove(selectedFilter);
                                holder.filterCheckbox.setChecked(false);
                            } else {
                                categoriesList.add(selectedFilter);
                                holder.filterCheckbox.setChecked(true);
                            }
                            break;
                        case 2:
                            if (jobTypeList.contains(selectedFilter)) {
                                jobTypeList.remove(selectedFilter);
                                holder.filterCheckbox.setChecked(false);
                            } else {
                                jobTypeList.add(selectedFilter);
                                holder.filterCheckbox.setChecked(true);
                            }
                            break;
                    }
                }
            });

        }

        public List<String> getCategoriesList() {
            if (categoriesList != null)
                return categoriesList;
            else {
                categoriesList = new ArrayList<>();
                return categoriesList;
            }
        }

        public List<String> getJobTypeList() {
            return jobTypeList;
        }

        public List<String> getLocationList() {
            return locationList;
        }


        public void clearSelectdeData() {
            categoriesList.clear();
            jobTypeList.clear();
            locationList.clear();
            selectedData.clear();
            notifyDataSetChanged();
            categoryAdapter.notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            if (filterList != null && filterList.size() > 0) {
                return filterList.size();
            } else {
                return 0;
            }
        }


        class FilterViewHolder extends RecyclerView.ViewHolder {

            TextView filterItem;
            CheckBox filterCheckbox;
            LinearLayout filterLayout;

            public FilterViewHolder(@NonNull View itemView) {
                super(itemView);

                filterItem = itemView.findViewById(R.id.filterItem);
                filterCheckbox = itemView.findViewById(R.id.filter_checkbox);
                filterLayout = itemView.findViewById(R.id.filterLayout);
            }
        }
    }


    private JsonArray getArrayListFromList(List<String> data) {
        JsonArray builder = new JsonArray();

        for (String filaterData : data) {
            builder.add(filaterData);
        }
        return builder;
    }

    @OnClick(R.id.button_close)
    public void closeFilterActivity(View view) {
        finish();
    }

}
